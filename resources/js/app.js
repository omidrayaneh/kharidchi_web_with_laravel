/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import vSelect from 'vue-select'


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('products-list', require('./components/Products.vue').default);
Vue.component('att-group-list', require('./components/AttGroup.vue').default);
Vue.component('att-value-list', require('./components/AttValue.vue').default);
Vue.component('notification-order', require('./components/NotificationOrder.vue').default);
Vue.component('attribute-component', require('./components/AttributeComponent.vue').default);
Vue.component('selected-city', require('./components/SelectedCity.vue').default);
Vue.component('edit-address', require('./components/editAddress.vue').default);
Vue.component('main-filter', require('./components/MainFilter.vue').default);
Vue.component('main-search', require('./components/mainSearch.vue').default);
Vue.component('sell-product', require('./components/sellProduct.vue').default);
Vue.component('new-product', require('./components/newProduct.vue').default);
Vue.component('color-value', require('./components/ColorValue.vue').default);
Vue.component('the-loader', require('./components/TheLoader.vue').default);

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('v-select', vSelect)
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

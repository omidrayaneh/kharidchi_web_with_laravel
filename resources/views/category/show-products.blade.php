@extends('layouts.main-master')
@section('title')
    <title>محصولات دسته بندی  {{$title}} | فروشگاه اینترنتی {{__('word.storeName')}}  </title>
@endsection
@section('style-search')
    <link rel="stylesheet" href="{{asset('assets/css/vendor/nouislider.min.css')}}">
@endsection
@section('content')
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container" id="app">
            <main-filter :sku="{{$sku}}" :category="{{$category}}"></main-filter>
        </div>
    </main>

@endsection
@section('scripts')
    <script src="{{asset('assets/js/vendor/nouislider.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/wNumb.js')}}"></script>
    <script src="{{asset('assets/js/vendor/ResizeSensor.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/theia-sticky-sidebar.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            var nonLinearStepSlider = document.getElementById('slider-non-linear-step');

            noUiSlider.create(nonLinearStepSlider, {
                start: [0, 20000],
                connect: true,
                direction: 'rtl',
                format: wNumb({
                    decimals: 0,
                    thousand: ','
                }),
                range: {
                    'min': [0],
                    '10%': [500, 500],
                    '50%': [40000, 1000],
                    'max': [100000]
                }
            });
            var nonLinearStepSliderValueElement = document.getElementById('slider-non-linear-step-value');

            nonLinearStepSlider.noUiSlider.on('update', function (values) {
                nonLinearStepSliderValueElement.innerHTML = values.join(' - ');
            });
        });
    </script>
@endsection

@extends('layouts.main-master')

@section('title')
    <title>{{$product->title}} | {{__('word.storeName')}} </title>
@endsection
@section('description')
   <meta name="description" content="{{$product->meta_description}}">
@endsection
@section('keyword')
     <meta name="keywords" content="{{$product->meta_keywords}} ">
@endsection
@section('content')
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">
            <!-- Start title - breadcrumb -->
            <div class="title-breadcrumb-special dt-sl mb-3">
                <div class="breadcrumb dt-sl">
                    <nav>
                        @foreach($product->categories as $category)
                            <a href="#">{{$category->title}}</a>
                        @endforeach
                    </nav>
                </div>
            </div>
            <!-- End title - breadcrumb -->

            <!-- Start Product -->

            <div class="dt-sn mb-5 dt-sl">
                <div class="row">
                    <!-- Product Gallery-->
                    <div class="col-lg-4 col-md-6 pb-5 ps-relative">
                        <!-- Product Options-->
                        <ul class="gallery-options">
                            <form method="post" action="/favorite-add/{{$product->sku}}">
                                @csrf
                                <li>
                                    <button type="submit" class="add-favorites">
                                        @auth
                                            @if(!empty($product->favourite->where('favourable_id',$product->id)->where('user_id',auth()->user()->id)->first()))
                                                <i class="mdi mdi-heart red"></i>
                                            @else
                                                <i class="mdi mdi-heart "></i>
                                            @endif
                                        @endauth
                                    </button>
                                    <span class="tooltip-option">افزودن به علاقمندی</span>
                                </li>
                            </form>
                        </ul>

                        <div class="product-gallery">
                            <div class="product-carousel owl-carousel">
                                @foreach($product->photos as $photo)
                                    <div class="item">
                                        <a class="gallery-item" href="{{$photo->path}}"
                                           data-fancybox="gallery1" data-hash="{{$photo->id}}">
                                            <img src="{{$photo->path}}" alt="{{$photo->title}}">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                            <ul class="product-thumbnails">
                                @foreach($product->photos as $photo)
                                    <li @if($loop->first ) class="active" @endif>
                                        <a href="#{{$photo->id}}">
                                            <img src="{{$photo->path}}" alt="{{$photo->title}}">
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- Product Info -->

                    <div class="col-lg-8 col-md-6 pb-5">
                        <div class="product-info dt-sl">
                            <div class="product-title dt-sl">
                                <h1>{{$product->title}}</h1>
                            </div>
                            <div class="product-variant dt-sl">
                                @foreach($attGroup as $group)
                                    @foreach($product->att_values as $item )
                                        @if($group->id == $item->attributeGroup_id && $group->multiple && $group->number)
                                            <div
                                                class="section-title text-sm-title title-wide no-after-title-wide mb-0">
                                                <h2>{{$group->title}} :</h2>
                                            </div>
                                            @break
                                        @endif
                                    @endforeach
                                @endforeach
                                <ul class="product-variants float-right ml-3">
                                    <li class="ui-variant">

                                        @foreach($product->att_values as $item )
                                            @foreach($attGroup as $group)
                                                @if($group->id == $item->attributeGroup_id && $group->multiple)
                                                    @foreach($product->values as $value)


                                                        @if($value->attributeValue_id ==  $item->id && $value->colorCount>0 )
                                                            <label class="ui-variant ui-variant--color">

                                                            <span class="ui-variant-shape"
                                                                  style="background: {{$item->color}}"></span>
                                                                <input type="radio" value="{{$item->id}}"
                                                                       id="data{{$item->id}}" name="data"
                                                                       class="variant-selector">
                                                                <span class="ui-variant--check">{{$item->title}}</span>
                                                            </label>
                                                        @elseif($value->attributeValue_id ==  $item->id && $value->sizeCount>0)
                                                            <label class="ui-variant ui-variant--color">
                                                            <span class="ui-variant-shape"
                                                                  style="background: {{$item->color}}"></span>
                                                                <input type="radio" value="{{$item->id}}"
                                                                       id="data{{$item->id}}" name="data"
                                                                       class="variant-selector">
                                                                <span class="ui-variant--check">{{$item->title}}</span>
                                                            </label>
                                                        @endif

                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </li>
                                </ul>
                            </div>
                            @if(count($values)>0)
                                <div class="product-params dt-sl">

                                    <ul data-title="ویژگی‌های محصول">
                                        @foreach($values as $value)
                                            <li>
                                                <span>{{$value->groupTitle}} : </span>
                                                <span>{{$value->valueTitle}}</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="sum-more">
                                        <span class="show-more btn-link-border">
                                            + موارد بیشتر
                                        </span>
                                        <span class="show-less btn-link-border">
                                            - بستن
                                        </span>
                                    </div>
                                </div>
                            @endif
                            <div class="section-title text-sm-title title-wide no-after-title-wide mb-0 dt-sl">
                                <h2>کد محصول:{{$product->sku}}</h2>
                            </div>
                            <div class="section-title text-sm-title title-wide no-after-title-wide mb-0 dt-sl">
                                <h2>قیمت : <span class="price">{{number_format($product->price-($product->price*$product->discount)/100)}} تومان</span>
                                </h2>
                            </div>
                            @if($product->qty != 0)
                                <div class="dt-sl mt-4">
                                    <a href="javascript:showfunction()" class="btn-primary-cm btn-with-icon">
                                        <img src="{{asset('assets/img/theme/shopping-cart.png')}}" alt="">
                                        افزودن به سبد خرید
                                    </a>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
            <div class="dt-sn mb-5 px-0 dt-sl pt-0">
                <!-- Start tabs -->
                <section class="tabs-product-info mb-3 dt-sl">
                    <div class="ah-tab-wrapper dt-sl">
                        <div class="ah-tab dt-sl">
                            <a class="ah-tab-item" data-ah-tab-active="true" href=""><i
                                    class="mdi mdi-glasses"></i>نقد و بررسی</a>
                                                      <a class="ah-tab-item" href=""><i class="mdi mdi-format-list-checks"></i>مشخصات</a>
                            <a class="ah-tab-item" href=""><i
                                    class="mdi mdi-comment-text-multiple-outline"></i>نظرات کاربران</a>
                                                      <a class="ah-tab-item" href=""><i class="mdi mdi-comment-question-outline"></i>پرسش و
                                                          پاسخ</a>
                        </div>
                    </div>
                    <div class="ah-tab-content-wrapper product-info px-4 dt-sl">
                        <div class="ah-tab-content dt-sl" data-ah-tab-active="true">
                            <div class="section-title text-sm-title title-wide no-after-title-wide mb-0 dt-sl">
                                <h2>نقد و بررسی</h2>
                            </div>
                            <div class="product-title dt-sl">
                                <h1>{{$product->title}}
                                </h1>
                            </div>
                            <div class="description-product dt-sl mt-3 mb-3">
                                <div class="container">
                                    {!! $product->description !!}
                                </div>
                            </div>
                        </div>
                                              <div class="ah-tab-content params dt-sl">
                                                  <div class="section-title text-sm-title title-wide no-after-title-wide mb-0 dt-sl">
                                                      <h2>مشخصات فنی</h2>
                                                  </div>
                                                  <div class="product-title dt-sl mb-3">
                                                      <h1>{{$product->title}}
                                                      </h1>
                                                  </div>
                                                  <section>
                                                      <h3 class="params-title">مشخصات کلی</h3>
                                                      <ul class="params-list">
                                                      @if(count($values)>0)
                                                          @foreach($values as $value)
                                                              <li>
                                                                  <div class="params-list-key">
                                                                      <span class="d-block">{{$value->groupTitle}}</span>
                                                                  </div>
                                                                  <div class="params-list-value">
                                                                      <span class="d-block">
                                                                         {{$value->valueTitle}}
                                                                      </span>
                                                                  </div>
                                                              </li>
                                                          @endforeach
                                                      @endif
                                                      </ul>
                                                  </section>
                                              </div>
                        <div class="ah-tab-content comments-tab dt-sl">
                            <div class="section-title title-wide no-after-title-wide mb-0 dt-sl">
                                <div class="row">
                                    <h2>امتیاز کاربران به:</h2>
                                    <h4> {{$product->title}}</h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <ul class="content-expert-rating">
                                        <div class="product-title dt-sl mb-3">
                                            @if(count($comments) == 1)
                                                <h3><span
                                                        class="rate-product">{{count($comments)}} نفر نظر داده است</span><span
                                                        class="rate-product green">امتیاز : {{round($star,2)}} از 5</span>@endif
                                                    @if(count($comments) > 1)   <h3><span class="rate-product">{{count($comments)}} نفر نظر داده اند</span><span
                                                            class="rate-product green">امتیاز : {{ round($star,2)}} از 5</span>
                                                    </h3>@endif

                                        </div>
                                        <div class="comments-area dt-sl">
                                            <div class="section-title text-sm-title title-wide no-after-title-wide mb-0 dt-sl">
                                              <div class="row">
                                                  <h2>نظرات کاربران</h2>
                                                  <p class="count-comment"><span>{{count($comments)}}نظر </span></p>
                                              </div>
                                            </div>
                                            <ol class="comment-list">
                                                <!-- #comment-## -->
                                                @foreach($comments as $comment)
                                                    <li>
                                                        <div class="comment-body">
                                                            <div class="row">
                                                                <div class="col-md-7 col-sm-12">
                                                                    <div class="message-light message-light--purchased">
                                                                        خریدار این محصول
                                                                    </div>
                                                                    <ul class="comments-user-shopping">
                                                                        <li>
                                                                            <div class="cell">خریداری شده
                                                                                از:
                                                                            </div>
                                                                            <div class="cell seller-cell">
                                                                    <span
                                                                        class="o-text-blue">{{__('word.storeName')}}</span>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                    @if($comment->recommended==2)

                                                                        <div
                                                                            class="message-light message-light--opinion-negative red">
                                                                            خرید این محصول را توصیه نمی‌کنم
                                                                        </div>
                                                                    @elseif($comment->recommended==1)
                                                                        <div
                                                                            class="message-light message-light--opinion-positive green">
                                                                            خرید این محصول را توصیه می‌کنم
                                                                        </div>
                                                                    @elseif($comment->recommended==3)
                                                                        <div
                                                                            class="message-light message-light--opinion-refuse">
                                                                            نظری ندارم
                                                                        </div>
                                                                    @endif

                                                                </div>
                                                                <div class="col-md-5 col-sm-12 ">
                                                                    <div class="comment-title">
                                                                        {{$comment->title}}
                                                                    </div>
                                                                    <div>
                                                                        {{$comment->body}}
                                                                    </div>
                                                                    @foreach(\Str::of($comment->advantages)->explode(' ') as $advantage)
                                                                        <div
                                                                            class="evaluation-item evaluation-item item--positive cli mt-1">
                                                                            {{$advantage}}
                                                                        </div>
                                                                    @endforeach
                                                                    @foreach(\Str::of($comment->disadvantages)->explode(' ') as $disadvantage)
                                                                        <div
                                                                            class="evaluation-item evaluation-item item--negative cli">
                                                                            {{$disadvantage}}

                                                                        </div>
                                                                    @endforeach
                                                                          <div class="footer">
                                                                              <div class=" row comments-likes">
                                                                                 <span class="p-2"> آیا این نظر برایتان مفید بود؟</span>

                                                                                  <div class="col-6">
                                                                                      <form action="/product/recommend/{{$comment->id}}" method="post">
                                                                                          @csrf
                                                                                          <input type="hidden" value="yes" name="recommend">
                                                                                          <button class="btn-like" @if($comment->recommend>0)data-counter="{{$comment->recommend}}"@endif >بله</button>
                                                                                      </form>
                                                                                  </div>
                                                                                 <div class="col-6">
                                                                                     <form action="/product/recommend/{{$comment->id}}" method="post">
                                                                                         @csrf
                                                                                         <input type="hidden" value="no" name="recommend">
                                                                                         <button class="btn-like" @if($comment->notrecommend>0)data-counter="{{$comment->notrecommend}}"@endif >خیر</button>
                                                                                     </form>
                                                                                 </div>
                                                                              </div>
                                                                          </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                            @endforeach
                                            <!-- #comment-## -->
                                            </ol>
                                        </div>
{{--                                                                              <li>--}}
{{--                                                                                  <div class="cell">طراحی</div>--}}
{{--                                                                                  <div class="cell">--}}
{{--                                                                                      <div class="rating rating--general" data-rate-digit="عالی">--}}
{{--                                                                                          <div class="rating-rate" data-rate-value="100%"--}}
{{--                                                                                               style="width: 70%;"></div>--}}
{{--                                                                                      </div>--}}
{{--                                                                                  </div>--}}
{{--                                                                              </li>--}}
{{--                                                                              <li>--}}
{{--                                                                                  <div class="cell">ارزش خرید</div>--}}
{{--                                                                                  <div class="cell">--}}
{{--                                                                                      <div class="rating rating--general" data-rate-digit="عالی">--}}
{{--                                                                                          <div class="rating-rate" data-rate-value="100%"--}}
{{--                                                                                               style="width: 20%;"></div>--}}
{{--                                                                                      </div>--}}
{{--                                                                                  </div>--}}
{{--                                                                              </li>--}}
{{--                                                                              <li>--}}
{{--                                                                                  <div class="cell">کیفیت ساخت</div>--}}
{{--                                                                                  <div class="cell">--}}
{{--                                                                                      <div class="rating rating--general" data-rate-digit="عالی">--}}
{{--                                                                                          <div class="rating-rate" data-rate-value="100%"--}}
{{--                                                                                               style="width: 100%;"></div>--}}
{{--                                                                                      </div>--}}
{{--                                                                                  </div>--}}
{{--                                                                              </li>--}}
{{--                                                                              <li>--}}
{{--                                                                                  <div class="cell">صدای مزاحم</div>--}}
{{--                                                                                  <div class="cell">--}}
{{--                                                                                      <div class="rating rating--general" data-rate-digit="عالی">--}}
{{--                                                                                          <div class="rating-rate" data-rate-value="100%"--}}
{{--                                                                                               style="width: 100%;"></div>--}}
{{--                                                                                      </div>--}}
{{--                                                                                  </div>--}}
{{--                                                                              </li>--}}
{{--                                                                              <li>--}}
{{--                                                                                  <div class="cell">مصرف انرژی و آب</div>--}}
{{--                                                                                  <div class="cell">--}}
{{--                                                                                      <div class="rating rating--general" data-rate-digit="عالی">--}}
{{--                                                                                          <div class="rating-rate" data-rate-value="100%"--}}
{{--                                                                                               style="width: 100%;"></div>--}}
{{--                                                                                      </div>--}}
{{--                                                                                  </div>--}}
{{--                                                                              </li>--}}
{{--                                                                              <li>--}}
{{--                                                                                  <div class="cell">امکانات و قابلیت ها</div>--}}
{{--                                                                                  <div class="cell">--}}
{{--                                                                                      <div class="rating rating--general" data-rate-digit="عالی">--}}
{{--                                                                                          <div class="rating-rate" data-rate-value="100%"--}}
{{--                                                                                               style="width: 100%;"></div>--}}
{{--                                                                                      </div>--}}
{{--                                                                                  </div>--}}
{{--                                                                              </li>--}}
                                    </ul>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="comments-summary-note">
                                            <span>شما هم می‌توانید در مورد این کالا نظر
                                                بدهید.</span>
                                        <p>برای ثبت نظر، لازم است ابتدا وارد حساب کاربری خود
                                            شوید. اگر این محصول را قبلا از {{__('word.storeName')}} خریده
                                            باشید، نظر
                                            شما به عنوان مالک محصول ثبت خواهد شد.
                                        </p>
                                        <div class="dt-sl mt-2">
                                            <a href="{{route('comment.show',['product'=>$product->sku,'slug'=>$product->slug])}}"
                                               class="btn-primary-cm btn-with-icon">
                                                <i class="mdi mdi-comment-text-outline"></i>
                                                افزودن نظر جدید
                                            </a>
                                        </div>
                                    </div>
                                </div>


                            </div>
                                                  <div class="ah-tab-content dt-sl">
                                                      <div class="section-title title-wide no-after-title-wide dt-sl">
                                                          <h2>پرسش و پاسخ</h2>
                                                          <p class="count-comment">پرسش خود را در مورد محصول مطرح نمایید</p>
                                                      </div>
                                                      <div class="form-question-answer dt-sl mb-3">
                                                          <form action="">
                                                              <textarea class="form-control mb-3" rows="5"></textarea>
                                                              <button class="btn btn-dark float-right ml-3" disabled="">ثبت پرسش</button>
                                                              <div class="custom-control custom-checkbox float-right mt-2">
                                                                  <input type="checkbox" class="custom-control-input" id="customCheck3">
                                                                  <label class="custom-control-label" for="customCheck3">اولین پاسخی که به
                                                                      پرسش من داده شد، از طریق ایمیل به من اطلاع دهید.</label>
                                                              </div>
                                                          </form>
                                                      </div>
                                                      <div class="comments-area default">
                                                          <div class="section-title text-sm-title title-wide no-after-title-wide mt-5 mb-0 dt-sl">
                                                              <h2>پرسش ها و پاسخ ها</h2>
                                                              <p class="count-comment">123 پرسش</p>
                                                          </div>
                                                          <ol class="comment-list">
                                                              <!-- #comment-## -->
                                                              <li>
                                                                  <div class="comment-body">
                                                                      <div class="comment-author">
                                                                          <span class="icon-comment">?</span>
                                                                          <cite class="fn">حسن</cite>
                                                                          <span class="says">گفت:</span>
                                                                          <div class="commentmetadata">
                                                                              <a href="#">
                                                                                  اسفند ۲۰, ۱۳۹۶ در ۹:۴۱ ب.ظ
                                                                              </a>
                                                                          </div>
                                                                      </div>


                                                                      <p>لورم ایپسوم متن ساختگی</p>

                                                                      <div class="reply"><a class="comment-reply-link" href="#">پاسخ</a></div>
                                                                  </div>
                                                              </li>
                                                              <!-- #comment-## -->
                                                              <li>
                                                                  <div class="comment-body">
                                                                      <div class="comment-author">
                                                                          <span class="icon-comment">?</span>
                                                                          <cite class="fn">رضا</cite>
                                                                          <span class="says">گفت:</span>
                                                                          <div class="commentmetadata">
                                                                              <a href="#">
                                                                                  اسفند ۲۰, ۱۳۹۶ در ۹:۴۲ ب.ظ
                                                                              </a>
                                                                          </div>
                                                                      </div>
                                                                      <p>
                                                                          لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از
                                                                          صنعت چاپ و با استفاده از طراحان گرافیک است.
                                                                      </p>

                                                                      <div class="reply"><a class="comment-reply-link" href="#">پاسخ</a></div>
                                                                  </div>
                                                                  <ol class="children">
                                                                      <li>
                                                                          <div class="comment-body">
                                                                              <div class="comment-author">
                                                                                      <span
                                                                                          class="icon-comment mdi mdi-lightbulb-on-outline"></span>
                                                                                  <cite class="fn">بهرامی راد</cite> <span
                                                                                      class="says">گفت:</span>
                                                                                  <div class="commentmetadata">
                                                                                      <a href="#">
                                                                                          اسفند ۲۰, ۱۳۹۶ در ۹:۴۷ ب.ظ
                                                                                      </a>
                                                                                  </div>
                                                                              </div>
                                                                              <p>لورم ایپسوم متن ساختگی با تولید سادگی
                                                                                  نامفهوم از صنعت چاپ و با استفاده از
                                                                                  طراحان گرافیک است.
                                                                                  چاپگرها و متون بلکه روزنامه و مجله در
                                                                                  ستون و سطرآنچنان که لازم است و برای
                                                                                  شرایط فعلی تکنولوژی
                                                                                  مورد نیاز و کاربردهای متنوع با هدف بهبود
                                                                                  ابزارهای کاربردی می باشد.</p>

                                                                              <div class="reply"><a class="comment-reply-link"
                                                                                                    href="#">پاسخ</a></div>
                                                                          </div>
                                                                          <ol class="children">
                                                                              <li>
                                                                                  <div class="comment-body">
                                                                                      <div class="comment-author">
                                                                                          <span class="icon-comment">?</span>
                                                                                          <cite class="fn">محمد</cite>
                                                                                          <span class="says">گفت:</span>
                                                                                          <div class="commentmetadata">
                                                                                              <a href="#">
                                                                                                  خرداد ۳۰, ۱۳۹۷ در ۸:۵۳ ق.ظ
                                                                                              </a>
                                                                                          </div>
                                                                                      </div>
                                                                                      <p>عالیه</p>

                                                                                      <div class="reply"><a class="comment-reply-link"
                                                                                                            href="#">پاسخ</a></div>
                                                                                  </div>
                                                                                  <ol class="children">
                                                                                      <li>
                                                                                          <div class="comment-body">
                                                                                              <div class="comment-author">
                                                                                                      <span
                                                                                                          class="icon-comment mdi mdi-lightbulb-on-outline"></span>
                                                                                                  <cite class="fn">اشکان</cite>
                                                                                                  <span class="says">گفت:</span>
                                                                                                  <div class="commentmetadata">
                                                                                                      <a href="#">
                                                                                                          خرداد ۳۰, ۱۳۹۷ در ۸:۵۳ ق.ظ
                                                                                                      </a>
                                                                                                  </div>
                                                                                              </div>
                                                                                              <p>لورم ایپسوم متن ساختگی با
                                                                                                  تولید سادگی نامفهوم از
                                                                                                  صنعت چاپ و با استفاده از
                                                                                                  طراحان
                                                                                                  گرافیک است. چاپگرها و
                                                                                                  متون بلکه روزنامه و مجله
                                                                                                  در ستون و سطرآنچنان که
                                                                                                  لازم است و
                                                                                                  برای شرایط فعلی تکنولوژی
                                                                                                  مورد نیاز و کاربردهای
                                                                                                  متنوع با هدف بهبود
                                                                                                  ابزارهای
                                                                                                  کاربردی می باشد.</p>

                                                                                              <div class="reply"><a class="comment-reply-link"
                                                                                                                    href="#">پاسخ</a>
                                                                                              </div>
                                                                                          </div>
                                                                                      </li>
                                                                                      <!-- #comment-## -->
                                                                                  </ol>
                                                                                  <!-- .children -->
                                                                              </li>
                                                                              <!-- #comment-## -->
                                                                          </ol>
                                                                          <!-- .children -->
                                                                      </li>
                                                                      <!-- #comment-## -->
                                                                  </ol>
                                                                  <!-- .children -->
                                                              </li>
                                                          </ol>
                                                      </div>
                                                  </div>

                        </div>        </div>
                </section>
                <!-- End tabs -->
            </div>
            <!-- End Product -->



        </div>
    </main>
@endsection
@section('scripts')
    <script src="{{asset('assets/js/vendor/jquery.fancybox.min.js')}}"></script>

    <script>
        function showfunction() {
            var product = @json($product);
            var att_groups = @json($attGroup);
            var att_group = [];
            let attId = 0;
            let id = null;
            var title = '';
            number = 0;

            let productSku = product.sku;
            id = document.querySelector('input[name = "data"]:checked');

            for (var i = 0; i < att_groups.length; i++) {
                for (var j = 0; j < product.att_values.length; j++) {
                    if (product.att_values[j].attributeGroup_id == att_groups[i].id) {
                        if ( att_groups[i].number == 1) {
                         number = 1;
                        }
                        if (id == null && att_groups[i].number == 1) {
                            Swal.fire({
                                icon: 'error',
                                title: 'خطا',
                                text: 'لطفا ' + att_groups[i].title + ' کالا را انتخاب کنید',
                                showConfirmButton: true,
                                timer: 3000
                            })
                        }
                        attId = att_groups[i].id
                        title = att_groups[i].title
                        att_group = att_groups[i]
                    }
                }
            }
            if(number  == 1){
                 id = document.querySelector('input[name = "data"]:checked');
                document.location.href = "/add/" + productSku + "/" + id.value;
            }
            else{
                document.location.href = "/add/" + productSku;
            }


            // if (id == null && attId !== 0) {
            //     // Swal.fire({
            //     //     icon: 'error',
            //     //     title:'خطا',
            //     //     text: 'لطفا ' + title  + ' کالا را انتخاب کنید',
            //     //     showConfirmButton: true,
            //     //     timer: 3000
            //     // })
            // } else if (attId !== 0) {
            //     id = document.querySelector('input[name = "data"]:checked');
            //     document.location.href = "/add/" + productSku + "/" + id.value;

            // } else if (attId === 0) {
            //     document.location.href = "/add/" + productSku;
            // }


        }
    </script>
@endsection

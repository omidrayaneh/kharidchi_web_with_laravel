@extends('layouts.main-master')
@section('title')
    <title>فروشگاه اینترنتی {{__('word.storeName')}} - جستجوی {{$data}}</title>
@endsection
@section('style-search')
    <link rel="stylesheet" href="{{asset('assets/css/vendor/nouislider.min.css')}}">
@endsection
@section('content')
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">
            @if(!empty($category))
                <main-search :data="'{{$data}}'" :category="{{$category}}"></main-search>
            @else
                <main-search :data="'{{$data}}'" :category="{{$category ='null'}}"></main-search>
        @endif


        <!-- End Content -->
        </div>
    </main>

@endsection
@section('scripts')
    <script src="{{asset('assets/js/vendor/nouislider.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/wNumb.js')}}"></script>
    <script src="{{asset('assets/js/vendor/ResizeSensor.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/theia-sticky-sidebar.min.js')}}"></script>
@endsection

@extends('layouts.master')

@section('content')

    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">

            <div class="row">
                <div class="col-xl-4 col-lg-5 col-md-7 col-12 mx-auto">
                    <div class="form-ui dt-sl dt-sn pt-4">
                        <div class="section-title title-wide mb-1 no-after-title-wide">
                            <h2 class="font-weight-bold">تایید شماره</h2>
                        </div>
                        <div class="message-light">
                             برای شماره همراه {{$user->mobile}}  کد تایید ارسال گردید
                            <a href="{{route('register')}}" class="btn-link-border">
                                ویرایش شماره
                            </a>
                        </div>
                        <form method="post" action="{{route('verify-user')}}">
                            @csrf
                            <input type="hidden" name="userId" value="{{ $user->id }}">

                            <div class="form-row-title">
                                <h3>کد تایید را وارد کنید</h3>
                            </div>
                            <div class="form-row">
                                <div class="numbers-verify">
                                    <div  class="lines-number-input">
                                        <input name="a" type="text" class="line-number" maxlength="1" autofocus="">
                                        <input name="b" type="text" class="line-number" maxlength="1">
                                        <input name="c" type="text" class="line-number" maxlength="1">
                                        <input name="d" type="text" class="line-number" maxlength="1">
                                        <input name="e" type="text" class="line-number" maxlength="1">
                                    </div>
                                </div>
                            </div>

                    </div>
                            <div class="form-row mt-2">
                                <span class="text-primary">دریافت مجدد کد تایید</span>(
                                <p id="countdown-verify-end"></p>)
                            </div>
                            <div class="form-row mt-3">
                                <button type="submit" class="btn-primary-cm btn-with-icon mx-auto w-100">
                                    <i class="mdi mdi-login-variant"></i>
                                    ارسال
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection
@section('script')
    <script src="../assets/js/vendor/countdown.min.js"></script>
@endsection

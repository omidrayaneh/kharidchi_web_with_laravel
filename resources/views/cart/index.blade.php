@extends('layouts.main-master')
@section('title')
    <title>سبد خرید | فروشگاه اینترنتی {{__('word.storeName')}}  </title>
@endsection
@push('accessed-button')
    <button  onclick="window.location='{{ url("shopping-payment") }}'" class="btn-primary-cm btn-with-icon w-100 text-center pr-0 pl-0">
        <i class="mdi mdi-arrow-left"></i>
        تایید و ادامه ثبت سفارش
    </button>
@endpush
@section('content')

    @if(\Cart::isEmpty())
        <main class="main-content dt-sl mt-4 mb-3">
            <div class="container main-container">

                <div class="row">
                    <div class="col-12">
                        <div class="dt sl dt-sn pt-3 pb-5">
                            <div class="cart-page cart-empty">
                                <div class="circle-box-icon">
                                    <i class="mdi mdi-cart-remove"></i>
                                </div>
                                <p class="cart-empty-title">سبد خرید شما خالیست!</p>
                                <p>می‌توانید برای مشاهده محصولات بیشتر به صفحات زیر بروید:</p>
                                <div class="cart-empty-links mb-5">
                                    <a href="#" class="border-bottom-dt">لیست مورد علاقه من</a>
                                    <a href="#" class="border-bottom-dt">محصولات شگفت‌انگیز</a>
                                    <a href="#" class="border-bottom-dt">محصولات پرفروش روز</a>
                                </div>
                                <a href="{{route('/')}}" class="btn-primary-cm">ادامه خرید در {{__('word.storeName')}}</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="dt-sn mt-4">
                    <div class="row">
                        <!-- Start Product-Slider -->
                        <div class="col-12">
                            <div class="features-checkout-slider carousel-md owl-carousel owl-theme">
                                <div class="item">
                                    <a href="#">
                                        <img src="./assets/img/svg/delivery.svg" class="img-fluid" alt="">
                                        <div class="title-feature-checkout-slider">تحویل اکسپرس</div>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <img src="./assets/img/svg/contact-us.svg" class="img-fluid" alt="">
                                        <div class="title-feature-checkout-slider">پشتیبانی ۲۴ ساعته</div>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <img src="./assets/img/svg/payment-terms.svg" class="img-fluid" alt="">
                                        <div class="title-feature-checkout-slider">پرداخت در محل</div>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <img src="./assets/img/svg/return-policy.svg" class="img-fluid" alt="">
                                        <div class="title-feature-checkout-slider">۷ روز ضمانت بازگشت</div>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <img src="./assets/img/svg/origin-guarantee.svg" class="img-fluid" alt="">
                                        <div class="title-feature-checkout-slider">ضمانت اصل بودن کالا</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- End Product-Slider -->
                    </div>
                </div>




            </div>
        </main>
    @else
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <div>{{session('success')}}</div>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <div>{{session('error')}}</div>
                </div>
            @endif
            <div class="row">
                <div class="col-xl-9 col-lg-8 col-md-12 col-sm-12 mb-2 px-0">
                    <nav class="tab-cart-page">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                               role="tab" aria-controls="nav-home" aria-selected="true">  @if(Cart::getTotalQuantity()!=0)سبد خرید<span class="count-cart">{{\Cart::getTotalQuantity()}}</span>@endif</a>
                        </div>
                    </nav>
                </div>
                <div class="col-12">
                    <div class="tab-content" id="nav-tabContent">
                        @if(!\Cart::isempty())
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                 aria-labelledby="nav-home-tab">
                                <div class="row">
                                    <div class="col-xl-9 col-lg-8 col-12 px-0">
                                        <div class="table-responsive checkout-content dt-sl">
                                            <div class="checkout-header checkout-header--express">
                                                <span class="checkout-header-title">ارسال عادی</span>
                                                <span class="checkout-header-extra-info">({{\Cart::getTotalQuantity()}} کالا)</span>
                                            </div>
                                            <table class="table table-cart">
                                                <tbody>
                                                @foreach(\Cart::getcontent() as $item)
                                                <tr class="checkout-item">
                                                    <td>
                                                        <img width="100" src="{{$item->attributes->path}}" >
                                                        <button class="checkout-btn-remove" onclick="event.preventDefault();
                                                            document.getElementById('remove-cart-item_{{$item->id}}').submit();"><span class="red">&times;</span></button>
                                                        <form id="remove-cart-item_{{$item->id}}" action="{{ route('cart.remove', ['id' => $item->id]) }}" method="post" style="display: none;">
                                                            @csrf
                                                        </form>

                                                    </td>
                                                    <td class="text-right">
                                                        <a href="#">
                                                            <h3 class="checkout-title">
                                                                {{$item->name}}
                                                            </h3>
                                                        </a>
                                                        <p class="checkout-dealer">
                                                            فروشنده: موبایدک
                                                        </p>
{{--                                                        گارانتی 18 ماهه کاوش تیم                                                    --}}
                                                      <p class="checkout-guarantee"></p>
                                                        @if($item->attributes->attValue)
                                                        <div class="checkout-variant checkout-variant--color">
                                                            <span class="checkout-variant-title">{{$item->attributes->attGroup}} :</span>
                                                            <span class="checkout-variant-value">
                                                                    {{$item->attributes->attValue}}
                                                                @if($item->attributes->color)
                                                                    <div class="checkout-variant-shape"
                                                                         style="background-color: {{$item->attributes->color}}"></div>
                                                                @endif
                                                                </span>
                                                        </div>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <p class="mb-0">تعداد</p>
                                                        <div class="number-input">
{{--                                                            <button data-toggle="tooltip" title="کم" onclick="event.preventDefault();--}}
{{--                                                                document.getElementById('remove-cart-item_{{$item->id}}').submit();"></button>--}}
{{--                                                            <form id="remove-cart-item_{{$item->id}}" action="{{ route('cart.remove', ['id' => $item->id]) }}" method="post" style="display: none;">--}}
{{--                                                                @csrf--}}
{{--                                                            </form>--}}
                                                            <input class="quantity" disabled name="quantity"
                                                                   value="{{$item->quantity}}" type="number">
{{--                                                           @if($item->attributes->attValueId!= null)--}}

{{--                                                                <button data-toggle="tooltip" title="اضافه"  onclick="event.preventDefault();--}}
{{--                                                                    document.getElementById('add-cart-item_{{$item->attributes->productId}}').submit();" class="plus"></button>--}}
{{--                                                                <form id="add-cart-item_{{$item->attributes->productId}}" action="{{ route('add', ['id' => $item->attributes->productId,'idd'=> $item->attributes->attValueId]) }}" method="get" style="display: none;">--}}
{{--                                                                    @csrf--}}
{{--                                                                </form>--}}
{{--                                                            @else--}}
{{--                                                                <button data-toggle="tooltip" title="اضافه"  onclick="event.preventDefault();--}}
{{--                                                                    document.getElementById('add-cart-item_{{$item->id}}').submit();" class="plus"></button>--}}
{{--                                                                <form id="add-cart-item_{{$item->id}}" action="{{ route('add', ['id' => $item->id]) }}" method="get" style="display: none;">--}}
{{--                                                                    @csrf--}}
{{--                                                                </form>--}}
{{--                                                            @endif--}}
                                                        </div>
                                                    </td>
                                                    <td><strong>{{number_format($item->price)}} تومان</strong></td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                  @include('layouts.shopping-left-side')
                                </div>
                            </div>
                        @endif


                    </div>
                </div>
            </div>
        </div>
    </main>
    @endif
@endsection
@section('scripts')
    <script>

       function myFunction(){
           var price = document.getElementById("cost_price").value;
           sessionStorage.setItem("cost_price", price);
       }
    </script>
@endsection

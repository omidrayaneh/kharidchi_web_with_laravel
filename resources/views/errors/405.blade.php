@extends('layouts.main-master')
@section('title')
    <title>{{__('word.storeName')}}</title>
@endsection
@section('content')
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">

            <div class="row">
                <div class="col-12">
                    <div class="dt-sl dt-sn pt-3 pb-5">
                        <div class="error-page text-center">
                            <h1>لطفا با پشتیبانی تماس بگیرید!</h1>
                            <a href="{{route('/')}}" class="btn-primary-cm">ادامه خرید در {{__('word.storeName')}}</a>
                            <img src="{{asset('assets/img/theme/404.svg')}}" class="img-fluid" width="60%" alt="">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection

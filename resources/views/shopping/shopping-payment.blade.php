@extends('layouts.shopping')
@section('title')
    <title>تایید پرداخت | فروشگاه اینترنتی {{__('word.storeName')}}  </title>
@endsection
@push('accessed-button')
    @if(Session::has('coupon'))
        <input type="hidden" name="coupon_discount" value="{{session('coupon')['id']}}">
        <button onclick="window.location='{{ url("order-verify/".session('coupon')['id']) }}'"
                class="btn-primary-cm btn-with-icon w-100 text-center pr-0 pl-0">
            @else
                <button onclick="window.location='{{ url("order-verify") }}'"
                        class="btn-primary-cm btn-with-icon w-100 text-center pr-0 pl-0">
                    @endif
                    <i class="mdi mdi-arrow-left"></i>
                    تایید و ادامه ثبت سفارش
                </button>
                @endpush
            @section('content')

                <div class="wrapper shopping-page">
                    <!-- Start header-shopping -->
                    <header class="header-shopping dt-sl">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 text-center pt-2">
                                    <div class="header-shopping-logo dt-sl">
                                        @if(isset($logo->path))
                                            <a href="{{route('/')}}">
                                                <img src="{{asset($logo->path)}}" alt="{{$logo->title}}">
                                            </a>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <ul class="checkout-steps">
                                        <li>
                                            <a href="#" class="active">
                                                <span>اطلاعات ارسال</span>
                                            </a>
                                        </li>
                                        <li class="active">
                                            <a href="#" class="active">
                                                <span>پرداخت</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>اتمام خرید و ارسال</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </header>
                    <!-- End header-shopping -->

                    <!-- Start main-content -->
                    <main class="main-content dt-sl mt-4 mb-3">
                        <div class="container main-container">

                            <div class="row">
                                <div class="cart-page-content col-xl-9 col-lg-8 col-12 px-0">
                                    <section class="page-content dt-sl">
                                        <div
                                            class="section-title text-sm-title title-wide no-after-title-wide mb-0 px-res-1">
                                            <h2>انتخاب شیوه پرداخت</h2>
                                        </div>
                                        <form method="post" id="shipping-data-form" class="dt-sn pt-3 pb-3 mb-4">
                                            <div class="checkout-pack">
                                                <div class="row">
                                                    <div class="checkout-time-table checkout-time-table-time">
                                                        <div class="col-12">
                                                            <div
                                                                class="radio-box custom-control custom-radio pl-0 pr-3">
                                                                <input type="radio" class="custom-control-input"
                                                                       name="post-pishtaz" id="1" value="1" checked>
                                                                <label for="1" class="custom-control-label">
                                                                    <i class="mdi mdi-credit-card-outline checkout-additional-options-checkbox-image"></i>
                                                                    <div class="content-box">
                                                                        <div
                                                                            class="checkout-time-table-title-bar checkout-time-table-title-bar-city">
                                                                            پرداخت اینترنتی
                                                                            هوشمند {{__('word.storeName')}}
                                                                            <span class="help-sn" data-toggle="tooltip"
                                                                                  data-html="true"
                                                                                  data-placement="bottom"
                                                                                  title="<div class='help-container is-left'><div class='help-arrow'></div><p class='help-text'>با پرداخت اینترنتی، سفارش شما با اولویت بیشتری نسبت به پرداخت در محل پردازش و ارسال می شود. در صورت پرداخت ناموفق هزینه کسر شده حداکثر طی ۷۲ ساعت به حساب شما بازگردانده می‌شود.</p></div>">
                                                                    <span class="mdi mdi-information-outline"></span>
                                                                </span>
                                                                        </div>
                                                                        <ul class="checkout-time-table-subtitle-bar">
                                                                            <li>
                                                                                آنلاین با تمامی کارت‌های بانکی
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
{{--                                                        <div class="col-12">--}}
{{--                                                            <div class="radio-box custom-control custom-radio pl-0 pr-3">--}}
{{--                                                                <input type="radio" class="custom-control-input" name="post-pishtaz" id="2" value="2">--}}
{{--                                                                <label for="2" class="custom-control-label">--}}
{{--                                                                    <i class="mdi mdi-credit-card-multiple-outline checkout-additional-options-checkbox-image"></i>--}}
{{--                                                                    <div class="content-box">--}}
{{--                                                                        <div--}}
{{--                                                                            class="checkout-time-table-title-bar checkout-time-table-title-bar-city">--}}
{{--                                                                            پرداخت در محل--}}
{{--                                                                        </div>--}}
{{--                                                                    </div>--}}
{{--                                                                </label>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}

                                                    </div>

                                                </div>
                                            </div>
                                        </form>
                                        <div
                                            class="section-title text-sm-title title-wide no-after-title-wide mb-0 px-res-1">
                                            <h2>خلاصه سفارش</h2>
                                        </div>
                                        <div class="dt-sn pt-3 pb-5">
                                            <div class="checkout-order-summary">
                                                <div class="accordion checkout-order-summary-item"
                                                     id="accordionExample">
                                                    <div class="card pt-sl-res">
                                                        <div class="card-header checkout-order-summary-header"
                                                             id="headingOne">
                                                            <h2 class="mb-0">
                                                                <button class="btn btn-link" type="button"
                                                                        data-toggle="collapse"
                                                                        data-target="#collapseOne" aria-expanded="false"
                                                                        aria-controls="collapseOne">
                                                                    <div class="checkout-order-summary-row">
                                                                        <div
                                                                            class="checkout-order-summary-col checkout-order-summary-col-post-time">
                                                                            مرسوله
                                                                            <span class="fs-sm">({{count(\Cart::getContent())}} کالا)</span>
                                                                        </div>
                                                                        <div
                                                                            class="checkout-order-summary-col checkout-order-summary-col-how-to-send">
                                                                            <span class="dl-none-sm">نحوه ارسال</span>
                                                                            <span class="dl-none-sm">
                                                                    پست پیشتاز با ظرفیت اختصاصی برای {{__('word.storeName')}}
                                                                </span>
                                                                        </div>
                                                                        <div
                                                                            class="checkout-order-summary-col checkout-order-summary-col-how-to-send">
                                                                            <span>ارسال از</span>
                                                                            <span class="fs-sm">
                                                                    2 روز کاری
                                                                </span>
                                                                        </div>
                                                                        <div
                                                                            class="checkout-order-summary-col checkout-order-summary-col-shipping-cost">
                                                                            <span>هزینه ارسال</span>
                                                                            <span class="fs-sm">
                                                                    رایگان
                                                                </span>
                                                                        </div>
                                                                    </div>
                                                                    <i class="mdi mdi-chevron-down icon-down"></i>
                                                                </button>
                                                            </h2>
                                                        </div>

                                                        <div id="collapseOne" class="collapse"
                                                             aria-labelledby="headingOne"
                                                             data-parent="#accordionExample">
                                                            <div class="card-body">
                                                                <div class="box">
                                                                    <div class="row">
                                                                        @foreach(\Cart::getContent() as $product)
                                                                            <div
                                                                                class="col-lg-3 col-md-4 col-sm-6 col-12">
                                                                                <div class="product-box-container">
                                                                                    <div
                                                                                        class="product-box product-box-compact">
                                                                                        <a class="product-box-img">
                                                                                            <img
                                                                                                src="{{$product->attributes->path}}">
                                                                                        </a>
                                                                                        <div class="product-box-title">
                                                                                            {{$product->name}}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-4 ">
                                            <div class="col-sm-12 col-12 ">
                                                <div class="dt-sn pt-3 pb-3 px-res-1">
                                                    <div
                                                        class="section-title text-sm-title title-wide no-after-title-wide mb-0">
                                                        <h2>استفاده از کد تخفیف {{__('word.storeName')}}
                                                            <span class="help-sn" data-toggle="tooltip" data-html="true"
                                                                  data-placement="bottom"
                                                                  title="<div class='help-container is-left'><div class='help-arrow'></div><p class='help-text'>بعد از نهایی شدن سفارش کد تخفیف را ثبت نمایید. بعد از ثبت کد تخفیف امکان بازگشت و یا تغییر سبد وجود نخواهد داشت. در صورت تغییر سفارش، کد تخفیف از بین خواهد رفت و امکان اعمال مجدد آن وجود ندارد</p></div>">
                                                    <span class="mdi mdi-information-outline"></span>
                                                </span>
                                                        </h2>
                                                    </div>
                                                    <p>با ثبت کد تخفیف، مبلغ کد تخفیف از “مبلغ قابل پرداخت” کسر
                                                        می‌شود.</p>
                                                    <div class="form-ui">
                                                        <form method="post" action="{{route('add.coupon')}}">
                                                            @csrf
                                                            <div class="row text-center">
                                                                <div class="col-xl-8 col-lg-12 px-0">
                                                                    <div class="form-row">
                                                                        <input type="text" name="code" required
                                                                               class="input-ui pr-2"
                                                                               placeholder="کوپن تخفیف را وارد کنید">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-4 col-lg-12 px-0">
                                                                    <button class="btn btn-primary mt-res-1">ثبت کد
                                                                        تخفیف
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mt-5">
                                            <a href="#" class="float-right border-bottom-dt"><i
                                                    class="mdi mdi-chevron-double-right"></i>بازگشت به شیوه ارسال</a>
                                            <a href="#" class="float-left border-bottom-dt">ثبت نهایی سفارش<i
                                                    class="mdi mdi-chevron-double-left"></i></a>
                                        </div>
                                    </section>
                                </div>
                                @include('layouts.shopping-left-side')
                            </div>

                        </div>
                    </main>
                    <!-- End main-content -->

                    <!-- Start mini-footer -->
                    <footer class="mini-footer dt-sl">
                        <div class="container main-container">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 text-left">
                                    <i class="mdi mdi-phone-outline"></i>
                                    شماره تماس : <a href="#">
                                        ۶۱۹۳۰۰۰۰
                                        - ۰۲۱
                                    </a>
                                </div>
                                <div class="col-md-6 col-sm-12 text-right">
                                    <i class="mdi mdi-email-outline"></i>
                                    آدرس ایمیل : <a href="#">info@gmail.com</a>
                                </div>
                                <div class="col-12 text-center mt-2">
                                    <p class="text-secondary text-footer">
                                        استفاده از کارت هدیه یا کد تخفیف، درصفحه ی پرداخت امکان پذیر است.
                                    </p>
                                </div>
                                <div class="col-12 text-center">
                                    <div class="copy-right-mini-footer">
                                        Copyright © 2021 Mobayadak
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    <!-- End mini-footer -->

                </div>
        @endsection

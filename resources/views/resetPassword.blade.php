@extends('layouts.master')
@section('title')
    <title>تغییر رمز عبور</title>
@endsection

@section('content')

<main class="main-content dt-sl mt-4 mb-3">
    <div class="container main-container">

        <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-7 col-12 mx-auto">
                <div class="form-ui dt-sl dt-sn pt-4">
                    <div class="section-title title-wide mb-1 no-after-title-wide">
                        <h2 class="font-weight-bold">تغییر رمز عبور</h2>
                    </div>
                    <form method="post" action="{{route('reset-password')}}">
                        @csrf
                        <input type="hidden" name="user" value="{{ $user }}">
                        <div class="form-row-title">
                            <h3>رمز عبور جدید</h3>
                        </div>
                        <div class="form-row with-icon">
                            <input minlength="8" name="password" id="password"  type="password" class="input-ui pr-2" placeholder="رمز عبور خود را وارد نمایید"
                                   required
                                   oninput="(function(e){e.setCustomValidity(''); return !e.validity.valid && e.setCustomValidity(' ')})(this)"
                                   oninvalid="this.setCustomValidity('لطفا  رمز عبور را وارد کنید.')">
                            <i class="mdi mdi-lock-reset"></i>
                        </div>
                        <div class="form-row-title">
                            <h3>تکرار رمز عبور جدید <a>( حداقل 8 کاراکتر )</a></h3>
                        </div>
                        <div class="form-row with-icon">
                            <input minlength="8" type="password" name="confirm_password" id="confirm_password"  class="input-ui pr-2" placeholder="رمز عبور خود را وارد نمایید"
                                   required
                                   oninput="(function(e){e.setCustomValidity(''); return !e.validity.valid && e.setCustomValidity(' ')})(this)"
                                   oninvalid="this.setCustomValidity('لطفا تکرار رمز عبور را وارد کنید.')">
                            <i class="mdi mdi-lock-reset"></i>
                        </div>
                        <div class="form-row mt-3">
                            <button class="btn-primary-cm btn-with-icon mx-auto w-100">
                                <i class="mdi mdi-lock-reset"></i>
                                تغییر رمز عبور
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</main>
@endsection

@section('script')
    <script >
        var password = document.getElementById("password")
            , confirm_password = document.getElementById("confirm_password");

        function validatePassword(){
            if(password.value != confirm_password.value) {
                confirm_password.setCustomValidity("Passwords Don't Match");
            } else {
                confirm_password.setCustomValidity('');
            }
        }

        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;
    </script>
@endsection

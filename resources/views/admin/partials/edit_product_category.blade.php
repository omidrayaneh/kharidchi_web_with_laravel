@if(isset($selected_category))
    @foreach($categories as $sub_category)
        <option value="{{$sub_category->id}}" @if($selected_category->id==$sub_category->id) selected @endIf>{{str_repeat(' --- ',$level)}}{{$sub_category->name}}</option>
        @if(count($sub_category->childrenCategories)>0)
            @include('admin.partials.edit_post_category',['categories'=>$sub_category->childrenCategories,'level'=>$level+1,'selected_category'])
        @endif
    @endforeach
@else
    @foreach($categories as $sub_category)
        <option value="{{$sub_category->id}}">{{str_repeat(' --- ',$level)}}{{$sub_category->name}}</option>
        @if(count($sub_category->childrenCategories)>0)
            @include('admin.partials.edit_post_category',['categories'=>$sub_category->childrenCategories,'level'=>$level+1])
        @endif
    @endforeach
@endif
@foreach($categories as $sub_category)
    <tr role="row" class="odd">
        <td><a href="{{route('categories.edit', $sub_category->slug)}}">{{$sub_category->sku}}</a></td>
        <td>{{str_repeat(' --- ', $level)}}{{$sub_category->title}}</td>
        <td>{{$sub_category->status}}</td>
        <td>{{$sub_category->ended}}</td>
        <td>{{$sub_category->created_at}}</td>
        <td>{{$sub_category->updated_at}}</td>
        <td>
            <div class="row">
                <a class="btn-delete blue mt-auto ml-1"
                   href="{{route('categories.indexSetting', $sub_category->slug)}}">
                    <i class="fa fa-plus green" data-toggle="tooltip" title="افزودن"></i>
                </a>
                |
                <a class="btn-delete blue mt-auto ml-1 mr-1"
                   href="{{route('categories.edit', $sub_category->slug)}}">
                    <i class="fa fa-edit" data-toggle="tooltip" title="ویرایش"></i>
                </a>
                |
               {{-- <form method="post" class="mt-sm"
                      action="/admin/categories/{{$sub_category->slug}}">
                    @csrf
                    <input type="hidden" name="_method"
                           value="DELETE">
                    <button type="submit" class="btn-delete">
                        <i class="fa fa-trash red" data-toggle="tooltip" title="حذف"></i>
                    </button>
                </form>--}}
                <button data-id="{{ $sub_category->slug }}"  class="btn-delete deleteRecord">
                    <i class="fa fa-trash red" data-toggle="tooltip" title="حذف"></i>
                </button>
            </div>
        </td>
    </tr>
    @if(count($sub_category->childrenRecursive) > 0)
        @include('admin.partials.category_list', ['categories' => $sub_category->childrenRecursive, 'level' => $level+1])
    @endif
@endforeach

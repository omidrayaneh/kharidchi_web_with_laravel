@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">متا</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">ایجاد متا جدید</h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    @if($errors->any())
                        <ol  style=" list-style-type: persian" >
                            <div class="  alert alert-danger mr-1">
                                @foreach($errors->all() as $error)
                                    <li >{{$error}}</li>
                                @endforeach
                            </div>
                        </ol>
                    @endif
                    <form  method="post" action="/admin/metas">
                        @csrf
                        <div class="card-body offset-md-2 col-md-8">
                            <div class="form-group">
                                <label>عنوان متا</label>
                                <input name="title" type="text" class="form-control" placeholder="عنوان متا را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>توضیحات متا</label>
                                <input name="description" type="text" class="form-control" placeholder="توضیحات متا را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>کلمات کلیدی متا</label>
                                <input name="keyword" type="text" class="form-control" placeholder="کلمات کلیدی متا را وارد کنید ...">
                            </div>
                            <div class="form-group ">
                                <label>وضعیت انتشار<br>
                                    <input   name="status" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                            data-offstyle="danger"> </label>
                            </div>
                            <button type="submit" class="btn btn-success pull-left ">ذخیره</button>
                        </div>

                    </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection


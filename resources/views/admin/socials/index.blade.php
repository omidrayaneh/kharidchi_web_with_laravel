@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">شبکه های اجتماعی</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">جدول آدرس شبکه های اجتماعی</h3>
                    <div class="text-left">
                        <a class="btn btn-app" href="{{route('socials.create')}}">
                            <i class="fa fa-plus green"></i> جدید
                        </a>
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive">
                    <table id="example2" class="table table-bordered table-hover ">
                        <thead>
                        <tr role="row" class="text-center">
                            <th>#</th>
                            <th scope="col">اینیستاگرام</th>
                            <th scope="col">توئیتر</th>
                            <th scope="col">لینکداین</th>
                            <th scope="col">تلگرام</th>
                            <th scope="col">عملیات</th>
                        </thead>
                        <tbody>
                        @foreach($socials as $key=>$social)
                            <tr role="row" class="odd">
                                <td>{{$socials->currentPage() == 1 ? $key+1: (($socials->perPage()*($socials->currentPage()-1)))+$key+1}}</td>
                                <td>{{$social->instagram}}</td>
                                <td>{{$social->twitter }}</td>
                                <td>{{$social->linkedin }}</td>
                                <td>{{$social->telegram }}</td>
                                <td>
                                    @if($social->status == 1)
                                        <span class="float badge bg-success">منتشر شده</span>
                                    @else
                                        <span class="float badge bg-danger">منتشر نشده</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="text-center">
                                        <a class="btn-delete blue mt-auto ml-1 mr-1"
                                           href="{{route('socials.edit', $social->id)}}">
                                            <i class="fa fa-edit" data-toggle="tooltip" title="ویرایش"></i>
                                        </a>
                                        |
                                        <button data-id="{{ $social->id }}" class="btn-delete deleteRecord">
                                            <i class="fa fa-trash red" data-toggle="tooltip" title="حذف"></i>
                                        </button>
                                        {{-- </form>--}}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="col-md-12 pagination pagination-centered ">
                    {!! $socials->links() !!}
                </div>
            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script>

        $(".deleteRecord").click(function () {
            Swal.fire({
                title: 'هشدار!',
                text: "آیا از حذف سطر جاری مطمئن هستید؟",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'بله, حذف کن!',
                cancelButtonText: 'خیر'
            }).then((result) => {
                if (result.value) {
                    var id = $(this).data("id");
                    var token = $("meta[name='csrf-token']").attr("content");
                    $.ajax(
                        {
                            url: "/admin/socials/" + $(this).data("id"),
                            type: 'DELETE',
                            data: {
                                "id": id,
                                "_token": token,
                            },
                            success: function (result) {
                                window.location.replace('/admin/socials/');
                            }
                        });
                    Swal.fire(
                        'حذف!',
                        'با موفقیت حذف شد.',
                        'success'
                    )
                }
            });

        });

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    </script>
@endsection

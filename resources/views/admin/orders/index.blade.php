@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">سفارش ها</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">جدول سفارش ها</h3>
                    <div class="text-left">

                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive">
                    <table id="example2" class="table table-bordered table-hover ">
                        <thead>
                        <tr role="row" class="text-center">
                            <th scope="col">شناسه</th>
                            <th scope="col">نام خریدار</th>
                            <th scope="col">وضعیت پرداخت</th>
                            <th scope="col">عملیات</th>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr role="row" class="odd text-center">
                                <td>{{$order->id}}</td>
                                <td>{{$order->user->name}} {{$order->user->family}}</td>
                                <td>{{$order->status}}</td>
                                <td>
                                    <div class="row">
                                        @if($order->status==1)
                                            <a class="btn-delete blue mt-auto ml-1 mr-1"
                                               href="{{route('orders.edit', $order->id)}}">
                                                <i class="fa fa-edit" data-toggle="tooltip" title="نمایش"></i>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 pagination pagination-centered ">
                    {!! $orders->links() !!}
                </div>
            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script>

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    </script>
@endsection

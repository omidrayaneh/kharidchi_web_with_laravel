@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">سفارش</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right"> سفارش به شماره <span class="red">{{$orders[0]->orderId}} </span>متعلق
                        به<span class="green"> {{$orders[0]->username}} {{$orders[0]->family}}</span></h3>
                </div>
                <!-- /.card-header -->

                <div class="card-body">
                    <section class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12">
                                    <!-- Main content -->
                                    <div class="invoice p-3 mb-3">
                                        <!-- title row -->
                                        <div class="row">
                                            <div class="col-12">
                                                <h4>
                                                    <i class="fa fa-globe"></i> فروشگاه
                                                    اینترنتی {{__('word.storeName')}}
                                                    <small class="float-left"> تاریخ
                                                        : {{\Verta::instance($orders[0]->created_at)->format('H:m:s Y/m/d')}}</small>
                                                </h4>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- info row -->
                                        <div class="row invoice-info">
                                            <div class="col-sm-4 invoice-col">
                                                <address>
                                                    استان :{{$orders[0]->province}} <br>
                                                    شهرستان :{{$orders[0]->city}} <br>
                                                    تلفن :{{$orders[0]->phone}} <br>
                                                    ایمیل :{{$orders[0]->email}} <br>
                                                </address>
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4 invoice-col">
                                                <address>
                                                    نام مشتری :{{$orders[0]->username}} {{$orders[0]->family}}
                                                    <br>
                                                    آدرس مشتری :{{$orders[0]->address}}<br>
                                                    تلفن : {{$orders[0]->phone}}<br>
                                                    کد پستی : {{$orders[0]->post_code}}
                                                </address>
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4 invoice-col">
                                                <b>سفارش : {{$orders[0]->orderId}}</b><br>
                                                <b>کد سفارش :</b> {{$orders[0]->authority}}<br>
                                                <b>تاریخ پرداخت
                                                    :</b>{{\Verta::instance($orders[0]->pay_day)->format('H:m:s Y/m/d')}}
                                                <br>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <!-- Table row -->
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>تعداد</th>
                                                        <th>محصول</th>
                                                        <th>قیمت</th>
                                                        <th>تخفیف</th>
                                                        <th>تعداد</th>
                                                        <th>ویژگی</th>
                                                        <th>نوع</th>
                                                        <th>قیمت کل</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $discount_total = 0.00 ?>
                                                    @foreach($orders as $item)
                                                        <tr>
                                                            <td>1</td>
                                                            <td>{{$item->product}}</td>
                                                            <td>{{number_format($item->price - ($item->price*$item->discount)/100)}}
                                                                <span> تومان </span></td>
                                                            <td>{{number_format(($item->price*$item->discount)/100)}}
                                                                <span> تومان </span></td>
                                                            <td>{{$item->qty}}</td>
                                                            @if(!empty($item->attGroup))
                                                                <td>{{$item->attGroup}}</td>
                                                            @else
                                                                <td>---</td>@endif
                                                            @if(!empty($item->attValue))
                                                                <td>{{$item->attValue}}</td>
                                                            @else
                                                                <td>---</td>@endif
                                                            <td>{{number_format($item->price* $item->qty)}}
                                                                <span> تومان </span></td>
                                                            <?php $discount_total += ($item->price * $item->discount) / 100 * $item->qty  ?>

                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <div class="row">
                                            <!-- accepted payments column -->

                                            <!-- /.col -->
                                            <div class="col-6">
                                                <p class="lead">جزئیات پرداخت :</p>

                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tr>
                                                            <th style="width:50%">مبلغ کل :</th>
                                                            <td>{{number_format($orders[0]->amount+$discount_total)}}
                                                                تومان
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>مالیات (9.3%)</th>
                                                            <td>0</td>
                                                        </tr>
                                                        <tr>
                                                            <th>تخفیف :</th>
                                                            <td>{{$discount_total}} تومان</td>
                                                        </tr>
                                                        <tr>
                                                            <th>هزینه ارسال :</th>
                                                            @if($orders[0]->amount- $total !=0)
                                                                <td>{{$orders[0]->amount- $total}} تومان</td>
                                                            @else
                                                                <td>رایگان</td>
                                                            @endif
                                                        </tr>
                                                        <tr>
                                                            <th>مبلغ پرداخت شده:</th>
                                                            <td>{{number_format($orders[0]->amount)}} تومان</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->

                                        <!-- this row will not appear when printing -->
                                        <div class="row no-print">
                                            <div class="col-12">
                                                <form method="post" action="/admin/orders/{{$orders[0]->orderId}}">
                                                    @csrf
                                                    <input type="hidden" name="_method" value="PATCH">
                                                    <button onclick="window.print();"
                                                       class="btn btn-default"><i
                                                            class="fa fa-print"></i> پرینت </button>
                                                    @if( $orders[0]->approve == -1)
                                                        <button type="submit" class="btn btn-primary float-left ml-2"
                                                                style="margin-right: 5px;">
                                                            <i class="fa fa-check"></i> ارسال
                                                        </button>
                                                    @elseif($orders[0]->approve == 1)
                                                        <button type="submit" class="btn btn-danger float-left ml-2"
                                                                style="margin-right: 5px;">
                                                            <i class="fa fa-close"></i> لغو
                                                        </button>
                                                    @elseif($orders[0]->approve == 0)
                                                        <button type="submit" disabled
                                                                class="btn btn-primary float-left ml-2"
                                                                style="margin-right: 5px;">
                                                            <i class="fa fa-close"></i> ارسال
                                                        </button>
                                                    @endif
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.invoice -->
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.container-fluid -->
                    </section>


                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection





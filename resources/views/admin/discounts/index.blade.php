@extends('admin.layouts.master')
@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.0/slimselect.min.css" rel="stylesheet"></link>
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">تخفیف گروهی </i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">تخفیف و افزایش  گروهی</h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    @if($errors->any())
                        <ol  style=" list-style-type: persian" >
                            <div class="  alert alert-danger mr-1">
                                @foreach($errors->all() as $error)
                                    <li >{{$error}}</li>
                                @endforeach
                            </div>
                        </ol>
                    @endif
                        <form method="post" action="{{route('increase.categories')}}">
                            @csrf
                            <div class="card-body offset-md-2 col-md-6">
                                <div class="form-group" >
                                    <label>دسته بندی ها</label>
                                    <select id="multiple"  name="categories[]" multiple>
                                        @foreach($categories as $category)
                                            <option class="multi-select-text" value="{{$category->id}}" >{{$category->title}}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-group">
                                        <label>افزایش درصدی</label>
                                        <input name="increase" type="text" class="form-control" placeholder="افزایش درصدی را وارد کنید ...">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-success pull-left ">ذخیره</button>
                            </div>
                        </form>
                    <form method="post" action="{{route('discounts.categories')}}">
                        @csrf
                        <div class="card-body offset-md-2 col-md-6">

                            <div class="form-group" >
                                <label>دسته بندی ها</label>
                                <select id="multiple1"  name="categories[]" multiple>
                                    @foreach($categories as $category)
                                        <option class="multi-select-text" value="{{$category->id}}" >{{$category->title}}</option>
                                    @endforeach
                                </select>
                                <div class="form-group">
                                    <label>تخفیف درصدی</label>
                                    <input name="discount" type="text" class="form-control" placeholder="تخفیف را وارد کنید ...">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success pull-left ">ذخیره</button>
                        </div>
                    </form>

                        <hr>
                        <form method="post" action="{{route('discounts.products')}}">
                            @csrf
                            <div class="card-body offset-md-2 col-md-6">

                                <div class="form-group" >
                                    <label>محصول ها</label>
                                    <select id="multiple2"  name="products[]" multiple>
                                        @foreach($products as $product)
                                            <option class="multi-select-text" value="{{$product->id}}" >{{$product->title}}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-group">
                                        <label>تخفیف درصدی</label>
                                        <input name="discount" type="text" class="form-control" placeholder="تخفیف درصدی را وارد کنید ...">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-success pull-left ">ذخیره</button>
                            </div>

                        </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.0/slimselect.min.js"></script>
    <script>
        var slim =  new SlimSelect({
            select: '#multiple',
            placeholder: 'جستجوی دسته بندی'
        })
        var slim =  new SlimSelect({
            select: '#multiple1',
            placeholder: 'جستجوی دسته بندی'
        })
        var slim1 =  new SlimSelect({
            select: '#multiple2',
            placeholder: 'جستجوی محصول'
        })
    </script>
@endsection



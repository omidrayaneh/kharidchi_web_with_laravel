@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">لیست ویژگی محصول ها</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">جدول محصول ها</h3>
                    <div class="text-left">

                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive">
                    <table id="example2" class="table table-bordered table-hover ">
                        <thead>
                        <tr role="row" class="text-center">
                            <th scope="col">#</th>
                            <th scope="col">نام محصول</th>
                            <th scope="col">عنوان ویژگی</th>
                            <th scope="col">نوع ویژگی</th>
                            <th scope="col">وضعیت ویژگی</th>
                            <th scope="col">تعداد ویژگی محصول</th>
                            <th scope="col">تعداد محصول</th>
                        </thead>
                        <tbody>
                        @foreach($products as $key => $product )
                            <tr role="row" class="odd text-center">
                                <td> {{ $products->firstItem() + $key }}</td>
                                <td>{{$product->productName}}</td>
                                <td>{{$product->att_group}}</td>
                                <td>{{ $product->att_value }}</td>
                                @if($product->color_status != null)
                                  <td>  <span class="float badge " style="background-color: {{$product->color_status}}">رنگ بندی</span></td>
                                @else
                                  <td>سایز بندی</td>
                                @endif
                                @if($product->colorCount != null)
                                    <td>{{$product->colorCount}} عدد</td>
                                @else
                                   <td>{{$product->sizeCount}} عدد</td>
                                @endif
                                <td>{{$product->qty}}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="col-md-12 pagination pagination-centered ">
                    {!! $products->links() !!}
                </div>
            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection


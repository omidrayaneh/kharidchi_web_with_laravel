@extends('admin.layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">

@endsection
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">محصول</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">ایجاد محصول جدید</h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    @if($errors->any())
                        <ol  style=" list-style-type: persian" >
                            <div class="  alert alert-danger mr-1">
                                @foreach($errors->all() as $error)
                                    <li >{{$error}}</li>
                                @endforeach
                            </div>
                        </ol>
                    @endif
                    <form id="myForm" method="post" action="/admin/products">
                        @csrf
                        <div class="card-body offset-md-2 col-md-8">
                            <div class="form-group">
                                <label>نام</label>
                                <input name="title" type="text" class="form-control" placeholder="نام محصول را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>قیمت</label>
                                <input name="price" type="text" class="form-control" placeholder="قیمت محصول را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>تخفیف</label>
                                <input name="discount" type="number" class="form-control" placeholder="تخفیف محصول را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>تعداد</label>
                                <input name="qty" type="number" class="form-control" placeholder="تعداد محصول را وارد کنید ...">
                            </div>
                            <attribute-component :brands="{{$brands}}"></attribute-component>
                            <div class="form-group ">
                                <label>وضعیت انتشار</label>
                                <br>
                                    <input name="status" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                           data-offstyle="danger">
                            </div>
                            <!--<div class="form-group ">-->
                            <!--    <label>وضعیت در اپلیکیشن</label>-->
                            <!--    <br>-->
                            <!--    <input name="app" type="checkbox" data-toggle="toggle" data-onstyle="success"-->
                            <!--               data-offstyle="danger">-->
                            <!--</div>-->
                            <div class="form-group">
                                <label>توضیحات</label>
                                <textarea id="textareaDescription" type="text" name="description" class=" form-control"
                                          placeholder="توضیحات محصول را وارد کنید..."></textarea>
                            </div>
                            <!--<div class="form-group">-->
                            <!--    <label>نام سئو</label>-->
                            <!--    <input name="meta_title" type="text" class="form-control" placeholder="  نام سئو را وارد کنید ...">-->
                            <!--</div>-->
                            <div class="form-group">
                                <label>توضیحات سئو</label>
                                <input name="meta_description" type="text" class="form-control" placeholder="توضیحات سئو را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>کلمات کلیدی</label>
                                <input name="meta_keywords" type="text" class="form-control" placeholder="کلمات کلیدی را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label for="photo">گالری تصاویر</label>
                                <input type="hidden" name="photo_id[]" id="product-photo" required>
                                <div id="photo" class="dropzone"></div>
                            </div>
                            <button type="submit" onclick="productGallery()" class="btn btn-success pull-left ">ذخیره</button>
                        </div>

                    </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script src="https://unpkg.com/vue-select@latest"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-select@latest/dist/vue-select.css">
    <script type="text/javascript" src="{{asset('/admin/dist/js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{asset('/admin/plugins/ckeditor/ckeditor.js')}}"></script>

    <script>
        Dropzone.autoDiscover = false;
        var id= '';
        var photosGallery = []
        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            removedfile: function(file) {
                var name = file.name;

                var token = $("meta[name='csrf-token']").attr("content");
                $.ajax(
                    {
                        url: "/admin/photos/" + id,
                        type: 'DELETE',
                        data: {
                            "id": id,
                            "_token": token,
                        },
                        success: function (result) {
                            // window.location.replace('/admin/galleries/');
                        }
                    });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            } ,
            acceptedFiles: ".jpg ,.png",
            maxFiles: 5,
            maxFilesize: 0.3,
            paramName: "file",
            acceptedMimeTypes: null,
            acceptParameter: null,
            enqueueForUpload: true,
            dictDefaultMessage:"افزودن عکس محصول",
            dictFileTooBig:"اندازه عکس بزرگ است",// image size error message
            dictInvalidFileType:"فرمت فایل اشتباه است",// file type error message
            dictCancelUpload:"لغو آپلود",//cancel error message
            dictCancelUploadConfirmation:"آیا می خواهید آپلود را متوقف کنید؟", //cancel conform
            dictMaxFilesExceeded:"برای هر محصول فقط 5 عکس می توانید آپلود کنید",
            dictRemoveFile:"حذف",// remove file

            url: "{{ route('photos.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                id = response.photo_id;
                photosGallery.push(response.photo_id)
            }
        });
        productGallery = function(){
            document.getElementById('product-photo').value = photosGallery
        }

        // CKEDITOR.replace('textareaDescription',{
        //     customConfig: 'config.js',
        //     toolbar: 'simple',
        //     language: 'fa',
        //     removePlugins: 'cloudservices, easyimage'
        // });
        CKEDITOR.replace('textareaDescription', {
            language: 'fa',
            filebrowserUploadMethod: 'form',
            filebrowserBrowseUrl: 'form',
            filebrowserUploadUrl: '{{route('photos.store',['_token'=>csrf_token()])}}',
            removePlugins: 'cloudservices, easyimage',
        });


    </script>



@endsection



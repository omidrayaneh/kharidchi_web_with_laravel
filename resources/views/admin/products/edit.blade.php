@extends('admin.layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">

@endsection
@section('content')

    <div class="content-wrapper" >
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">محصول</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">ویرایش محصول <span class="red">{{$product->title}}</span></h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    @if($errors->any())
                        <ol  style=" list-style-type: persian" >
                            <div class="  alert alert-danger mr-1">
                                @foreach($errors->all() as $error)
                                    <li >{{$error}}</li>
                                @endforeach
                            </div>
                        </ol>
                    @endif
                    <form id="myForm" method="post" action="/admin/products/{{$product->sku}}">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="card-body offset-md-2 col-md-8">
                            <div class="form-group">
                                <label>نام</label>
                                <input name="title" value="{{$product->title}}" type="text" class="form-control" placeholder="نام محصول را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>قیمت</label>
                                <input name="price" value="{{$product->price}}" type="text" class="form-control" placeholder="قیمت محصول را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>تخفیف</label>
                                <input name="discount" value="{{$product->discount}}" type="number" class="form-control" placeholder="تخفیف محصول را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>تعداد</label>
                                <input name="qty" value="{{$product->qty}}" type="number" class="form-control" placeholder="تعداد محصول را وارد کنید ...">
                            </div>
                            <attribute-component :brands="{{$brands}}" :product="{{$product}}"></attribute-component>
                            <div class="form-group ">
                                <label>وضعیت انتشار<br>
                                    <input  @if($product->status ==1) checked @endif name="status" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                            data-offstyle="danger"> </label>
                            </div>
                            <!--<div class="form-group ">-->
                            <!--    <label>وضعیت در اپلیکیشن<br>-->
                            <!--        <input @if($product->app ==1) checked @endif name="app" type="checkbox" data-toggle="toggle" data-onstyle="success"-->
                            <!--               data-offstyle="danger"> </label>-->
                            <!--</div>-->
                            <div class="form-group">
                                <label>توضیحات</label>
                                <textarea id="textareaDescription" type="text" name="description" class=" form-control"
                                          placeholder="توضیحات محصول را وارد کنید...">{{$product->description}}"</textarea>
                            </div>
                            <!--<div class="form-group">-->
                            <!--    <label>نام سئو</label>-->
                            <!--    <input name="meta_title" value="{{$product->meta_title}}" type="text" class="form-control" placeholder="  نام سئو را وارد کنید ...">-->
                            <!--</div>-->
                            <div class="form-group">
                                <label>توضیحات سئو</label>
                                <input name="meta_description" value="{{$product->meta_description}}" type="text" class="form-control" placeholder="توضیحات سئو را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>کلمات کلیدی</label>
                                <input name="meta_keywords" value="{{$product->meta_keywords}}" type="text" class="form-control" placeholder="کلمات کلیدی را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label for="photo">گالری تصاویر</label>
                                <input type="hidden" name="photo_id[]" id="product-photo">
                                <div id="photo" class="dropzone"></div>
                                <div class="row">
                                    @foreach($product->photos as $photo)
                                        <div class="col-sm-3" id="updated_photo_{{$photo->id}}">
                                            <img width="100" height="100" src="{{$photo->path }}" >
                                            <button type="button" class="btn btn-danger" onclick="removeImages({{$photo->id}})" >حذف</button>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <button type="submit" onclick="productGallery()" class="btn btn-success pull-left ">ذخیره</button>
                        </div>

                    </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')


    <script type="text/javascript" src="{{asset('/admin/dist/js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{asset('/admin/plugins/ckeditor/ckeditor.js')}}"></script>

    <script>

        Dropzone.autoDiscover = false;
        var photosGallery = []
        var photos = [].concat({{$product->photos->pluck('id')}})
        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            acceptedFiles: ".jpg ,.png",
            maxFiles: 5,
            maxFilesize: 0.3,
            paramName: "file",
            acceptedMimeTypes: null,
            acceptParameter: null,
            enqueueForUpload: true,
            dictDefaultMessage:"افزودن عکس محصول",
            dictFileTooBig:"اندازه عکس بزرگ است",// image size error message
            dictInvalidFileType:"فرمت فایل اشتباه است",// file type error message
            dictCancelUpload:"لغو آپلود",//cancel error message
            dictCancelUploadConfirmation:"آیا می خواهید آپلود را متوقف کنید؟", //cancel conform
            dictMaxFilesExceeded:"برای هر محصول فقط 5 عکس می توانید آپلود کنید",
            dictRemoveFile:"حذف",// remove file

            url: "{{ route('photos.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                photosGallery.push(response.photo_id)
            }
        });
        productGallery = function(){
            document.getElementById('product-photo').value = photosGallery.concat(photos)
        }
        CKEDITOR.replace('textareaDescription', {
            language: 'fa',
            filebrowserUploadMethod: 'form',
            filebrowserBrowseUrl: 'form',
            filebrowserUploadUrl: '{{route('photos.store',['_token'=>csrf_token()])}}',
            removePlugins: 'cloudservices, easyimage',
        });
        removeImages = function(id){
            console.log(photos.length)
            var index = photos.indexOf(id)

            photos.splice(index, 1);
            document.getElementById('updated_photo_' + id).remove();
            Swal.fire({
                title: 'هشدار!',
                text: "آیا از حذف عکس  مطمئن هستید؟",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'بله, حذف کن!',
                cancelButtonText: 'خیر'
            }).then((result) => {
                if (result.value) {
                    var token = $("meta[name='csrf-token']").attr("content");
                    $.ajax(
                        {
                            url: "/admin/photos/" + id,
                            type: 'DELETE',
                            data: {
                                "id": id,
                                "_token": token,
                            },
                            success: function (result) {
                               // window.location.replace('/admin/products/' + product.sku + '/edit');
                            }
                        });
                    Swal.fire(
                        'حذف!',
                        'محصول با موفقیت حذف شد.',
                        'success'
                    )
                }
                else{
                    Swal.fire(
                        'حذف!',
                        'حذف محصول لغو شد.',
                        'error'
                    )
                }
            });



        };
    </script>

@endsection



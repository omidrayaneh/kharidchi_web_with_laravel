@extends('admin.layouts.master')

@section('content')
    <products-list ></products-list>

@endsection

@section('script')
    <script>
        document.cookie="pageurl=" + encodeURIComponent(window.location['search']);
    </script>
@endsection

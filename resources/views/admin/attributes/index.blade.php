@extends('admin.layouts.master')

@section('content')
   <att-group-list></att-group-list>
@endsection

@section('script')
    <script>

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    </script>
@endsection

@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">ویژگی</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">ویرایش ویژگی <span class="red">{{$attribute->title}}</span></h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    @if($errors->any())
                        <ol  style=" list-style-type: persian" >
                            <div class="  alert alert-danger mr-1">
                                @foreach($errors->all() as $error)
                                    <li >{{$error}}</li>
                                @endforeach
                            </div>
                        </ol>
                    @endif
                    <form method="post" action="/admin/attributes/{{$attribute->slug}}">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="card-body offset-md-2 col-md-6">
                            <div class="form-group">
                                <label>نام ویژگی</label>
                                <input name="title" value="{{$attribute->title}}" type="text" class="form-control" placeholder="نام ویژگی را وارد کنید ...">
                            </div>
                            <div class="form-group ">
                                <label>چندتایی</label>
                                <br>
                                <input @if($attribute->multiple ==1) checked @endif  name="multiple" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                       data-offstyle="danger">
                            </div>
                            <div class="form-group ">
                                <label>رنگ</label>
                                <br>
                                <input  @if($attribute->color ==1) checked @endif name="color" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                       data-offstyle="danger">
                            </div>
                            <div class="form-group ">
                                <label>سایز</label>
                                <br>
                                <input  @if($attribute->size ==1) checked @endif name="size" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                        data-offstyle="danger">
                            </div>
                            <div class="form-group ">
                                <label>وضعیت</label>
                                <br>
                                <input  @if($attribute->status ==1) checked @endif  name="status" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                       data-offstyle="danger">
                            </div>
                            <button type="submit" class="btn btn-success pull-left ">ذخیره</button>
                        </div>

                    </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection



@extends('admin.layouts.master')
@section('style')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">ویرایش تصویر </h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    @if($errors->any())
                        <ol style=" list-style-type: persian">
                            <div class="  alert alert-danger mr-1">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </div>
                        </ol>
                    @endif
                    <form method="post" action="/admin/galleries/{{$photo->id}}">
                        @csrf
                        @method('PATCH')
                        <div class="card-body offset-md-2 col-md-8">
                            <div class="form-group">
                                <label for="photo">تصویر</label>
                                <input type="hidden" name="photo_id[]" id="product-photo" required>
                                <div id="photo" class="dropzone"></div>
                            </div>
                            <label for="place">مکان عکس</label>
                            <select class="form-control" name="place">
                                <option value="">انتخاب کنید...</option>
                                <option @if($photo->place == 'logo') selected @endif value="logo">لوگو</option>

                                <option @if($photo->place == 'favicon') selected @endif value="favicon">لوگوی کنار تب</option>

                                <option @if($photo->place == 'ads') selected @endif value="ads">تبلیغ بالای صفحه</option>
                                <option @if($photo->place == 'slider') selected @endif value="slider">اسلایدر صفحه
                                    اصلی
                                </option>
                                <option @if($photo->place == 'res-slider') selected @endif value="res-slider">اسلایدر
                                    صفحه اصلی رسپانسیو
                                </option>
                                <option @if($photo->place == 'sidebar-banner') selected @endif value="sidebar-banner">
                                    بنر کناری
                                </option>
                                <option @if($photo->place == 'medium-banner') selected @endif value="medium-banner">بنر
                                    متوسط
                                </option>
                                <option @if($photo->place == 'small-banner') selected @endif value="small-banner">بنر
                                    کوچک
                                </option>
                                <option @if($photo->place == 'large-banner') selected @endif value="large-banner">بنر
                                    بزرگ
                                </option>
                            </select>
                            <br>
                            <div class="form-group">
                                <label>محتوای عکس</label>
                                <input type="text" name="detail" value="{{$photo->detail}}" class=" form-control"
                                       placeholder="محتوای عکس را وارد کنید...">
                            </div>
                            <div class="form-group ">
                                <label>وضعیت انتشار</label>
                                <br>
                                <input name="status" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                       data-offstyle="danger" @if($photo->status) checked @endif>
                            </div>
                            <button type="submit" class="btn btn-success pull-left ">ذخیره</button>
                        </div>

                    </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('script')

    <script type="text/javascript" src="{{asset('/admin/dist/js/dropzone.js')}}"></script>

    <script>
        var image = @json($photo);

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-start',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        var id = image.id;
        Dropzone.autoDiscover = false;
        var photosGallery = []
        var drop = new Dropzone('#photo', {
            // addRemoveLinks: true,
            // removedfile: function(file) {
            //     var name = file.name;
            //     alert(image.id)
            //     var token = $("meta[name='csrf-token']").attr("content");
            //     $.ajax(
            //         {
            //             url: "/admin/galleries/" + id,
            //             type: 'DELETE',
            //             data: {
            //                 "id": id,
            //                 "_token": token,
            //             },
            //             success: function (result) {
            //                 // window.location.replace('/admin/galleries/');
            //             }
            //         });
            //     var _ref;
            //     return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            // } ,
            acceptedFiles: ".jpg ,.png,.gif",
            maxFiles: 1,
            maxFilesize: 1.5,
            paramName: "file",
            acceptedMimeTypes: null,
            acceptParameter: null,
            enqueueForUpload: true,
            dictDefaultMessage: "افزودن عکس محصول",
            dictFileTooBig: "اندازه عکس بزرگ است",// image size error message
            dictInvalidFileType: "فرمت فایل اشتباه است",// file type error message
            dictCancelUpload: "لغو آپلود",//cancel error message
            dictCancelUploadConfirmation: "آیا می خواهید آپلود را متوقف کنید؟", //cancel conform
            dictMaxFilesExceeded: "برای هر محصول فقط 5 عکس می توانید آپلود کنید",
            dictRemoveFile: "حذف",// remove file
            url: "{{ route('slider.update',$photo->id) }}",
            sending: function (file, xhr, formData) {
                formData.append("_token", "{{csrf_token()}}")
                formData.append("_method", "PATCH");

            },
            success: function (file, response) {
               window.location.replace('/admin/galleries/' + image.id + '/edit');
                Toast.fire({
                    type: 'success',
                    title: 'تصویر با موفقیت آپلود شد'
                })
            }
        });
        // productGallery = function(){
        //     document.getElementById('slide-photo').value = photosGallery
        // }
        var file = image.path;
        var mockFile = {name: image.original_name, size: 12345};
        drop.emit('addedfile', mockFile);
        drop.emit('thumbnail', mockFile, image.path);
        drop.emit('complete', mockFile);

    </script>



@endsection



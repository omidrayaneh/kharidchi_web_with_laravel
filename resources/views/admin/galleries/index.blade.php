@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">گالری تصاویر</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">جدول عکس ها</h3>
                    <div class="text-left">
                        <a class="btn btn-app" href="/admin/galleries/create">
                            <i class="fa fa-plus green"></i> جدید
                        </a>
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive">
                    <table id="example2" class="table table-bordered table-hover ">
                        <thead>
                        <tr role="row" class="text-center">
                            <th>مسیر</th>
                            <th scope="col">تصاویر</th>
                            <th scope="col">توضیحات</th>
                            <th scope="col">مکان تصویر</th>
                            <th scope="col">وضعیت تصویر</th>
                            <th scope="col">عملیات</th>
                        </thead>
                        <tbody>
                        @foreach($photos as $photo)
                            <tr role="row" class="odd text-center">
{{--                                <td><input type="text" readonly onclick="myFunction()" id="myInput" value="{{url($photo->path) }}"></td>--}}
                                <td><a class="copy_text"  data-toggle="tooltip" title="کپی مسیر تصویر" href="{{url($photo->path) }}">کپی لینک</a></td>
                                <td class="text-center">
                                    <img width="50" height="50" src="{{$photo->path }}" class="img-fluid rounded">
                                </td>
                                @if($photo->detail != '' || $photo->detail != null)
                                <td>{{$photo->detail}}</td>
                                @else
                                    <td>---</td>
                                @endif
                                @if($photo->place == 'ads')
                                    <td>هدر تبلیغاتی</td>
                                @elseif($photo->place== 'favicon')
                                    <td>لوگوی کنار تب</td>
                                @elseif($photo->place== 'logo')
                                    <td>لوگو</td>
                                @elseif($photo->place== 'slider')
                                    <td>اسلایدر صفحه اصلی</td>
                                @elseif($photo->place== 'res-slider')
                                    <td>اسلایدر صفحه اصلی رسپانسیو</td>
                                @elseif($photo->place== 'sidebar-banner')
                                    <td>بنر کناری</td>
                                @elseif($photo->place== 'medium-banner')
                                    <td>بنر متوسط</td>
                                @elseif($photo->place== 'small-banner')
                                    <td>بنر کوچک</td>
                                @elseif($photo->place== 'large-banner')
                                    <td>بنر بزرگ</td>
                                @else
                                    <td><span class="float badge bg-danger">انتخاب نشده</span></td>
                                @endif
                                <td>
                                    @if($photo->status == 1)
                                        <span class="float badge bg-success">منتشر شده</span>
                                    @else
                                        <span class="float badge bg-danger">منتشر نشده</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="row">
                                        <a class="btn-delete blue mt-auto ml-1 mr-1"
                                           href="{{route('galleries.edit', $photo->id)}}">
                                            <i class="fa fa-edit" data-toggle="tooltip" title="ویرایش"></i>
                                        </a>
                                        |
                                        <button data-id="{{ $photo->id }}" class="btn-delete deleteRecord">
                                            <i class="fa fa-trash red" data-toggle="tooltip" title="حذف"></i>
                                        </button>
                                        {{-- </form>--}}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="col-md-12 pagination pagination-centered ">
                    {!! $photos->links() !!}
                </div>
            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script>

        $(".deleteRecord").click(function () {
            Swal.fire({
                title: 'هشدار!',
                text: "آیا از حذف سطر جاری مطمئن هستید؟",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'بله, حذف کن!',
                cancelButtonText: 'خیر'
            }).then((result) => {
                if (result.value) {
                    var id = $(this).data("id");
                    var token = $("meta[name='csrf-token']").attr("content");
                    $.ajax(
                        {
                            url: "/admin/galleries/" + $(this).data("id"),
                            type: 'DELETE',
                            data: {
                                "id": id,
                                "_token": token,
                            },
                            success: function (result) {
                                setTimeout(function (){
                                    window.location.replace('/admin/galleries');
                                },1000);
                                Swal.fire(
                                    'حذف!',
                                    'با موفقیت حذف شد.',
                                    'success',)
                            }
                        });
                }else {
                    Swal.fire(
                        'حذف!',
                        'عملیات حذف،لغو شد.',
                        'error'
                    )
                }
            });

        });

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        //     function myFunction() {
        //     var copyText = document.getElementById("myInput");
        //     copyText.select();
        //     copyText.setSelectionRange(0, 99999)
        //     document.execCommand("copy");
        //     alert("Copied the text: " + copyText.value);
        // }
        $('.copy_text').click(function (e) {
            e.preventDefault();
            var copyText = $(this).attr('href');

            document.addEventListener('copy', function(e) {
                e.clipboardData.setData('text/plain', copyText);
                e.preventDefault();
            }, true);

            document.execCommand('copy');
            console.log('copied text : ', copyText);
           // alert('مسیر تصویر،کپی شد.');
            Swal.fire(
                'کپی!',
                'مسیر تصویر،کپی شد.',
                'success',)
        });
    </script>
@endsection

@extends('admin.layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">
@endsection
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">گالری عکس</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">ایجاد گالری عکس جدید</h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    <div class="card-body offset-md-2 col-md-8">
                        <div class="form-group">
                            <label for="photo">گالری تصاویر</label>
                            <input type="hidden" name="photo_id[]" id="product-photo" required>
                            <div id="photo" class="dropzone"></div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')

    <script type="text/javascript" src="{{asset('/admin/dist/js/dropzone.js')}}"></script>

    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-start',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        var id ='';
        Dropzone.autoDiscover = false;
        //var photosGallery = []
        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            removedfile: function(file) {
                var name = file.name;

                var token = $("meta[name='csrf-token']").attr("content");
                $.ajax(
                    {
                        url: "/admin/galleries/" + id,
                        type: 'DELETE',
                        data: {
                            "id": id,
                            "_token": token,
                        },
                        success: function (result) {
                           // window.location.replace('/admin/galleries/');
                        }
                    });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            } ,
            acceptedFiles: ".jpg ,.png,.gif",
            paramName: "file",
            acceptedMimeTypes: null,
            acceptParameter: null,
            enqueueForUpload: true,
            dictDefaultMessage:"افزودن عکس محصول",
            dictFileTooBig:"اندازه عکس بزرگ است",// image size error message
            dictInvalidFileType:"فرمت فایل اشتباه است",// file type error message
            dictCancelUpload:"لغو آپلود",//cancel error message
            dictCancelUploadConfirmation:"آیا می خواهید آپلود را متوقف کنید؟", //cancel conform
            dictMaxFilesExceeded:"برای هر محصول فقط 5 عکس می توانید آپلود کنید",
            dictRemoveFile:"حذف",// remove file

            url: "{{ route('galleries.store') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
                formData.append("type",true)
            },
            success: function(file, response){
                console.log(response)
                Toast.fire({
                    type: 'success',
                    title: 'عکس با موفقیت آپلود شد'
                })
            }
        });
        // productGallery = function(){
        //     document.getElementById('product-photo').value = photosGallery
        // }

    </script>



@endsection



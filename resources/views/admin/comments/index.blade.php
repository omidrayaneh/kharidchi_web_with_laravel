@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">نظر ها</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        {{--   accepted modal     --}}
        <div  class="modal fade " id="ActModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">ویرایش نظر</h5>
                    </div>
                    <div id="Cbody" class="modal-body">
                        <div class="form-group">
                            <label>متن</label>
                            <div></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" onclick="clearLink()" data-dismiss="modal">بستن</button>

                        <button id="btnChange"  type="button" class="btn btn-primary">
                            <a id="change" class="text-white" href="">
                            منتشر کردن
                            </a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    {{--  /accepted modal     --}}
        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">جدول نظر ها</h3>
                    <div class="text-left">

                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive">
                    <table id="example2" class="table table-bordered table-hover ">
                        <thead>
                        <tr role="row" class="text-center">
                            <th scope="col">شناسه</th>
                            <th scope="col">موضوع</th>
                            <th scope="col">توضیحات</th>
                            <th scope="col">نویسنده</th>
                            <th scope="col">تایید کننده</th>
                            <th scope="col">وضعیت</th>
                            <th scope="col">عملیات</th>
                        </thead>
                        <tbody>
                        @foreach($comments as $key=>$comment)

                            <tr role="row" class="odd text-center">
                                <td>{{$comments->currentPage() == 1 ? $key+1: (($comments->perPage()*($comments->currentPage()-1)))+$key+1}}</td>
                                <td class="text-center">{{$comment->title}}</td>
                                <td>{{$comment->body}}</td>
                                <td>{{ $comment->user->name}} {{$comment->user->family}}</td>
                                <td>@if($comment->users){{ $comment->users->name}} {{$comment->users->family}}@endif</td>
                                <td>
                                    @if($comment->status == 1)
                                        <span class="float badge bg-success">منتشر شده</span>
                                    @else
                                        <span class="float badge bg-danger">منتشر نشده</span>
                                    @endif
                                </td>
                                <td>
                                    <div>
                                        <button type="button"  class="btn-delete blue" data-toggle="modal" data-target="#ActModal"  data-id="{{$comment->id}}"
                                                onclick="SetLink(this,{{$comment}},{{$comment->commentable}})">
                                            <i class="fa fa-edit" data-toggle="tooltip"  title="ویرایش"></i>
                                        </button>

                                        <button class="btn-delete deleteRecord">
                                            <i class="fa fa-trash red" data-toggle="tooltip" title="حذف"></i>
                                        </button>
                                        {{-- </form>--}}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="col-md-12 pagination pagination-centered ">
                    {!! $comments->links() !!}
                </div>
            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script>

        $(".deleteRecord").click(function () {

            Swal.fire({
                title: 'هشدار!',
                text: "آیا از حذف سطر جاری مطمئن هستید؟",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'بله, حذف کن!',
                cancelButtonText: 'خیر'
            }).then((result) => {
                if (result.value) {
                    var id = $(this).data("id");
                    var token = $("meta[name='csrf-token']").attr("content");
                    $.ajax(
                        {
                            url: "/admin/comments/" + $(this).data("id"),
                            type: 'DELETE',
                            data: {
                                "id": id,
                                "_token": token,
                            },
                            success: function (result) {
                                window.location.replace('/admin/comments/');
                            }
                        });

                    Swal.fire(
                        'حذف!',
                        'با موفقیت حذف شد.',
                        'success'
                    )
                }
            });

        });

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    </script>
    <script>

        function SetLink(id,data,product) {
            let p = id.getAttribute("data-id");
            var token = $("meta[name='csrf-token']").attr("content");
            $.ajax(
                {
                    url: "/admin/comments/read/" + p,
                    type: 'PATCH',
                    data: {
                        "id": p,
                        "_token": token,
                    },
                    success: function (result) {
                        // window.location.replace('/admin/comments/');
                    }
                });

            let s =   '\<div class="form-group">\
                \<label>'+' نام کالا : '+product['title']+'</label>\
                \<div>'+' عنوان نظر: '+data['title']+'</div>\
                \<div>'+' متن نظر: '+data['body']+'</div>\
                \<div>'+' نظر مثبت: '+data['advantages']+'</div>\
                \<div>'+' نظر منفی: '+data['disadvantages']+'</div>\
                \</div>';

            $('#Cbody')[0].innerHTML = s;
            document.getElementById("change").setAttribute("href", "/admin/comments/"+p+"/edit");
            if (data['status'] == 1){
                $('#btnChange').removeClass('btn btn-primary');
                $('#btnChange').addClass('btn btn-danger');
                $('#change').text('لغو انتشار') ;
            }
            else {
                $('#btnChange').removeClass('btn btn-danger');
                $('#btnChange').addClass('btn btn-primary');

                $('#change').text('منتشر کن') ;
            }
        }
        function clearLink(){
            $('#Cbody')[0].innerHTML = '';
            document.getElementById("change").setAttribute("href", "/admin/comments/");
            window.location.replace('/admin/comments/');
        }
    </script>
@endsection

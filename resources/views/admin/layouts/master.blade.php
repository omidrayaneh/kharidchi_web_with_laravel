<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>پنل مدیریت | داشبورد اول</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="{{asset('/admin/plugins/font-awesome/css/font-awesome.min.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('/admin/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/dist/css/bootstrap-toggle.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('/admin/plugins/iCheck/flat/blue.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{asset('/admin/plugins/morris/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset('/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset('/admin/plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('/admin/plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{asset('/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- bootstrap rtl -->
    <link rel="stylesheet" href="{{asset('/admin/dist/css/bootstrap-rtl.min.css')}}">
    <!-- template rtl version -->
    <link rel="stylesheet" href="{{asset('/admin/dist/css/custom-style.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/dist/css/color.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.css">
    @yield('style')
</head>
<body class="hold-transition sidebar-mini">

<div class="wrapper" id="app">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom" style="z-index: 1;">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-3">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="جستجو" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </form>

        <!-- Right navbar links -->
        <ul class="navbar-nav mr-auto">
            <!-- Messages Dropdown Menu -->

           <notification-order :userid="{{auth()->id()}}" :unreads="{{auth()->user()->unreadNotifications}}"></notification-order>
            <!-- Notifications Dropdown Menu -->
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{route('dashboard')}}" class="brand-link">
            <img src="/admin/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">پنل مدیریت</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar" style="direction: ltr">
            <div style="direction: rtl">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="/admin/dist/img/user.png" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">{{auth()->user()->name}}</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                             with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview">
                            <a href="{{route('orders.index')}}" class="nav-link">
                                <i class="nav-icon fa fa-bell darkblue"></i>
                                <p>
                                    سفارش ها
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('comments.index')}}" class="nav-link">
                                <i class="purple nav-icon fa fa-comment "></i>
                                <p>
                                    نظر ها
                                    <span class="badge badge-danger right">@if(count($comments)>0){{count($comments)}}@endif</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview {{ \Route::is('texts.create') ? 'menu-open':''}} {{ \Route::is('texts.index') ? 'menu-open':''}}">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-text-height pink"></i>
                                <p>
                                    متون
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview mr-3 ">
                                <li class="nav-item ">
                                    <a href="{{route('texts.create')}}" class="nav-link">
                                        <i class="pink nav-icon fa fa-plus"></i>
                                        <p>متن جدید</p>
                                    </a>
                                </li>
                                <li class="nav-item {{ Route::is('texts.index') ? 'menu-open':''}}">
                                    <a href="{{route('texts.index')}}" class="nav-link">
                                        <i class="pink nav-icon fa fa-list-ul "></i>
                                        <p>متن ها</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview {{ \Route::is('costs.create') ? 'menu-open':''}} {{ \Route::is('costs.index') ? 'menu-open':''}}">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-code cyan"></i>
                                <p>
                                    هزینه پستی
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview mr-3 ">
                                <li class="nav-item ">
                                    <a href="{{route('costs.create')}}" class="nav-link">
                                        <i class="cyan nav-icon fa fa-plus"></i>
                                        <p>  هزینه پستی جدید</p>
                                    </a>
                                </li>
                                <li class="nav-item {{ Route::is('costs.index') ? 'menu-open':''}}">
                                    <a href="{{route('costs.index')}}" class="nav-link">
                                        <i class="cyan nav-icon fa fa-list-ul "></i>
                                        <p> هزینه های پستی</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview {{ \Route::is('categories.create') ? 'menu-open':''}} {{ \Route::is('categories.index') ? 'menu-open':''}}">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-bars blue"></i>
                                <p>
                                    دسته بندی
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview mr-3 ">
                                <li class="nav-item ">
                                    <a href="{{route('categories.create')}}" class="nav-link">
                                        <i class="blue nav-icon fa fa-plus"></i>
                                        <p>دسته بندی جدید</p>
                                    </a>
                                </li>
                                <li class="nav-item {{ Route::is('categories.index') ? 'menu-open':''}}">
                                    <a href="{{route('categories.index')}}" class="nav-link">
                                        <i class="blue nav-icon fa fa-list-ul "></i>
                                        <p>دسته بندی ها</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview {{ \Route::is('galleries.create') ? 'menu-open':''}} {{ \Route::is('galleries.index') ? 'menu-open':''}}">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-photo red"></i>
                                <p>
                                    اسلایدر
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview mr-3">
                                <li class="nav-item">
                                    <a href="{{route('galleries.create')}}" class="nav-link">
                                        <i class="red nav-icon fa fa-plus"></i>
                                        <p>اسلایدر جدید</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('galleries.index')}}" class="nav-link">
                                        <i class="red nav-icon fa fa-list-ul "></i>
                                        <p>اسلایدر ها</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview  {{ \Route::is('attributes.create') ? 'menu-open':''}} {{ \Route::is('attributes.index') ? 'menu-open':''}}">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-link green"></i>
                                <p>
                                    ویژگی
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview mr-3">
                                <li class="nav-item">
                                    <a href="{{route('attributes.create')}}" class="nav-link">
                                        <i class="green nav-icon fa fa-plus"></i>
                                        <p>ویژگی جدید</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('attributes.index')}}" class="nav-link">
                                        <i class="green nav-icon fa fa-list-ul "></i>
                                        <p>ویژگی ها</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview  {{ \Route::is('attributes-value.create') ? 'menu-open':''}} {{ \Route::is('attributes-value.index') ? 'menu-open':''}}">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-dedent orange"></i>
                                <p>
                                    مقادیر ویژگی
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview mr-3">
                                <li class="nav-item">
                                    <a href="{{route('attributes-value.create')}}" class="nav-link">
                                        <i class="orange nav-icon fa fa-plus"></i>
                                        <p>مقادیر ویژگی جدید</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('attributes-value.index')}}" class="nav-link">
                                        <i class="orange nav-icon fa fa-list-ul "></i>
                                        <p>ویژگی ها</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview  {{ Route::is('brands.create') ? 'menu-open':''}} {{ Route::is('brands.index') ? 'menu-open':''}}" >
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-braille yellow"></i>
                                <p>
                                    برند
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview mr-3">
                                <li class="nav-item">
                                    <a href="{{route('brands.create')}}" class="nav-link">
                                        <i class="yellow nav-icon fa fa-plus"></i>
                                        <p>برند جدید</p>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="{{route('brands.index')}}" class="nav-link">
                                        <i class="yellow nav-icon fa fa-list-ul "></i>
                                        <p>برند ها</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview {{ Route::is('socials.create') ? 'menu-open':''}} {{ Route::is('socials.index') ? 'menu-open':''}}"  >
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-instagram teal "></i>
                                <p>شبکه اجتماعی<i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview mr-3">
                                <li class="nav-item">
                                    <a href="{{route('socials.create')}}" class="nav-link">
                                        <i class="teal  nav-icon fa fa-plus"></i>
                                        <p>شبکه اجتماعی جدید</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('socials.index')}}" class="nav-link">
                                        <i class="teal  nav-icon fa fa-list-ul "></i>
                                        <p>شبکه های اجتماعی</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview {{ Route::is('coupons.create') ? 'menu-open':''}} {{ Route::is('coupons.index') ? 'menu-open':''}}"  >
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-copyright blue-green "></i>
                                <p>
                                    کوپن
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview mr-3">
                                <li class="nav-item">
                                    <a href="{{route('coupons.create')}}" class="nav-link">
                                        <i class="blue-green  nav-icon fa fa-plus"></i>
                                        <p>کوپن جدید</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('coupons.index')}}" class="nav-link">
                                        <i class="blue-green  nav-icon fa fa-list-ul "></i>
                                        <p>کوپن ها</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview  {{ Route::is('products.create') ? 'menu-open':''}} {{ Route::is('products.index') ? 'menu-open':''}}" >
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-product-hunt  darkred"></i>
                                <p>
                                    محصول
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview mr-3">
                                <li class="nav-item">
                                    <a href="{{route('products.create')}}" class="nav-link">
                                        <i class="darkred  nav-icon fa fa-plus"></i>
                                        <p>محصول جدید</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('products.index')}}" class="nav-link">
                                        <i class="darkred  nav-icon fa fa-list-ul "></i>
                                        <p>محصول ها</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview {{ Route::is('users.create') ? 'menu-open':''}} {{ Route::is('users.index') ? 'menu-open':''}}"  >
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-user tomato "></i>
                                <p>
                                    کاربر
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview mr-3">
                                <li class="nav-item">
                                    <a href="{{route('users.create')}}" class="nav-link">
                                        <i class="tomato  nav-icon fa fa-plus"></i>
                                        <p>کاربر جدید</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('users.index')}}" class="nav-link">
                                        <i class="tomato  nav-icon fa fa-list-ul "></i>
                                        <p>کاربر ها</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item has-treeview  {{ Route::is('metas.create') ? 'metas-open':''}} {{ Route::is('metas.index') ? 'menu-open':''}} " >
                            <a href="#" class="nav-link">
                                <i class="nav-icon fa fa-meanpath redd "></i>
                                <p>
                                    متا
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview  mr-3">
                                <li class="nav-item">
                                    <a href="{{route('metas.create')}}" class="nav-link">
                                        <i class="redd  nav-icon fa fa-plus"></i>
                                        <p>متای جدید</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('metas.index')}}" class="nav-link">
                                        <i class="redd  nav-icon fa fa-list-ul "></i>
                                        <p>متا ها</p>
                                    </a>
                                </li>
                            </ul>
                        </li>



                        <li class="nav-item">
                            <a href="{{route('logout')}}" class="nav-link" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="nav-icon fa fa-power-off red"></i>
                                <p>خروج</p>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
   @yield('content')
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>CopyLeft &copy; 2018 <a href="#">{{__('word.storeName')}}</a>.</strong>
    </footer>

    <!-- Control Sidebar -->
    {{--<aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>--}}
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('js/app.js') }}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{asset('/admin/plugins/morris/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('/admin/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('/admin/plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="{{asset('/admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('/admin/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('/admin/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('/admin/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('/admin/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
{{--<script src="{{asset('/admin/dist/js/demo.js')}}"></script>--}}
<script src="{{asset('/admin/dist/js/bootstrap-toggle.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
@include('sweetalert::alert')
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

@yield('script')
</body>
</html>

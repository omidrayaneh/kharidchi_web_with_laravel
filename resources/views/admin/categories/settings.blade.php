@extends('admin.layouts.master')
@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.0/slimselect.min.css" rel="stylesheet"></link>
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">افرزودن ویژگی </i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">افزودن ویژگی به دسته بندی <span class="red">{{$category->title}}</span></h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    @if($errors->any())
                        <ol  style=" list-style-type: persian" >
                            <div class="  alert alert-danger mr-1">
                                @foreach($errors->all() as $error)
                                    <li >{{$error}}</li>
                                @endforeach
                            </div>
                        </ol>
                    @endif
                    <form method="post" action="/admin/categories/{{$category->slug}}/setting">
                        @csrf
                        <div class="card-body offset-md-2 col-md-6">

                            <div class="form-group" >
                                <label>ویژگی ها</label>
                                <select id="multiple"  name="attributes_id[]" multiple>
                                    @foreach($attGroups as $attGroup)
                                        <option class="multi-select-text" value="{{$attGroup->id}}" @if(in_array($attGroup->id, $category->attributeGroups->pluck('id')->toArray())) selected @endif>{{$attGroup->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <button type="submit" class="btn btn-success pull-left ">ذخیره</button>
                        </div>

                    </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.0/slimselect.min.js"></script>
    <script>

      var slim =  new SlimSelect({
            select: '#multiple',
          placeholder: 'جستجوی ویژگی'
        })


        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });




    </script>
@endsection



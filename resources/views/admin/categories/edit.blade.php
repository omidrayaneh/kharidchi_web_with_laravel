@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">دسته بندی</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">ویرایش دسته بندی <span class="red">{{$category->title}}</span></h3>
                </div>

                <!-- /.card-header -->
                @if($errors->any())
                    <ol  style=" list-style-type: persian" >
                        <div class="  alert alert-danger mr-1">
                            @foreach($errors->all() as $error)
                                <li >{{$error}}</li>
                            @endforeach
                        </div>
                    </ol>
                @endif
                <div class="card-body">
                    <form method="post" action="/admin/categories/{{$category->slug}}">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="card-body offset-md-2 col-md-6">
                            <div class="form-group">
                                <label>نام دسته بندی</label>
                                <input value="{{$category->title}}" name="title" type="text" class="form-control" placeholder="نام دسته بندی را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>نام سئو</label>
                                <input value="{{$category->meta_title}}" name="meta_title" type="text" class="form-control" placeholder="  نام سئو را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>توضیحات سئو</label>
                                <input value="{{$category->meta_description}}" name="meta_description" type="text" class="form-control" placeholder="توضیحات سئو را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>کلمات کلیدی</label>
                                <input value="{{$category->meta_keywords}}" name="meta_keywords" type="text" class="form-control" placeholder="کلمات کلیدی را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>سر دسته</label>
                                <select name="parent_id" class="form-control">
                                    <option value="">بدون سر دسته</option>
                                    @foreach($categories as $category_data)
                                        <option value="{{$category_data->id}}" @if($category->parent_id == $category_data->id) selected @endif>{{$category_data->title}}</option>
                                        @if(count($category_data->childrenRecursive) > 0)
                                            @include('admin.partials.category', ['categories'=>$category_data->childrenRecursive, 'level'=>1, 'selected_category'=>$category])
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group ">
                                <label>وضعیت انتشار<br>
                                    <input  @if($category->status ==1) checked @endif name="status" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                           data-offstyle="danger"> </label>
                            </div>
                            <div class="form-group ">
                                <label>وضعیت در اپلیکیشن<br>
                                    <input @if($category->ended ==1) checked @endif name="app" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                           data-offstyle="danger"> </label>
                            </div>
                            <button type="submit" class="btn btn-success pull-left ">ذخیره</button>
                        </div>

                    </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection



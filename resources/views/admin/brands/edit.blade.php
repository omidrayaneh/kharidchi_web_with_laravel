@extends('admin.layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">برند</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">ویرایش برند <span class="red">{{$brand->title}}</span></h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    @if($errors->any())
                        <ol  style=" list-style-type: persian" >
                            <div class="  alert alert-danger mr-1">
                                @foreach($errors->all() as $error)
                                    <li >{{$error}}</li>
                                @endforeach
                            </div>
                        </ol>
                    @endif
                    <form  method="post" action="/admin/brands/{{$brand->slug}}">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="card-body offset-md-2 col-md-8">
                            <div class="form-group">
                                <label>نام برند</label>
                                <input value="{{$brand->title}}" name="title" type="text" class="form-control" placeholder="نام ویژگی را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>توضیحات</label>
                                <textarea  id="textareaDescription" type="text" name="description" class="ckeditor form-control" placeholder="توضیحات محصول را وارد کنید...">{{$brand->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="photo">گالری تصاویر</label>
                                <input type="hidden" name="photo_id[]" id="brand-photo">
                                <div id="photo"  class="dropzone"></div>
                                <div class="row">
                                    @foreach($brand->photos as $photo)
                                        <div class="col-sm-3" id="updated_photo_{{$photo->id}}">
                                            <img width="100" height="100" src="{{$photo->path }}" >
                                            <button type="button" class="btn btn-danger" onclick="removeImages({{$photo->id}})" >حذف</button>
                                        </div>
                                    @endforeach
                                </div>
                            </div >
                            <button type="submit" onclick="brandGallery()" class="btn btn-success pull-left ">ذخیره</button>
                        </div>

                    </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('/admin/dist/js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{asset('/admin/plugins/ckeditor/ckeditor.js')}}"></script>

    <script>
        {{--const image = [];--}}
        {{--var img  =  @json($brand->photos);--}}

        {{--for (var i = 0; i<img.length ;i++){--}}
        {{--    image.push('http://kharidchi.test' + img[i].path)--}}
        {{--}--}}

        Dropzone.autoDiscover = false;
        var photosGallery = []
        var id ='';
        var photos = [].concat({{$brand->photos->pluck('id')}})

        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            removedfile: function(file) {
                var name = file.name;

                var token = $("meta[name='csrf-token']").attr("content");
                $.ajax(
                    {
                        url: "/admin/photos/" + id,
                        type: 'DELETE',
                        data: {
                            "id": id,
                            "_token": token,
                        },
                        success: function (result) {
                            //window.location.replace('/admin/galleries/');
                        }
                    });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            } ,
            acceptedFiles: ".jpg ,.png",
            maxFiles: 3,
            maxFilesize: 0.3,
            paramName: "file",
            acceptedMimeTypes: null,
            acceptParameter: null,
            enqueueForUpload: true,
            dictDefaultMessage:"افزودن عکس برند",
            dictFileTooBig:"اندازه عکس بزرگ است",// image size error message
            dictInvalidFileType:"فرمت فایل اشتباه است",// file type error message
            dictCancelUpload:"لغو آپلود",//cancel error message
            dictCancelUploadConfirmation:"آیا می خواهید آپلود را متوقف کنید؟", //cancel conform
            dictMaxFilesExceeded:"بیشتر از 3 عکس نمی توانید",
            dictRemoveFile:"حذف",// remove file

            url: "{{ route('photos.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                id = response.photo_id;
                photosGallery.push(response.photo_id)
            }
        });
        brandGallery = function(){
            document.getElementById('brand-photo').value = photosGallery.concat(photos)
        };



        //
        // for (var j = 0; j<image.length ;j++){
        //
        //     var mockFile = { name: "Filename", size: 12345 };
        //     // Call the default addedfile event handler
        //     drop.emit("addedfile", mockFile);
        //     // And optionally show the thumbnail of the file:
        //
        //     drop.emit("thumbnail", mockFile, image[j]);
        // }

        CKEDITOR.replace('textareaDescription',{
            customConfig: 'config.js',
            toolbar: 'simple',
            language: 'fa',
            removePlugins: 'cloudservices, easyimage'
        });

        removeImages = function(id){
            console.log(photos.length)

            var index = photos.indexOf(id)
            photos.splice(index, 1);
            document.getElementById('updated_photo_' + id).remove();
        };
    </script>


@endsection



@extends('admin.layouts.master')

@section('content')
    <att-value-list></att-value-list>
@endsection

@section('script')
    <script>

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    </script>
@endsection

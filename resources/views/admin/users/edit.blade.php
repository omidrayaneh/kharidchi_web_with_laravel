@extends('admin.layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">کاربر ها</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">ویرایش کاربر <span class="red">{{$user->name}} {{$user->family}} </span></h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    @if($errors->any())
                        <ol  style=" list-style-type: persian" >
                            <div class="  alert alert-danger mr-1">
                                @foreach($errors->all() as $error)
                                    <li >{{$error}}</li>
                                @endforeach
                            </div>
                        </ol>
                    @endif
                    <form  method="post" action="/admin/users/{{$user->id}}">
                        @csrf
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="card-body offset-md-2 col-md-8">
                            <div class="form-group">
                                <label>نام</label>
                                <input value="{{$user->name}}"  name="name" type="text" class="form-control" placeholder="نام  را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>نام خانوادگی</label>
                                <input value="{{$user->family}}"  name="family" type="text" class="form-control" placeholder="نام خانوادگی را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>موبایل</label>
                                <input value="{{$user->mobile}}" name="mobile" type="text" class="form-control" placeholder="موبایل  را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>تلفن</label>
                                <input value="{{$user->phone}}" name="phone" type="text" class="form-control" placeholder="تلفن  را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>کارت بانکی</label>
                                <input value="{{$user->cart}}" name="cart" type="text" class="form-control" placeholder="کارت بانکی را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>ایمیل</label>
                                <input value="{{$user->email}}" name="email" type="email" class="form-control" placeholder="ایمیل  را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>کد ملی</label>
                                <input value="{{$user->national_code}}" name="national_code" type="text" class="form-control" placeholder="کد ملی  را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>نقش</label>
                                <select name="role" class="form-control">
                                    <option value="member" @if($user->role=="member") selected @endif>کاربر عادی</option>
                                    <option value="manager" @if($user->role=="manager") selected @endif>مدیر</option>
                                    <option value="admin" @if($user->role=="admin")selected @endif>ادمین</option>
                                </select>
                            </div>
                            <div class="form-group ">
                                <label>وضعیت<br>
                                    <input  @if($user->status ==1) checked @endif name="status" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                            data-offstyle="danger"> </label>
                            </div>
                            <div class="form-group">
                                <label>رمز عبور</label>
                                <input   name="password" type="text" class="form-control" placeholder="رمز عبور  را وارد کنید ...">
                            </div>
                            <button type="submit" class="btn btn-success pull-left ">ذخیره</button>
                        </div>

                    </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection



@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">متون</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">متون</h3>
                    <div class="text-left">
                        <a class="btn btn-app" href="{{route('texts.create')}}">
                            <i class="fa fa-plus green"></i> جدید
                        </a>
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive">
                    <table id="example2" class="table table-bordered table-hover ">
                        <thead>
                        <tr role="row" class="text-center">
                            <th>#</th>
                            <th>عنوان متن</th>
                            <th>جزئیات متن</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </thead>
                        <tbody>
                        @foreach($texts as $key=>$text)
                            <tr class="odd text-center">
                                <td> {{ $texts->firstItem() + $key }}</td>
                               <td>
                                   @if($text->title == 'privacy')
                                       <span class="float badge bg-info">حریم خصوصی</span>
                                   @elseif($text->title == 'about')
                                       <span class="float badge bg-info">درباره ما</span>
                                   @elseif($text->title == 'contact')
                                       <span class="float badge bg-info">تماس با ما</span>
                                   @elseif($text->title == 'careers')
                                       <span class="float badge bg-info">فرصت های شغلی</span>
                                   @elseif($text->title == 'returning')
                                       <span class="float badge bg-info">رویه‌های بازگرداندن کالا</span>
                                   @elseif($text->title == 'register-product')
                                       <span class="float badge bg-info">نحوه ثبت سفارش</span>
                                   @elseif($text->title == 'send-product')
                                       <span class="float badge bg-info">رویه ارسال سفارش</span>
                                   @elseif($text->title == 'payment-product')
                                       <span class="float badge bg-info">شیوه‌های پرداخت</span>
                                   @elseif($text->title == 'teams')
                                       <span class="float badge bg-info">شرایط استفاده</span>
                                   @elseif($text->title == 'question')
                                       <span class="float badge bg-info">پرسش و پاسخ</span>
                                   @endif
                               </td>
                                <td>{!! Str::limit($text->text,50 , ' ...') !!}</td>
                                <td>
                                    @if($text->status == 1)
                                        <span class="float badge bg-success">منتشر شده</span>
                                    @else
                                        <span class="float badge bg-danger">منتشر نشده</span>
                                    @endif
                                </td>
                                <td>
                                    <div>
                                        <a class="btn-delete blue mt-auto ml-1 mr-1"
                                           href="{{route('texts.edit', $text->id)}}">
                                            <i class="fa fa-edit" data-toggle="tooltip" title="ویرایش"></i>
                                        </a>
                                        |
                                        <button data-id="{{ $text->id }}" class="btn-delete deleteRecord">
                                            <i class="fa fa-trash red" data-toggle="tooltip" title="حذف"></i>
                                        </button>
                                        {{-- </form>--}}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="col-md-12 pagination pagination-centered ">
                    {!! $texts->links() !!}
                </div>
            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script>

        $(".deleteRecord").click(function () {
            Swal.fire({
                title: 'هشدار!',
                text: "آیا از حذف سطر جاری مطمئن هستید؟",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'بله, حذف کن!',
                cancelButtonText: 'خیر'
            }).then((result) => {
                if (result.value) {
                    var id = $(this).data("id");
                    var token = $("meta[name='csrf-token']").attr("content");
                    $.ajax(
                        {
                            url: "/admin/texts/" + $(this).data("id"),
                            type: 'DELETE',
                            data: {
                                "id": id,
                                "_token": token,
                            },
                            success: function (result) {
                                window.location.replace('/admin/texts');
                            }
                        });
                    Swal.fire(
                        'حذف!',
                        'با موفقیت حذف شد.',
                        'success'
                    )
                }
            });

        });

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    </script>
@endsection

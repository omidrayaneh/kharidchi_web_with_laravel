@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">متون</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">ایجاد متن جدید</h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    @if($errors->any())
                        <ol style=" list-style-type: persian">
                            <div class="  alert alert-danger mr-1">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </div>
                        </ol>
                    @endif
                    <form method="post" action="/admin/texts">
                        @csrf
                        <div class="card-body offset-md-2 col-md-8">
                            <div class="form-group">
                                <label>عنوان متن</label>
                                <select class="form-control" name="title" id="">
                                    <option value="">انتخاب کنید...</option>
                                    <option value="privacy">حریم خصوصی</option>
                                    <option value="teams">شرایط استفاده</option>
                                    <option value="about">درباره ما</option>
                                    <option value="contact">تماس با ما</option>
                                    <option value="careers">فرصت‌های شغلی</option>
                                    <option value="returning">رویه‌های بازگرداندن کالا</option>
                                    <option value="question">پاسخ به پرسش‌های متداول</option>
                                    <option value="register-product">نحوه ثبت سفارش</option>
                                    <option value="send-product">رویه ارسال سفارش</option>
                                    <option value="payment-product">شیوه‌های پرداخت</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>پرسش</label>
                                <textarea  type="text" name="question" class=" form-control"
                                           placeholder="درصورت ایجاد پرسش،متن پرسش را وارد کنید..."></textarea>
                            </div>
                            <div class="form-group">
                                <label>توضیحات متن</label>
                                <textarea id="textareaDescription" type="text" name="text" class="ckeditor form-control"
                                          placeholder="توضیحات متن را وارد کنید..."></textarea>
                            </div>
                            <div class="form-group ">
                                <label>وضعیت</label>
                                <br>
                                <input name="status" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                       data-offstyle="danger">
                            </div>
                            <button type="submit" class="btn btn-success pull-left ">ذخیره
                            </button>

                        </div>
                    </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('/admin/plugins/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace('textareaDescription', {
            customConfig: 'config.js',
            toolbar: 'simple',
            language: 'fa',
            removePlugins: 'cloudservices, easyimage'
        });
    </script>
@endsection




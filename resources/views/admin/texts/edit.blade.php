@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">متون</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">ویرایش متن <span class="red">{{$text->title}}</span></h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    @if($errors->any())
                        <ol style=" list-style-type: persian">
                            <div class="  alert alert-danger mr-1">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </div>
                        </ol>
                    @endif
                    <form method="post" action="/admin/texts/{{$text->id}}">
                        @csrf
                        @method('PATCH')
                        <div class="card-body offset-md-2 col-md-8">
                            <div class="form-group">
                                <label>عوان متن</label>
                                <select class="form-control" name="title" id="">
                                    <option value="">انتخاب کنید...</option>
                                    <option value="privacy" @if($text->title == 'privacy') selected @endif>حریم خصوصی</option>
                                    <option value="about"  @if($text->title == 'about') selected @endif>درباره ما</option>
                                    <option value="about"  @if($text->title == 'teams') selected @endif>شرایط استفاده</option>
                                    <option value="about"  @if($text->title == 'contact') selected @endif>تماس با ما</option>
                                    <option value="about"  @if($text->title == 'careers') selected @endif>فرصت های شغلی</option>
                                    <option value="returning"  @if($text->title == 'returning') selected @endif>رویه‌های بازگرداندن کالا</option>
                                    <option value="question"  @if($text->title == 'question') selected @endif>پرسش و پاسخ</option>
                                    <option value="register-product"  @if($text->title == 'register-product') selected @endif>نحوه ثبت سفارش</option>
                                    <option value="send-product"  @if($text->title == 'send-product') selected @endif>رویه ارسال سفارش</option>
                                    <option value="payment-product"  @if($text->title == 'payment-product') selected @endif>شیوه‌های پرداخت</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>پرسش</label>
                                <textarea type="text"  name="question" class=" form-control"
                                          placeholder="درصورت ایجاد پرسش،متن پرسش را وارد کنید...">{{$text->question}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>توضیحات متن</label>
                                <textarea id="textareaDescription" type="text" name="text" class="ckeditor form-control"
                                          placeholder="توضیحات متن را وارد کنید...">{{$text->text}}</textarea>
                            </div>
                            <div class="form-group ">
                                <label>وضعیت</label>
                                <br>
                                <input name="status" @if($text->status  ) checked @endif type="checkbox" data-toggle="toggle" data-onstyle="success"
                                       data-offstyle="danger">
                            </div>
                            <button type="submit" class="btn btn-success pull-left ">ذخیره
                            </button>

                        </div>
                    </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="{{asset('/admin/plugins/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace('textareaDescription', {
            customConfig: 'config.js',
            toolbar: 'simple',
            language: 'fa',
            removePlugins: 'cloudservices, easyimage'
        });
    </script>
@endsection



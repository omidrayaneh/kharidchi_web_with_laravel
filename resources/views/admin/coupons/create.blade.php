@extends('admin.layouts.master')

@section('content')

    <div class="content-wrapper" id="app">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">داشبورد</h1>

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="blue">کوپن</i></li>
                            <li class="breadcrumb-item active">داشبورد</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-right">ایجاد کوپن جدید</h3>
                </div>

                <!-- /.card-header -->

                <div class="card-body">
                    @if($errors->any())
                        <ol  style=" list-style-type: persian" >
                            <div class="  alert alert-danger mr-1">
                                @foreach($errors->all() as $error)
                                    <li >{{$error}}</li>
                                @endforeach
                            </div>
                        </ol>
                    @endif
                    <form id="myForm" method="post" action="/admin/coupons">
                        @csrf
                        <div class="card-body offset-md-2 col-md-8">
                            <div class="form-group">
                                <label>عنوان</label>
                                <input name="title" type="text" class="form-control" placeholder="نام کوپن را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>کد</label>
                                <input name="code" type="text" class="form-control" placeholder="کد کوپن را وارد کنید ...">
                            </div>
                            <div class="form-group">
                                <label>قیمت</label>
                                <input name="price" type="number" class="form-control" placeholder="قیمت کوپن را وارد کنید ...">
                            </div>
                            <div class="form-group ">
                                <label>وضعیت</label>
                                <br>
                                    <input name="status" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                           data-offstyle="danger">
                            </div>
                            <button type="submit" onclick="productGallery()" class="btn btn-success pull-left ">ذخیره</button>
                        </div>

                    </form>
                </div>

            </div>
            <!-- /.card-body -->
        </section>
        <!-- /.content -->
    </div>
@endsection



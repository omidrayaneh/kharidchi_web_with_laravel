@extends('layouts.main-master')
@section('title')
    <title>پرسشهای متداول | فروشگاه اینترنتی {{__('word.storeName')}}  </title>
@endsection
@section('content')
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="page-cover mb-2">
            <div class="page-cover-title">
                <h1>پاسخ پرسش‌های پرتکرار</h1>
{{--                <div class="form-ui">--}}
{{--                    <form action="">--}}
{{--                        <div class="form-row">--}}
{{--                            <input type="text" class="input-ui pr-2" placeholder="پرسش خود را جستجو کنید">--}}
{{--                            <button class="btn btn-info">جستجو</button>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
            </div>
        </div>
        <div class="container main-container">
            <div class="row">
                <div class="col-12">
                    <div class="page info-page-cats dt-sl dt-sn pt-3 pb-2">
                        <div class="row">
{{--                            <div class="col-12 pr-4 mb-3">--}}
{{--                                <div class="section-title title-wide no-title-wide-before mb-1 no-after-title-wide">--}}
{{--                                    <img src="./assets/img/faq/1.png" width="60" alt="">--}}
{{--                                    <h2 class="font-weight-bold">ورود و ثبت نام</h2>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="col-12 filter-product mb-3">
                                <div class="accordion" id="accordionExample">
                                    @foreach($questions as $question )
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h2 class="mb-0">
                                                <button class="btn btn-block text-right collapsed" type="button"
                                                        data-toggle="collapse" data-target="#collapseOne"
                                                        aria-expanded="false" aria-controls="collapseOne">
                                                    {{$question->question}}
                                                    <i class="mdi mdi-chevron-down"></i>
                                                </button>
                                            </h2>
                                        </div>
                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                             data-parent="#accordionExample">
                                            <div class="card-body">
                                                {!! $question->text !!}
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="col-12">--}}
{{--                    <div class="page-question-not-found">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-12">--}}
{{--                                <div class="page-question-not-found-text">--}}
{{--                                    جواب یا پرسش خود را پیدا نکردید؟--}}
{{--                                    <br>--}}
{{--                                    روش‌های ارتباط با ما--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-6 col-sm-12 text-center">--}}
{{--                                <img src="./assets/img/faq/phone.svg" alt="">--}}
{{--                                <div class="page-contact-option-text">تماس تلفنی</div>--}}
{{--                                <div class="page-contact-option-text mr-3">۰۲۱-۶۱۹۳۰۰۰۰</div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-6 col-sm-12 text-center">--}}
{{--                                <img src="./assets/img/faq/email.svg" class="mb-5" alt="">--}}
{{--                                <a href="#" class="btn btn-info pr-4 pl-4">ارسال پیام</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>

        </div>
    </main>
@endsection

@extends('layouts.main-master')
@section('title')
    <title>افزودن آدرس جدید | فروشگاه اینترنتی {{__('word.storeName')}}  </title>
@endsection
@section('scripts')
    <link rel="stylesheet" href="{{asset('assets/css/vendor/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vendor/nouislider.min.css')}}">
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.js"></script>
    <script src="{{asset('assets/js/vendor/owl.carousel.min.js')}}" ></script>
    <script src="{{asset('assets/js/vendor/theia-sticky-sidebar.min.js')}}" ></script>
    <script src="{{asset('assets/js/vendor/jquery.horizontalmenu.js')}}" ></script>
    <script src="{{asset('assets/js/vendor/jquery.nice-select.min.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <script>
        let addressId = '';
        $(".deleteRecord").click(function () {
            Swal.fire({
                icon: 'info',
                title: 'هشدار',
                text: "آیا از حذف آدرس مطمئن هستید؟",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'خیر',
                confirmButtonText: 'بله',
                reverseButtons:true
            }).then((result) => {
                if (result.value) {
                    var id = $(this).data("id");
                    var token = $("meta[name='csrf-token']").attr("content");
                    $.ajax(
                        {
                            url: "/profile/addresses/" + $(this).data("id"),
                            type: 'DELETE',
                            data: {
                                "id": id,
                                "_token": token,
                            },
                            success: function (result) {
                                window.location.replace('/profile/addresses/');
                            }
                        });
                    Swal.fire(
                        'حذف!',
                        'با موفقیت حذف شد.',
                        'success'
                    )
                }
            });

        });

        $(".editRecord").click(function () {

            let address = $(this).data("id");
            let province = [];
            let provinces =@json($provinces);
            addressId = address.id;
            document.getElementById("name").value = address.name;
            document.getElementById("postal_code").value = address.post_code;
            document.getElementById("mobile").value = address.phone;
            document.getElementById("plaque").value = address.plaque;
            document.getElementById("address").value = address.address;
            document.getElementById("province_edit_id").value = address.province_id;

            for (let i = 0; i < provinces.length; i++) {

                if (provinces[i].id === address.province_id) {
                    province = provinces[i]
                }
            }
            let city = province.cities
            let sel = document.getElementById('city_edit_id');
            for (var i = 0; i < city.length; i++) {
                var opt = document.createElement('option');
                opt.innerHTML = city[i].name;
                opt.value = city[i].id;
                sel.appendChild(opt);
            }
            document.getElementById("city_edit_id").value = address.city_id;




        });


        $(".createButton").click(function(e) {
            if ($('#EditForm').valid()) {
                let editAddress=[];
                var token = $("meta[name='csrf-token']").attr("content");

                editAddress.id =addressId;
                editAddress.name =document.getElementById("name").value ;
                editAddress.post_code= document.getElementById("postal_code").value ;
                editAddress.phone= document.getElementById("mobile").value ;
                editAddress.plaque= document.getElementById("plaque").value ;
                editAddress.address= document.getElementById("address").value ;
                editAddress.province_id= document.getElementById("province_edit_id").value ;
                editAddress.city_id= document.getElementById("city_edit_id").value ;
                $.ajax(
                    {
                        url: "/profile/addresses/" + addressId,
                        type: 'PATCH',
                        data: {
                            "id": editAddress.id,
                            "name": editAddress.name,
                            "phone": editAddress.phone,
                            "post_code": editAddress.post_code,
                            "plaque": editAddress.plaque,
                            "address": editAddress.address,
                            "province_id": editAddress.province_id,
                            "city_id": editAddress.city_id,
                            "_token": token,
                        },
                        success: function (result) {
                            window.location.replace('/profile/addresses/');
                        }
                    });
            }
        });

        $("#EditForm").validate({
            errorLabelContainer: 'errors',
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                },
                mobile: {
                    required: true,
                    minlength:11,
                    maxlength:11}
                ,
                postal_code: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                },
                address: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: "لطفا نام را وارد کنید",
                    minlength: "حداقل سه کاراکتر"
                },
                mobile: {
                    required: "لطفا موبایل را وارد کنید",
                    minlength: "11 کاراکتر",
                    maxlength: "11 کاراکتر",
                },
                postal_code: {
                    required: "لطفا کد پستی را وارد کنید",
                    minlength: "10 کاراکتر",
                    maxlength: "10 کاراکتر",
                },
                address: {
                    required: "لطفا آدرس پستی  خود را وارد کنید",
                },
            }
        });
        $(".selectRecord").click(function () {
            Swal.fire({
                title: 'آیا از انتخاب آدرس مطمئن هستید؟',
                text: 'آدرس مورد نظر به عنوان پش فرض آدرس انتخاب می شود',
                icon: 'warning',
                confirmButtonText: 'آره، انتخاب کن',
                cancelButtonText: 'نه، منصرف شدم!',
                cancelButtonColor:'red',
                showCancelButton: true,
                reverseButtons:true
            }).then((result) => {
                if (result.value) {
                    var id = $(this).data("id");
                    var token = $("meta[name='csrf-token']").attr("content");
                    $.ajax(
                        {
                            url: "/profile/addresses/enable/" + $(this).data("id"),
                            type: 'post',
                            data: {
                                "id": id,
                                "_token": token,
                            },
                            success: function (result) {
                                window.location.replace('/profile/addresses/');
                            }
                        });

                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                        icon: 'error',
                        title: 'لغو',
                        text: 'عمیلات انتخاب، لغو شد',
                        showConfirmButton: true,
                        timer: 1500
                    })
                }
            });

        });
    </script>
@endsection
@section('content')

    <div id="app">
        <!-- Start Modal location new -->
        <div class="modal fade" id="modal-location" role="dialog" aria-labelledby="exampleModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-lg send-info modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">
                            <i class="now-ui-icons location_pin"></i>
                            افزودن آدرس جدید
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-ui dt-sl">
                                <form class="form-account" method="post" action="/profile/addresses">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    نام و نام خانوادگی
                                                </h4>
                                            </div>
                                            <div class="form-row">
                                                <input name="name" type="text" required
                                                       oninvalid="this.setCustomValidity('لطفا نام و نام خانوادگی را بنویسید ')"
                                                       oninput="this.setCustomValidity('')"
                                                       class="input-ui pr-2 text-right"
                                                       placeholder="نام خود را وارد نمایید">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    شماره موبایل
                                                </h4>
                                            </div>
                                            <div class="form-row">
                                                <input required pattern="[0]{1}[0-9]{10}"
                                                       oninvalid="this.setCustomValidity('لطفا شماره موبایل را بنویسید ')"
                                                       name="mobile" class="input-ui pl-2 dir-ltr text-left" type="tel"
                                                       oninput="this.setCustomValidity('')"

                                                       placeholder="09xxxxxxxxx">
                                            </div>
                                        </div>
                                        <selected-city :address="{{$addresses}}" :provinces="{{$provinces}}"></selected-city>
                                        <div class="col-12 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    آدرس پستی
                                                </h4>
                                            </div>
                                            <div class="form-row">
                                                    <textarea name="address" class="input-ui pr-2 text-right"
                                                              required
                                                              oninvalid="this.setCustomValidity('لطفا آدرس پستی را بنویسید ')"
                                                              oninput="this.setCustomValidity('')"
                                                              placeholder=" آدرس تحویل گیرنده را وارد نمایید"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    پلاک
                                                </h4>
                                            </div>
                                            <div class="form-row">
                                                <input name="plaque"
                                                       class="input-ui pl-2 dir-ltr text-left placeholder-right"
                                                       type="text" placeholder=" پلاک را  بنویسید">
                                            </div>
                                        </div>
                                        <div class="col-6 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    کد پستی
                                                </h4>
                                            </div>
                                            <div class="form-row">
                                                <input name="postal_code"
                                                       class="input-ui pl-2 dir-ltr text-left placeholder-right"
                                                       required
                                                       oninvalid="this.setCustomValidity('لطفا کد پستی را بنویسید ')"
                                                       oninput="this.setCustomValidity('')"
                                                       type="text" placeholder=" کد پستی را بدون خط تیره بنویسید">
                                            </div>
                                        </div>
                                        <div class="col-12 pr-4 pl-4">
                                            <button type="submit" class="btn btn-sm btn-primary btn-submit-form">ثبت
                                                و
                                                ارسال به این آدرس
                                            </button>
                                            <button type="button" class="btn-link-border float-left mt-2">انصراف
                                                و بازگشت
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal location new -->

        <!-- Start Modal location edit -->
        <div class="modal fade" id="modal-location-edit" role="dialog" aria-labelledby="exampleModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-lg send-info modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">
                            <i class="now-ui-icons location_pin"></i>
                            ویرایش آدرس
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-ui dt-sl">
                                <form class="form-account"  role="form"  id="EditForm">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    نام و نام خانوادگی
                                                </h4>
                                            </div>
                                            <div class="form-row has-error" >
                                                <input id="name" name="name" type="text"
                                                       class=" has-error input-ui pr-2 text-right"
                                                       placeholder="نام خود را وارد نمایید" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    شماره موبایل
                                                </h4>
                                            </div>
                                            <div class="form-row">
                                                <input id="mobile"
                                                       name="mobile" class="input-ui pl-2 dir-ltr text-left" type="number"
                                                       placeholder="09xxxxxxxxx">
                                            </div>
                                        </div>
                                        <edit-address :provinces="{{$provinces}}"></edit-address>
                                        <div class="col-12 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    آدرس پستی
                                                </h4>
                                            </div>
                                            <div class="form-row">
                                                    <textarea id="address" name="address"
                                                              class="input-ui pr-2 text-right"
                                                              placeholder=" آدرس تحویل گیرنده را وارد نمایید"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    پلاک
                                                </h4>
                                            </div>
                                            <div class="form-row">
                                                <input id="plaque" name="plaque"
                                                       class="input-ui pl-2 dir-ltr text-left placeholder-right"
                                                       type="text" placeholder=" پلاک را  بنویسید">
                                            </div>
                                        </div>
                                        <div class="col-6 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    کد پستی
                                                </h4>
                                            </div>
                                            <div class="form-row">
                                                <input id="postal_code" name="postal_code"
                                                       class="input-ui pl-2 dir-ltr text-left placeholder-right"
                                                       type="text" placeholder=" کد پستی را بدون خط تیره بنویسید">
                                            </div>
                                        </div>
                                        <div class="col-12 pr-4 pl-4">
                                            <button type="button"
                                                    class="btn btn-sm btn-primary btn-submit-form createButton ">ثبت
                                                و
                                                ارسال به این آدرس
                                            </button>
                                            <button type="button" class="btn-link-border float-left mt-2">انصراف
                                                و بازگشت
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal location edit -->

        <!-- Start Modal remove-location -->
        <div class="modal fade" id="remove-location" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mb-3" id="exampleModalLabel">آیا مطمئنید که
                            این آدرس حذف شود؟</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="remodal-general-alert-button remodal-general-alert-button--cancel"
                                data-dismiss="modal">خیر
                        </button>
                        <button type="button"
                                class="remodal-general-alert-button remodal-general-alert-button--approve">بله
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal remove-location -->

        <main class="main-content dt-sl mt-4 mb-3">


            <div class="container main-container">
                <div class="row">

                    <!-- Start Sidebar -->
                @include('layouts.profile')
                <!-- End Sidebar -->

                    <!-- Start Content -->
                    <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12">
                        <div class="row">
                            <div class="col-12">
                                <div
                                    class="section-title text-sm-title title-wide mb-1 no-after-title-wide dt-sl mb-2 px-res-1">
                                    <h2>آدرس ها</h2>
                                </div>
                                <div class="dt-sl">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="card-horizontal-address text-center px-4">
                                                <button class="checkout-address-location" data-toggle="modal"
                                                        data-target="#modal-location">
                                                    <strong>ایجاد آدرس جدید</strong>
                                                    <i class="mdi mdi-map-marker-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        @foreach($addresses as $address)
                                            <div class="col-lg-6 col-md-12">
                                                <div class="card-horizontal-address">
                                                    <div class="card-horizontal-address-desc">
                                                        @if($address->status)
                                                            <a ><i class="fa fa-star yellow"></i> </a>
                                                        @else
                                                            <a type="button"  data-id="{{ $address->id }}"  class="selectRecord"><i class="fa fa-star"></i> </a>
                                                        @endif
                                                        <h4 class="card-horizontal-address-full-name">{{$address->name}}</h4>
                                                        <p>
                                                            {{$address->address}}
                                                        </p>
                                                    </div>

                                                    <div class="card-horizontal-address-data">
                                                        <ul class="card-horizontal-address-methods float-right">
                                                            <li class="card-horizontal-address-method">
                                                                <i class="mdi mdi-email-outline"></i>
                                                                کدپستی : <span>{{$address->post_code}}</span>
                                                            </li>
                                                            <li class="card-horizontal-address-method">
                                                                <i class="mdi mdi-cellphone-iphone"></i>
                                                                تلفن همراه : <span>{{$address->phone}}</span>
                                                            </li>
                                                        </ul>
                                                        <div class="card-horizontal-address-actions">
                                                            <button data-id="{{ $address }}" class="btn-note editRecord"
                                                                    data-toggle="modal"
                                                                    data-target="#modal-location-edit">ویرایش
                                                            </button>
                                                            <button data-id="{{ $address->id }}"
                                                                    class="btn-note deleteRecord">
                                                                حذف
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- End Content -->

                </div>
                <!-- Start Product-Slider -->

                <!-- End Product-Slider -->
            </div>

        </main>
    </div>

@endsection

@section('scripts')

    <script src="{{asset('assets/js/vendor/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/nouislider.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/wNumb.js')}}"></script>
    <script src="{{asset('assets/js/vendor/ResizeSensor.min.js')}}"></script>
    <script src="{{asset('assets/js/vendor/theia-sticky-sidebar.min.js')}}"></script>



@endsection

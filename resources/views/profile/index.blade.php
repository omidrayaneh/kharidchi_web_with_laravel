@extends('layouts.main-master')
@section('title')
    <title>پروفایل کاربری | فروشگاه اینترنتی {{__('word.storeName')}}  </title>
@endsection
@section('content')
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">
            <div class="row">

                <!-- Start Sidebar -->
              @include('layouts.profile')
                <!-- End Sidebar -->

                <!-- Start Content -->
                <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12">
                    <div class="row">
                        <div class="col-xl-6 col-lg-12">
                            <div class="px-3">
                                <div
                                    class="section-title text-sm-title title-wide mb-1 no-after-title-wide dt-sl mb-2">
                                    <h2>اطلاعات شخصی</h2>
                                </div>
                                <div class="profile-section dt-sl">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="label-info">
                                                <span>نام و نام خانوادگی:</span>
                                            </div>
                                            <div class="value-info">
                                                <span>{{auth()->user()->name}} {{auth()->user()->family}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="label-info">
                                                <span>پست الکترونیک:</span>
                                            </div>
                                            <div class="value-info">
                                                <span>{{auth()->user()->email}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="label-info">
                                                <span>شماره تلفن همراه:</span>
                                            </div>
                                            <div class="value-info">
                                                <span>{{auth()->user()->mobile}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="label-info">
                                                <span>کد ملی:</span>
                                            </div>
                                            <div class="value-info">
                                                <span>{{auth()->user()->national_code}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="label-info">
                                                <span>دریافت خبرنامه:</span>
                                            </div>
                                            <div class="value-info">
                                                @if(auth()->user()->subscribe==1)
                                                <span>
                                                        بله
                                                    </span>
                                                    @else
                                                    <span>خیر</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="label-info">
                                                <span>شماره کارت:</span>
                                            </div>
                                            <div class="value-info">
                                                <span>{{auth()->user()->cart}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="profile-section-link">
                                        <a href="{{route('additional-info')}}" class="border-bottom-dt">
                                            <i class="mdi mdi-account-edit-outline"></i>
                                            ویرایش اطلاعات شخصی
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(count($favorites)!=0 )

                        <div class="col-xl-6 col-lg-12">
                            <div class="px-3">
                                <div
                                    class="section-title text-sm-title title-wide mb-1 no-after-title-wide dt-sl mb-2">
                                    <h2>لیست آخرین علاقه‌مندی‌ها</h2>
                                </div>
                                <div class="profile-section dt-sl">
                                    <ul class="list-favorites">
                                        @foreach($favorites as $favorite)
                                        <li>
                                            <a href="{{route('product',['sku'=>$favorite->favourable->sku,'slug'=>$favorite->favourable->slug])}}">
                                                <img src="{{$favorite->favourable->photos[0]->path}}" alt="">
                                                <span>{{$favorite->favourable->title}}</span>
                                            </a>
                                            <form method="post" action="/favorite-add/{{$favorite->favourable->sku}}">
                                                @csrf
                                                <button type="submit">
                                                    <i class="mdi mdi-trash-can-outline"></i>
                                                </button>
                                            </form>
                                        </li>
                                        @endforeach
                                    </ul>
                                    <div class="profile-section-link">
                                        <a href="#" class="border-bottom-dt">
                                            <i class="mdi mdi-square-edit-outline"></i>
                                            مشاهده و ویرایش لیست مورد علاقه
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- Start Product-Slider -->
            <!-- End Product-Slider -->
        </div>
    </main>

@endsection

@extends('layouts.main-master')
@section('title')
    <title>ویرایش اطلاعات شخصی | فروشگاه اینترنتی {{__('word.storeName')}}  </title>
@endsection
@section('content')
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">
            <div class="row">

                <!-- Start Sidebar -->
                @include('layouts.profile')
                 <!-- End Sidebar -->
                <!-- Start Content -->
                <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12">
                    <div class="row">
                        <div class="col-md-10 col-sm-12 mx-auto">
                            <div class="px-3 px-res-0">
                                <div
                                    class="section-title text-sm-title title-wide mb-1 no-after-title-wide dt-sl mb-2 px-res-1">
                                    <h2>ویرایش اطلاعات شخصی</h2>
                                </div>
                                <div class="form-ui additional-info dt-sl dt-sn pt-4">
                                    <form method="post" action="{{route('additional.info')}}">
                                        @csrf
                                        <div class="form-row-title">
                                            <h3>نام</h3>
                                        </div>
                                        <div class="form-row">
                                            <input type="text" name="name" class="input-ui pr-2"
                                                   placeholder="نام خود را وارد نمایید" value="{{auth()->user()->name}}">
                                        </div>
                                        <div class="form-row-title">
                                            <h3>نام و نام خانوادگی</h3>
                                        </div>
                                        <div class="form-row">
                                            <input name="family" type="text" class="input-ui pr-2"
                                                   placeholder="نام خانوادگی خود را وارد نمایید" value="{{auth()->user()->family}}">
                                        </div>
                                        <div class="form-row-title">
                                            <h3>کد ملی</h3>
                                        </div>
                                        <div class="form-row">
                                            <input name="national_code" value="{{auth()->user()->national_code}}" type="text" class="input-ui pl-2 text-left dir-ltr"
                                                   placeholder="">
                                        </div>
                                        <div class="form-row mt-2">
                                            <div class="custom-control custom-checkbox float-right mt-2">
                                                <input type="checkbox"   @if(auth()->user()->foreign==1) checked @endif name="foreign" class="custom-control-input"
                                                       id="customCheck3">
                                                <label class="custom-control-label text-justify" for="customCheck3">
                                                    تبعه خارجی فاقد کد ملی هستم
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-row-title">
                                            <h3>شماره موبایل</h3>
                                        </div>
                                        <div class="form-row">
                                            <input  value="{{auth()->user()->phone}}" name="phone" type="text" class="input-ui pl-2 text-left dir-ltr"
                                                   placeholder="شماره موبایل خود را وارد نمایید" >
                                        </div>
                                        <div class="form-row-title">
                                            <h3>آدرس ایمیل</h3>
                                        </div>
                                        <div class="form-row">
                                            <input name="email"  type="email" class="input-ui pl-2 text-left dir-ltr"
                                                   placeholder="آدرس ایمیل خود را وارد نمایید" value="{{auth()->user()->email}}">
                                        </div>
                                        <div class="form-row mt-2">
                                            <div class="custom-control custom-checkbox float-right mt-2">
                                                <input @if(auth()->user()->subscribe==1) checked @endif name="subscribe" type="checkbox" class="custom-control-input"
                                                       id="customCheck4">
                                                <label class="custom-control-label text-justify" for="customCheck4">
                                                    اشتراک در خبرنامه دیدیکالا
                                                </label>
                                            </div>
                                        </div>
                                        <hr>
                                       <div class="bg-light px-3 pt-2 pb-2 mb-3 dt-sl">
                                            <div class="form-row-title">
                                                <h3>شماره کارت</h3>
                                            </div>
                                            <div class="form-row">
                                                <input value="{{auth()->user()->cart}}" name="cart" id="cart" type="text" class="input-ui pl-2 text-left dir-ltr"
                                                       placeholder="">
                                                <button type="button" onclick="checkCartDigit()" class="btn btn-info  float-left mt-2">بررسی اطلاعات</button>
                                                <span id="true" hidden class="blue btn-lg mt-2">&#128505;</span>
                                                <span id="false" hidden class="red btn-lg mt-2">&#128501;</span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row mt-3 justify-content-center">
                                            <button  type="submit" class="btn-primary-cm btn-with-icon ml-2">
                                                <i class="mdi mdi-account-circle-outline"></i>
                                                ثبت اطلاعات کاربری
                                            </button>
                                            <button  class="btn-primary-cm bg-secondary">انصراف</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Content -->

            </div>
          
        </div>
    </main>
@endsection
@section('scripts')
    <script>
        window.onload = checkCartDigit;
        function checkCartDigit() {
            var code =document.getElementById("cart").value;
            var L=code.length;
            if(L<16 || parseInt(code.substr(1,10),10)==0 || parseInt(code.substr(10,6),10)==0) return false;
            var c=parseInt(code.substr(15,1),10);
            var s=0;
            var k,d;
            for(var i=0;i<16;i++)
            {
                k=(i%2==0) ? 2 :1;
                d=parseInt(code.substr(i,1),10)*k;
                s+=(d >9) ? d-9 :d;
            }
            if((s%10)===0){
                document.getElementById("true").hidden = false;
                document.getElementById("false").hidden = true;
            }else{
                document.getElementById("true").hidden = true;
                document.getElementById("false").hidden = false;
            }
        }
    </script>
@endsection


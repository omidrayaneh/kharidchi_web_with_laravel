@extends('layouts.main-master')
@section('title')
    <title>علاقمندی ها | فروشگاه اینترنتی {{__('word.storeName')}}  </title>
@endsection
@section('content')
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">
            <div class="row">

                <!-- Start Sidebar -->
                 @include('layouts.profile')
               <!-- End Sidebar -->

                <!-- Start Content -->
                <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12">
                    <div class="row">
                        <div class="col-12">
                            <div
                                class="section-title text-sm-title title-wide mb-1 no-after-title-wide dt-sl mb-2 px-res-1">
                                <h2>علاقمندی ها</h2>
                            </div>
                            <div class="dt-sl">
                                <div class="row">
                                    @foreach($favorites as $favorite)
                                       <div class="col-lg-6 col-md-12">
                                        <div class="card-horizontal-product">
                                            <div class="card-horizontal-product-thumb">
                                                <a href="#">
                                                    <img src="{{$favorite->favourable->photos[0]->path}}" alt="">
                                                </a>
                                            </div>
                                            <div class="card-horizontal-product-content">
                                                <div class="card-horizontal-product-title">
                                                    <a href="#">
                                                        <h3>{{$favorite->favourable->title}}</h3>
                                                    </a>
                                                </div>
                                                <div class="rating-stars">
                                                    <i class="mdi mdi-star active"></i>
                                                    <i class="mdi mdi-star active"></i>
                                                    <i class="mdi mdi-star active"></i>
                                                    <i class="mdi mdi-star active"></i>
                                                    <i class="mdi mdi-star"></i>
                                                </div>
                                                <div class="card-horizontal-product-price">
                                                    <span>{{number_format($favorite->favourable->price - ($favorite->favourable->price * $favorite->favourable->discount)/100)}}</span>
                                                </div>
                                                <div class="card-horizontal-product-buttons">
                                                    <form method="post" action="/favorite-add/{{$favorite->favourable->sku}}">
                                                        @csrf
                                                        <a href="{{route('product',['sku'=>$favorite->favourable->sku,'slug'=>$favorite->favourable->slug])}}" class="btn">مشاهده محصول</a>
                                                        <button class="remove-btn">
                                                            <i class="mdi mdi-trash-can-outline"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Content -->

            </div>
            <!-- Start Product-Slider -->
            <!-- End Product-Slider -->
        </div>
    </main>

@endsection

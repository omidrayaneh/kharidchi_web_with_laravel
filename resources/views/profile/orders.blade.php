@extends('layouts.main-master')
@section('title')
    <title>سفارش ها | فروشگاه اینترنتی {{__('word.storeName')}}  </title>
@endsection
@section('content')
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">
            <div class="row">

                <!-- Start Sidebar -->
                @include('layouts.profile')


            <!-- End Sidebar -->

                <!-- Start Content -->

                <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12">
                        <div class="row">

                            <!-- Start Sidebar -->

                            <!-- End Sidebar -->

                            <!-- Start Content -->
                            <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div
                                            class="section-title text-sm-title title-wide mb-1 no-after-title-wide dt-sl mb-2 px-res-1">
                                            <h2>همه سفارش‌ها</h2>
                                        </div>
                                        <div class="dt-sl">
                                            <div class="table-responsive">
                                                <table class="table table-order">
                                                    <thead>
                                                    <tr>
                                                        <th>شماره سفارش</th>
                                                        <th>تاریخ ثبت سفارش</th>
                                                        <th>مبلغ قابل پرداخت</th>
                                                        <th>عملیات پرداخت</th>
                                                        <th>جزییات</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($orders as $order)
                                                    <tr>
                                                        <td>{{$order->id}}</td>
                                                        <td class="text-center">
                                                            {{\Hekmatinasser\Verta\Verta::instance($order->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}
                                                        </td>
                                                        <td>{{number_format($order->amount)}}</td>
                                                       @if($order->status ==1)
                                                            <td class="text-center"><span class="label label-success  green">تحویل داده شده</span></td>
                                                       @else
                                                            <td class="text-center"><span class="label label-danger red ">لغو شده</span></td>
                                                       @endif
                                                        <td class="details-link">
                                                            <a href="#">
                                                                <i class="mdi mdi-chevron-left"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                <div class="d-flex justify-content-center">
                                                    {{$orders->links()}}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Content -->

                        </div>

                        <!-- Start Product-Slider -->

                        <!-- End Product-Slider -->
                    </div>

                <!-- End Content -->

            </div>
            <!-- Start Product-Slider -->
            <!-- End Product-Slider -->
        </div>
    </main>

@endsection

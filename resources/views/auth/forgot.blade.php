@extends('layouts.master')
@section('title')
    <title>صفحه یازیابی رمز عبور</title>
@endsection
@section('content')
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">

            <div class="row">
                <div class="col-xl-4 col-lg-5 col-md-7 col-12 mx-auto">
                    <div class="form-ui dt-sl dt-sn pt-4">
                        <div class="section-title title-wide mb-1 no-after-title-wide">
                            <h2 class="font-weight-bold">یاد آوری کلمه عبور</h2>
                        </div>
                        <form method="post" action="{{route('forgot-password')}}">
                            @csrf
                            <div class="form-row-title">
                                <h3>شماره موبایل</h3>
                            </div>
                            <div class="form-row with-icon">
                                <input name="mobile" type="text" class="input-ui pr-2" placeholder="رمز عبور خود را وارد نمایید"
                                       required
                                       oninput="(function(e){e.setCustomValidity(''); return !e.validity.valid && e.setCustomValidity(' ')})(this)"
                                       oninvalid="this.setCustomValidity('لطفا  شماره مبایل خود را وارد کنید.')">
                                <i class="mdi mdi-lock-open-variant-outline"></i>
                            </div>
                            <div class="form-row mt-3">
                                <button class="btn-primary-cm btn-with-icon mx-auto w-100">
                                    <i class="mdi mdi-lock-reset"></i>
                                    یاد آوری کلمه عبور
                                </button>
                            </div>
                            <div class="form-footer text-right mt-3">
                                <span class="d-block font-weight-bold">کاربر جدید هستید؟</span>
                                <a href="{{route('register')}}" class="d-inline-block mr-3 mt-2">ثبت نام در {{__('word.storeName')}}</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </main>

@endsection

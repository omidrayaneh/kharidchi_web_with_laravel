@extends('layouts.main-master')
@section('title')
    <title>@if(!empty($meta)){{$meta->title}}@endif {{__('word.storeName')}} </title>
@endsection
@section('description')
    @if(!empty($meta)) <meta name="description" content="{{$meta->description}}">@endif
@endsection
@section('keyword')
    @if(!empty($meta))   <meta name="keywords" content="{{$meta->keyword}} ">@endif
@endsection
@section('content')
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">

            <!-- Start Main-Slider -->
            <div class="row mb-5" style="margin-top: 145px">
                @if(!empty($sidebar_banner))
                <aside class="sidebar col-xl-2 col-lg-3 col-12 order-2 order-lg-1 pl-0 hidden-md">
                    <!-- Start banner -->
                    <div class="sidebar-inner dt-sl">
                        <div class="sidebar-banner">
                            <a @if($sidebar_banner->detail != null || $sidebar_banner->detail != '') href="/search?q={{$sidebar_banner->detail}}" target="_top" @endif>
                                <img src="{{asset($sidebar_banner->path)}}" width="100%" height="329"
                                     alt="">
                            </a>
                        </div>
                    </div>
                    <!-- End banner -->
                </aside>
                @endif

                <div class=" @if(!empty($sidebar_banner)) col-xl-10 @else col-xl-12 @endif col-lg-9 col-12 order-1 order-lg-2">
                    <!-- Start main-slider -->
                    <section id="main-slider" class="main-slider carousel slide carousel-fade card hidden-sm"
                             data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach($sliders as $slider)
                               <li data-target="#main-slider" data-slide-to="{{$loop->index}}" @if($loop->index == 0) class="active" @endif ></li>
                            @endforeach
                        </ol>
                        @if(!empty($sliders))
                        <div class="carousel-inner">
                           @foreach($sliders as $slider)
                                <div class="carousel-item  @if($loop->index == 0) active @endif ">
                                    <a  type="submit" class="main-slider-slide" @if($slider->detail != null || $slider->detail != '')href="/search?q={{$slider->detail}}" @endif
                                       style="background-image: url({{asset($slider->path)}})">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        @endif
                        <a class="carousel-control-prev" href="#main-slider" role="button" data-slide="prev">
                            <i class="mdi mdi-chevron-right"></i>
                        </a>
                        <a class="carousel-control-next" href="#main-slider" data-slide="next">
                            <i class="mdi mdi-chevron-left"></i>
                        </a>
                    </section>
                    <section id="main-slider-res" class="main-slider carousel slide carousel-fade card d-none show-sm" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @if(!empty($res_sliders))
                                @foreach($res_sliders as $slider)
                                   <li data-target="#main-slider-res" data-slide-to="{{$loop->index}}" @if($loop->index == 0) class="active" @endif ></li>
                                @endforeach
                            @endif
                        </ol>
                        @if($res_sliders)
                        <div class="carousel-inner" >
                            @foreach($res_sliders as $slider)
                            <div class="carousel-item @if($loop->index == 0) active @endif">
                                <a class="main-slider-slide" @if($slider->detail != null || $slider->detail != '') href="/search?q={{$slider->detail}}" @endif>
                                    <img src="{{asset($slider->path)}}" alt=""
                                         class="img-fluid">
                                </a>
                            </div>
                            @endforeach
                        </div>
                        @endif
                        <a class="carousel-control-prev" href="#main-slider-res" role="button" data-slide="prev">
                            <i class="mdi mdi-chevron-right"></i>
                        </a>
                        <a class="carousel-control-next" href="#main-slider-res" data-slide="next">
                            <i class="mdi mdi-chevron-left"></i>
                        </a>
                    </section>
                    <!-- End main-slider -->
                </div>
            </div>

            <!-- End Main-Slider -->

            <!-- Start Banner -->
            @if(!empty($medium_banners))
            <div class="row mt-3 mb-5">
                @foreach($medium_banners as $banner)
                <div class="col-sm-6 col-12 mb-2">
                    <div class="widget-banner">
                        <a @if($banner->detail != null || $banner->detail != '') href="/search?q={{$banner->detail}}" @endif>
                            <img src="{{asset($banner->path)}}" alt="">
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            <!-- End Banner -->

            <!-- Start Banner -->
            @if(!empty($small_banners))
            <div class="row mt-3 mb-5">
                @foreach($small_banners as $banner)
                <div class="col-md-3 col-sm-6 col-6 mb-2">
                    <div class="widget-banner">
                        <a @if($banner->detail != null || $banner->detail != '') href="/search?q={{$banner->detail}}" @endif>
                            <img src="{{asset($banner->path)}}" alt="">
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            <!-- End Banner -->



            <!-- Start Product-Slider -->
            @if(!empty($products))
            <div class="row">
                <div class="col-xl-10 col-lg-12">

                    <section class="slider-section dt-sl mb-5">
                        <div class="row mb-3">
                            <div class="col-12">
                                <div class="section-title text-sm-title title-wide no-after-title-wide">
                                    <h2>جدید ترین ها</h2>
                                    <a href="{{route('get.productNew')}}">مشاهده همه</a>
                                </div>
                            </div>
                            <!-- Start Product-Slider -->
                            <div class="col-12 px-res-0">
                                <div class="product-carousel carousel-md owl-carousel owl-theme">
                                    @foreach($products as $product)
                                        <div class="item">
                                            <div class="product-card">
                                                <div class="product-head">
                                                    <div class="rating-stars">
                                                        <!--<i class="mdi mdi-star active"></i>-->
                                                        <!--<i class="mdi mdi-star active"></i>-->
                                                        <!--<i class="mdi mdi-star active"></i>-->
                                                        <!--<i class="mdi mdi-star active"></i>-->
                                                        <!--<i class="mdi mdi-star active"></i>-->
                                                    </div>
                                                    @if($product->discount!=0 && $product->qty!=0)
                                                        <div class="discount">
                                                            <span>{{$product->discount}}%</span>
                                                        </div>
                                                    @endif
                                                </div>
                                                <a class="product-thumb" target="_blank"
                                                   @if($product->qty!=0)    href="{{route('product',['sku'=>$product->sku,'slug'=>$product->slug])}}" @endif>
                                                    <img  height="170" src="{{$product->photos[0]->path}}" alt="{{$product->title}}">

                                                </a>
                                                <div class="product-card-body mt-3">
                                                    <h5 style="height: 50px;">
                                                        <a  class="product-meta" @if($product->qty!=0)href="{{route('product',['sku'=>$product->sku,'slug'=>$product->slug])}}" @endif>{{$product->title}}</a>
                                                    </h5>
                                                    <!--<a class="product-meta" @if($product->qty!=0) href="{{route('get.productList',['slug' => $product->categories[0]->slug,'sku'=>$product->categories[0]->sku])}}" @endif>{{$product->categories[0]->title}}</a>-->
                                                    @if($product->qty!=0)
                                                        @if($product->discount!=0)
                                                            <span class="product-price red">
                                                           <del> {{number_format($product->price)}} تومان</del>
                                                        </span>
                                                        <span class="product-price green">
                                                                {{number_format($product->price - ($product->price * $product->discount)/100)}} تومان
                                                            </span>

                                                        @else
                                                            <span class="product-price red">
                                                            {{number_format($product->price)}} تومان
                                                        </span>
                                                            <span class="product-price " style="visibility: hidden">
                                                                بدون تخفیف
                                                            </span>
                                                        @endif
                                                    @else
                                                        <div class="text-center">
                                                            <h5 class="">ناموجود</h5>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                            <!-- End Product-Slider -->
                        </div>
                    </section>

                </div>
                @if(!empty($random_product))
                <div class="col-xl-2 col-lg-3 hidden-lg pr-0">
                    <div class="widget-suggestion dt-sn pt-3 mt-3">
                        <div class="widget-suggestion-title">
                            <img src="./assets/img/theme/suggestion-title.png" alt="">
                        </div>
                        <div id="progressBar">
                            <div class="slide-progress"></div>
                        </div>
                        <div id="suggestion-slider" class="owl-carousel owl-theme">
                                <div class="item">
                                    <div class="product-card mb-3 shadow-unset">
                                        <div class="product-head">
                                            <div class="discount">
                                             @if($random_product->discount>0)   <span>{{$random_product->discount}}%</span>@endif
                                            </div>
                                        </div>
                                          <a class=""  href="{{route('product',['sku'=>$random_product->sku,'slug'=>$random_product->slug])}}" target="_blank">
                                              <img src="{{$random_product->photos[0]->path}}" >
                                          </a>
                                        <div class="product-card-body mt-3">
                                            <h5 style="height: 50px;">
                                                <a class="product-meta" href="{{route('product',['sku'=>$random_product->sku,'slug'=>$random_product->slug])}}">{{$random_product->title}}</a>
                                            </h5>
                                            <!--<a class="product-meta" href="{{route('product',['sku'=>$random_product->sku,'slug'=>$random_product->slug])}}">{{$random_product->categories[0]->title}}</a>-->
                                          @if($random_product->discount>0)   <span class="product-price red">
                                                           <del> {{number_format($random_product->price)}} تومان</del>
                                                        </span>@endif
                                            <span class="$random_product-price green">
                                                                {{number_format($random_product->price - ($random_product->price * $random_product->discount)/100)}} تومان
                                                            </span>                                        </div>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
                @endif
            </div>
            @endif
            <!-- End Product-Slider -->



            <!-- Start Banner -->
            <div class="row mt-3 mb-5">
                @if(!empty($large_banner))
                <div class="col-12">
                    <div class="widget-banner">
                        <a @if($large_banner->detail != null || $large_banner->detail != '') href="/search?q={{$large_banner->detail}}" @endif>
                            <img src="{{asset($large_banner->path)}}" alt="">
                        </a>
                    </div>
                </div>
                @endif
            </div>
            <!-- End Banner -->

            <!-- Start Product-Slider -->
            @if(!empty($sells_products))
            <section class="slider-section dt-sl mb-5">
                <div class="row mb-3">
                    <div class="col-12">
                        <div class="section-title text-sm-title title-wide no-after-title-wide">
                            <h2>پر فروش ترین ها</h2>
                            <a href="{{route('get.productSell')}}">مشاهده همه</a>
                        </div>
                    </div>

                    <!-- Start Product-Slider -->
                    <div class="col-12">
                        <div class="product-carousel carousel-lg owl-carousel owl-theme">
                            @foreach($sells_products as $sell_product)

                            <div class="item">
                                <div class="product-card mb-3">
                                    <div class="product-head">
                                        <div class="rating-stars">
                                            <!--<i class="mdi mdi-star active"></i>-->
                                            <!--<i class="mdi mdi-star active"></i>-->
                                            <!--<i class="mdi mdi-star active"></i>-->
                                            <!--<i class="mdi mdi-star active"></i>-->
                                            <!--<i class="mdi mdi-star active"></i>-->
                                        </div>
                                        @if($sell_product->discount!=0 &&  $product->qty!=0 )
                                            <div class="discount">
                                                <span>{{$sell_product->discount}}%</span>
                                            </div>
                                        @endif
                                    </div>
                                    <a class="product-thumb" target="_blank"
                                       @if($sell_product->qty!=0)  href="{{route('product',['sku'=>$sell_product->sku,'slug'=>$sell_product->slug])}}" @endif>
                                        <img  height="170" src="/storage/{{$sell_product->path}}" alt="{{$sell_product->title}}">
                                    </a>
                                    <div class="product-card-body mt-3">

                                        <h5 style="height: 50px;">
                                            <a class="product-meta" @if($product->qty!=0)  href="{{route('product',['sku'=>$sell_product->sku,'slug'=>$sell_product->slug])}}" @endif>{{$sell_product->title}}</a>
                                        </h5>
                                        @if($sell_product->qty!=0)
                                            @if($sell_product->discount!=0)
                                                <span class="product-price red">
                                                           <del> {{number_format($sell_product->price)}} تومان</del>
                                                        </span>
                                                <span class="product-price green">
                                                                {{number_format($sell_product->price - ($sell_product->price * $sell_product->discount)/100)}} تومان
                                                            </span>

                                            @else
                                                <span class="product-price red">
                                                            {{number_format($sell_product->price)}} تومان
                                                        </span>
                                                <span class="product-price " style="visibility: hidden">
                                                                بدون تخفیف
                                                            </span>
                                            @endif
                                        @else
                                            <div class="text-center">
                                                <h5 class="">ناموجود</h5>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- End Product-Slider -->

                </div>
            </section>
            @endif
            <!-- End Product-Slider -->

        </div>
    </main>
@endsection




<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#f7858d">
    <meta name="msapplication-navbutton-color" content="#f7858d">
    <meta name="apple-mobile-web-app-status-bar-style" content="#f7858d">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor/bootstrap.min.css')}}">
    <!-- Plugins -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vendor/jquery.horizontalmenu.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vendor/nouislider.min.css')}}">
    <!-- Font Icon -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor/materialdesignicons.min.css')}}">
    <!-- Main CSS File -->
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    @yield('title')
    @yield('style')
</head>

<body>

<div class="wrapper">

    <!-- Start mini-header -->
    <header class="mini-header pt-4 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="logo-area-mini-header">
                        @if(isset($logo->path))
                            <a href="{{route('/')}}">
                                <img src="{{asset($logo->path)}}" alt="{{$logo->title}}">
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- End mini-header -->

    <!-- Start main-content -->
@yield('content')
<!-- End main-content -->

    <!-- Start mini-footer -->
    @yield('layouts.footer')
<!--    <footer class="mini-footer dt-sl">
        <div class="container main-container">
            <div class="row">
                <div class="col-12">
                    <ul class="mini-footer-menu">
                        <li><a href="#">درباره موبایدک</a></li>
                        <li><a href="#">فرصت های شغلی</a></li>
                        <li><a href="#">تماس با ما</a></li>
                        <li><a href="#">همکاری با سازمان ها</a></li>
                    </ul>
                </div>
                <div class="col-12 mt-2 mb-3">
                    <div class="footer-light-text">
                        استفاده از مطالب فروشگاه اینترنتی موبایدک فقط برای مقاصد غیرتجاری و با ذکر منبع بلامانع است. کلیه حقوق این سایت متعلق به (فروشگاه آنلاین موبایدک) می‌باشد.
                    </div>
                </div>
                <div class="col-12 text-center">
                    <div class="copy-right-mini-footer">
{{--                        Copyright © 2021 {{__('word.storeName')}}--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>-->
    <!-- End mini-footer -->

</div>


<!-- Core JS Files -->
<script src="{{asset('assets/js/vendor/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/popper.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/bootstrap.min.js')}}"></script>
<!-- Plugins -->
<script src="{{asset('assets/js/vendor/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/jquery.horizontalmenu.js')}}"></script>
<script src="{{asset('assets/js/vendor/nouislider.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/wNumb.js')}}"></script>
<script src="{{asset('assets/js/vendor/ResizeSensor.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/theia-sticky-sidebar.min.js')}}"></script>
<!-- Main JS File -->
<script src="{{asset('assets/js/main.js')}}"></script>
@yield('script')
@include('sweetalert::alert')

</body>

</html>

<div class="col-xl-3 col-lg-4 col-12 w-res-sidebar sticky-sidebar">
    <div class="dt-sn mb-2">
        <ul class="checkout-summary-summary">
            <li>
                @if(\Cart::getTotalQuantity()!=0)
                    <span>مبلغ کل ({{\Cart::getTotalQuantity()}} کالا)</span><span>{{\Cart::gettotal()}} تومان</span>
                @endif
            </li>
                        <li class="checkout-summary-discount">
                            @php $total=0 @endphp
                            @foreach(\Cart::getcontent() as $item)
                                @php $total=$total + $item->attributes->discount_price*$item->quantity @endphp
                            @endforeach
                            <span>تخفیف</span><span>{{number_format($total)}} تومان  </span>
                        </li>
            <li class="checkout-summary-discount">
                @if(Session::has('coupon'))
                    @php
                        $coupon= session('coupon');
                        $total=  intval(str_replace(',', '', \Cart::gettotal()));
                        $total=  $total-$coupon['price'];

                    @endphp
                    {{--                    <input type="hidden" name="coupon_discount" value="{{$coupon['id']}}">--}}
                    <span> تخفیف کوپن {{$coupon['title']}}</span>
                    <span>{{number_format($coupon['price'])}} تومان  </span>
                @endif

            </li>
            <li>
                @if(!empty($cost))
            @php
                $total=  intval(str_replace(',', '', \Cart::gettotal()));
            @endphp
                @endif
                @if(!empty($cost))
                <span>هزینه ارسال<span class="help-sn" data-toggle="tooltip" data-html="true"
                                       data-placement="bottom"
                                       title="<div class='help-container is-right'><div class='help-arrow'></div><p class='help-text'>هزینه ارسال مرسولات می‌تواند وابسته به شهر و آدرس گیرنده متفاوت باشد. در صورتی که هر یک از مرسولات حداقل ارزشی برابر با {{number_format($cost->free)}}هزار تومان داشته باشد، آن مرسوله بصورت رایگان ارسال می‌شود.</p></div> ">
                                            <span class="mdi mdi-information-outline"></span>
                                               @if($total > $cost->free)
                                                   </span></span><span>رایگان</span>
                                               @else
                                                   </span></span><span>{{number_format($cost->price)}} تومان </span>
                                               @endif
                    @endif



            </li>

        </ul>
        <div class="checkout-summary-devider">
            <div></div>
        </div>
        <div class="checkout-summary-content">
            <div class="checkout-summary-price-title">مبلغ قابل پرداخت:</div>
            <div class="checkout-summary-price-value">
                @if(!empty(session('coupon')) && !empty($cost) && $total< $cost->free)
                    <span class="checkout-summary-price-value-amount">{{number_format($total+$cost->price - session('coupon')->price )}}</span>
                    تومان
                @elseif(!empty(session('coupon')) && !empty($cost) && $total> $cost->free  - session('coupon')->price)
                    <span class="checkout-summary-price-value-amount">{{number_format($total - session('coupon')->price)}}</span>
                    تومان
                @elseif(empty(session('coupon')) && !empty($cost) && $total < $cost->free)
                    <span class="checkout-summary-price-value-amount">{{number_format($total+$cost->price)}}</span>
                    تومان
                @elseif(empty(session('coupon')) && !empty($cost) && $total > $cost->free)
                    <span class="checkout-summary-price-value-amount">{{number_format($total)}}</span>
                    تومان
                @endif
            </div>
            <a href="#" class="mb-2 d-block">
                @stack('accessed-button')
            </a>
            <div>
                                    <span>
                                        کالاهای موجود در سبد شما ثبت و رزرو نشده‌اند، برای ثبت سفارش
                                        مراحل بعدی را تکمیل کنید.
                                    </span><span class="help-sn" data-toggle="tooltip" data-html="true"
                                                 data-placement="bottom"
                                                 title="<div class='help-container is-right'><div class='help-arrow'></div><p class='help-text'>محصولات موجود در سبد خرید شما تنها در صورت ثبت و پرداخت سفارش برای شما رزرو می‌شوند. در صورت عدم ثبت سفارش، {{__('word.storeName')}}  هیچگونه مسئولیتی در قبال تغییر قیمت یا موجودی این کالاها ندارد.</p></div>">
                                        <span class="mdi mdi-information-outline"></span>
                                    </span></div>
        </div>
    </div>
    <div class="dt-sn checkout-feature-aside pt-4">
        <ul>
            <li class="checkout-feature-aside-item">
                <img src="./assets/img/svg/return-policy.svg" alt="">
                هفت روز ضمانت تعویض
            </li>
            <li class="checkout-feature-aside-item">
                <img src="./assets/img/svg/payment-terms.svg" alt="">
                پرداخت در محل با کارت بانکی
            </li>
            <li class="checkout-feature-aside-item">
                <img src="./assets/img/svg/delivery.svg" alt="">
                تحویل اکسپرس
            </li>
        </ul>
    </div>
</div>

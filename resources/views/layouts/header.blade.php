<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#f7858d">
    <meta name="msapplication-navbutton-color" content="#f7858d">
    <meta name="apple-mobile-web-app-status-bar-style" content="#f7858d">
    <title>Shopping-complete-buy Page</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor/bootstrap.min.css')}}">
    <!-- Plugins -->
    <!-- Font Icon -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor/materialdesignicons.min.css')}}">
    <!-- Main CSS File -->
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
</head>

<body>

<div class="wrapper shopping-page">

    <!-- Start header-shopping -->
        @yield('content')
    <!-- End header-shopping -->

    <!-- Start main-content -->
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">

            <div class="row">
                <div class="cart-page-content col-12 px-0">
                    <div class="checkout-alert dt-sn mb-4">
                        <div class="circle-box-icon successful">
                            <i class="mdi mdi-check-bold"></i>
                        </div>
                        <div class="checkout-alert-title">
                            <h4> سفارش <span
                                    class="checkout-alert-highlighted checkout-alert-highlighted-success">DDC-75007560</span>
                                با موفقیت در سیستم ثبت شد.
                            </h4>
                        </div>
                        <div class="checkout-alert-content">
                            <p class="checkout-alert-content-success">
                                سفارش نهایتا تا یک روز آماده ارسال خواهد شد.
                            </p>
                        </div>
                    </div>
                    <section class="checkout-details dt-sl dt-sn mt-4 pt-2 pb-3 pr-3 pl-3 mb-5 px-res-1">
                        <div class="checkout-details-title">
                            <h4>
                                کد سفارش:
                                <span>
                                        DDC-75007560
                                    </span>
                            </h4>
                            <div class="row">
                                <div class="col-lg-9 col-md-8 col-sm-12">
                                    <div class="checkout-details-title">
                                        <p>
                                            سفارش شما با موفقیت در سیستم ثبت شد و هم اکنون
                                            <span class="text-highlight text-highlight-success">تکمیل شده</span>
                                            است.
                                            جزئیات این سفارش را می‌توانید با کلیک بر روی دکمه
                                            <a href="#" class="border-bottom-dt">پیگیری سفارش</a>
                                            مشاهده نمایید.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-12">
                                    <a href="#"
                                       class="btn-primary-cm bg-secondary btn-with-icon d-block text-center pr-0">
                                        <i class="mdi mdi-shopping"></i>
                                        پیگیری سفارش
                                    </a>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-12 px-res-0">
                                    <div class="checkout-table">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <p>
                                                    نام تحویل گیرنده:
                                                    <span>
                                                            جلال بهرامی راد
                                                        </span></p>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <p>
                                                    شماره تماس :
                                                    <span>
                                                            ۰۹۰۳۰۸۱۳۷۴۲
                                                        </span></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <p>
                                                    تعداد مرسوله :
                                                    <span>
                                                            ۱
                                                        </span></p>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <p>
                                                    مبلغ کل:
                                                    <span>
                                                            ۴,۴۳۹,۰۰۰ تومان
                                                        </span></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <p>
                                                    روش پرداخت:
                                                    <span>
                                                            پرداخت اینترنتی
                                                            <span class="green">
                                                                (موفق)
                                                            </span></span></p>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <p>
                                                    وضعیت سفارش:
                                                    <span>
                                                            پرداخت شد
                                                        </span></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <p>آدرس : استان خراسان شمالی
                                                    ، شهربجنورد، خراسان شمالی-بجنورد</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>

        </div>
    </main>
    <!-- End main-content -->

    <!-- Start mini-footer -->
    <footer class="mini-footer dt-sl">
        <div class="container main-container">
            <div class="row">
                <div class="col-md-6 col-sm-12 text-left">
                    <i class="mdi mdi-phone-outline"></i>
                    شماره تماس : <a href="#">
                        ۶۱۹۳۰۰۰۰
                        - ۰۲۱
                    </a>
                </div>
                <div class="col-md-6 col-sm-12 text-right">
                    <i class="mdi mdi-email-outline"></i>
                    آدرس ایمیل : <a href="#">info@gmail.com</a>
                </div>
                <div class="col-12 text-center mt-2">
                    <p class="text-secondary text-footer">
                        استفاده از کارت هدیه یا کد تخفیف، درصفحه ی پرداخت امکان پذیر است.
                    </p>
                </div>
                <div class="col-12 text-center">
                    <div class="copy-right-mini-footer">
                        Copyright © 2019 Didikala
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End mini-footer -->

</div>


<!-- Core JS Files -->
<script src="{{asset('assets/js/vendor/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/popper.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/bootstrap.min.js')}}"></script>
<!-- Plugins -->
<!-- Main JS File -->
<script src="{{asset('assets/js/vendor/theia-sticky-sidebar.min.js')}}"></script>

<script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>

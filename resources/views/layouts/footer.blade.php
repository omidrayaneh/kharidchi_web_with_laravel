<footer class="main-footer dt-sl">
    <div class="back-to-top">
        <a href="#"><span class="icon"><i class="mdi mdi-chevron-up"></i></span> <span>بازگشت به
                        بالا</span></a>
    </div>
    <div class="container main-container">
        <div class="footer-services">
            <div class="row">
                <div class="service-item col">
                    <a href="#" target="_blank">
                        <img src="{{asset('assets/img/svg/delivery.svg')}}">
                    </a>
                    <p>تحویل اکسپرس</p>
                </div>
                <div class="service-item col">
                    <a href="#" target="_blank">
                        <img src="{{asset('assets/img/svg/contact-us.svg')}}">
                    </a>
                    <p>پشتیبانی 24 ساعته</p>
                </div>
                <div class="service-item col">
                    <a href="#" target="_blank">
                        <img src="{{asset('assets/img/svg/payment-terms.svg')}}">
                    </a>
                    <p>پرداخت درمحل</p>
                </div>
                <div class="service-item col">
                    <a href="#" target="_blank">
                        <img src="{{asset('assets/img/svg/return-policy.svg')}}">
                    </a>
                    <p>هفت روز ضمانت بازگشت</p>
                </div>
                <div class="service-item col">
                    <a href="#" target="_blank">
                        <img src="{{asset('assets/img/svg/origin-guarantee.svg')}}">
                    </a>
                    <p>ضمانت اصل بودن کالا</p>
                </div>
            </div>
        </div>
        <div class="footer-widgets">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="widget-menu widget card">
                        <header class="card-header">
                            <h3 class="card-title">راهنمای خرید از {{__('word.storeName')}}</h3>
                        </header>
                        <ul class="footer-menu">
                            <li>
                                <a href="{{route('register.product')}}">نحوه ثبت سفارش</a>
                            </li>
                            <li>
                                <a href="{{route('send.product')}}">رویه ارسال سفارش</a>
                            </li>
                            <li>
                                <a href="{{route('payment.product')}}">شیوه‌های پرداخت</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="widget-menu widget card">
                        <header class="card-header">
                            <h3 class="card-title">خدمات مشتریان</h3>
                        </header>
                        <ul class="footer-menu">
                            <li>
                                <a href="{{route('questions')}}">پاسخ به پرسش‌های متداول</a>
                            </li>
                            <li>
                                <a href="{{route('returning')}}">رویه‌های بازگرداندن کالا</a>
                            </li>
                            <li>
                                <a href="{{route('teams')}}">شرایط استفاده</a>
                            </li>
                            <li>
                                <a href="{{route('privacy')}}">حریم خصوصی</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="widget-menu widget card">
                        <header class="card-header">
                            <h3 class="card-title">با {{__('word.storeName')}}</h3>
                        </header>
                        <ul class="footer-menu">
{{--                            <li>--}}
{{--                                <a href="#">فروش در {{__('word.storeName')}}</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#">همکاری با سازمان‌ها</a>--}}
{{--                            </li>--}}
                            <li>
                                <a href="{{route('careers')}}">فرصت‌های شغلی</a>
                            </li>
                            <li>
                                <a href="{{route('contact')}}">تماس با {{__('word.storeName')}}</a>
                            </li>
                            <li>
                                <a href="{{route('about')}}">درباره {{__('word.storeName')}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3" dir="rtl">
                    <div class="newsletter">
                        <p>از تخفیف‌ها و جدیدترین‌های فروشگاه باخبر شوید:</p>
                        <form action="">
                            <input type="email" class="form-control"
                                   placeholder="آدرس ایمیل خود را وارد کنید...">
                            <input type="submit" class="btn btn-primary" value="ارسال">
                        </form>
                    </div>
                    <div class="socials">
                        <p>ما را در شبکه های اجتماعی دنبال کنید.</p>
                        <div class="footer-social">
                            <ul class="text-center">
                                <li><a @if(!empty($social) && !empty($social->instagram))href="{{'https://www.instagram.com/'.$social->instagram}} " target="_blank" @endif ><i class="mdi mdi-instagram"></i></a></li>
                                <li><a @if(!empty($social) && !empty($social->telegram))href="{{'https://www.telegram.me/'.$social->telegram}} " target="_blank" @endif ><i class="mdi mdi-telegram"></i></a></li>
                                <li><a @if(!empty($social) && !empty($social->linkedin))href="{{'https://www.linkedin.com/in/'.$social->linkedin}} " target="_blank" @endif ><i class="mdi mdi-linkedin"></i></a></li>
                                <li><a @if(!empty($social) && !empty($social->twitter))href="{{'https://twitter.com/'.$social->twitter}} " target="_blank" @endif ><i class="mdi mdi-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="info">
            <div class="row">
                <div class="col-12 text-right">
                    <span>هفت روز هفته ، 24 ساعت شبانه‌روز پاسخگوی شما هستیم.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="description" dir="rtl">
        <div class="container main-container">
            <div class="row">
                <div class="site-description col-12 col-lg-7">
                    <h1 class="site-title">فروشگاه اینترنتی {{__('word.storeName')}}، بررسی، انتخاب و خرید آنلاین</h1>
                    <p>
                        {{__('word.storeName')}} به عنوان یکی از قدیمی‌ترین فروشگاه های اینترنتی با بیش از یک دهه تجربه، با
                        پایبندی به سه اصل کلیدی، پرداخت در
                        محل، 7 روز ضمانت بازگشت کالا و تضمین اصل‌بودن کالا، موفق شده تا همگام با فروشگاه‌های
                        معتبر جهان، به بزرگ‌ترین فروشگاه
                        اینترنتی ایران تبدیل شود. به محض ورود به {{__('word.storeName')}} با یک سایت پر از کالا رو به رو
                        می‌شوید! هر آنچه که نیاز دارید و به
                        ذهن شما خطور می‌کند در اینجا پیدا خواهید کرد.
                    </p>
                </div>
                <div class="symbol col-12 col-lg-5">
                    <!--<a href="#" target="_blank"><img src="{{asset('assets/img/symbol-01.png')}}" alt=""></a>-->
                    <!--<a href="#" target="_blank"><img src="{{asset('assets/img/symbol-02.png')}}" alt=""></a>-->
                    <img referrerpolicy='origin' id = 'nbqeapfusizpsizpoeuksizp' style = 'cursor:pointer' onclick = 'window.open("https://logo.samandehi.ir/Verify.aspx?id=259989&p=uiwkdshwpfvlpfvlmcsipfvl", "Popup","toolbar=no, scrollbars=no, location=no, statusbar=no, menubar=no, resizable=0, width=450, height=630, top=30")' alt = 'logo-samandehi' src = 'https://logo.samandehi.ir/logo.aspx?id=259989&p=odrfujynbsiybsiyaqgwbsiy' />
                    <a referrerpolicy="origin" target="_blank" href="https://trustseal.enamad.ir/?id=224002&amp;Code=SscCYk8OHwaU4bCHRi1V"><img referrerpolicy="origin" src="https://Trustseal.eNamad.ir/logo.aspx?id=224002&amp;Code=SscCYk8OHwaU4bCHRi1V" alt="" style="cursor:pointer" id="SscCYk8OHwaU4bCHRi1V"></a>

                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container main-container">
            <p>
                استفاده از مطالب فروشگاه اینترنتی {{__('word.storeName')}} فقط برای مقاصد غیرتجاری و با ذکر منبع بلامانع است.
                کلیه حقوق این سایت متعلق
                به گروه {{__('word.storeName')}} (فروشگاه آنلاین {{__('word.storeName')}}) می‌باشد.
            </p>
        </div>
    </div>
</footer>


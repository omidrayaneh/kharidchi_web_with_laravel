<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="UTF-8">
    @yield('title')
    @yield('description')
    @yield('keyword')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#f7858d">
    <meta name="msapplication-navbutton-color" content="#f7858d">
    <meta name="apple-mobile-web-app-status-bar-style" content="#f7858d">
    <meta name="csrf-token" content="{{ csrf_token() }}">


<!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/vendor/bootstrap.min.css')}}">
    <!-- Plugins -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vendor/jquery.horizontalmenu.css')}}">
    @yield('style-search')
    <link rel="stylesheet" href="{{asset('assets/css/vendor/fancybox.min.css')}}">
    <!-- Font Icon -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor/materialdesignicons.min.css')}}">
    <!-- Main CSS File -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">





    @yield('styles')
</head>

<body>
<div class="wrapper" id="app">
    <!-- Start header -->
    <header class="main-header ">
        <!-- Start ads -->
        @if($ads)
            <div class="a-header-wrapper">
                <a href="/search?q={{$ads->detail}}" class="a-header hidden-sm" target="_blank"  style="background-image: url({{asset($ads->path)}}) " ></a>
            </div>
        @endif
        <?php
        if(isset($_GET['q'])){
        $a= $_GET['q']
            ?>
        <?php   } ?>


    <!-- End ads -->
        <!-- Start topbar -->
        <div class="container main-container">
            <div class="topbar dt-sl">
                <div class="row">
                    <div class="col-lg-2 col-md-3 col-6">
                        <div class="logo-area float-right">
                            @if(isset($logo->path))
                                <a href="{{route('/')}}">
                                    <img src="{{asset($logo->path)}}" alt="{{$logo->title}}">
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-5 hidden-sm">
                        <div class="search-area dt-sl">
                            <form method="GET" action="/search" class="search">
{{--                                @if(!empty($a)) value="{{$a}}" @endif--}}
                                <input type="text" name="q"
                                       placeholder="نام کالا، برند و یا دسته مورد نظر خود را جستجو کنید…">
                                <button type="submit"><img src="{{asset('assets/img/theme/search.png')}}" alt="{{$logo->title}}"></button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4 col-6 topbar-left">
                        <ul class="nav float-left">
                            <li class="nav-item account dropdown">
                                @auth
                                    <a class="nav-link" aria-haspopup="false"
                                       aria-expanded="false">
                                        <span class="label-dropdown">{{auth()->user()->name}} {{auth()->user()->family}}</span>
                                        <i class="mdi mdi-account-circle-outline"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-left">
                                        <a class="dropdown-item" href="{{route('profile')}}">
                                            <i class="mdi mdi-account-card-details-outline"></i>پروفایل
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            <span class="float-left badge badge-dark">۴</span>
                                            <i class="mdi mdi-comment-text-outline"></i>پیغام ها
                                        </a>
                                        <a class="dropdown-item" href="{{route('additional-info')}}">
                                            <i class="mdi mdi-account-edit-outline"></i>ویرایش حساب کاربری
                                        </a>
                                        <div class="dropdown-divider" role="presentation"></div>
                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="mdi mdi-logout-variant"></i>خروج
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                @else
                                    <a href="{{ route('login') }}" class="nav-link" aria-haspopup="false"
                                       aria-expanded="false">
                                        <span class="label-dropdown">ورود به حساب کاربری</span>
                                        <i class="mdi mdi-account-circle-outline"></i>
                                    </a>
                                @endauth
                            </li>
                            <ul class="nav float-left ">
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true"
                                       aria-expanded="false">
                                        <span class="label-dropdown ml-4 mr-4 gray">|</span>
                                        <i class="mdi mdi-cart-outline"></i>
                                        @if(Cart::getTotalQuantity()!=0)
                                            <span class="count"> {{Cart::getTotalQuantity()}}</span>
                                        @endif

                                    </a>
                                    <div class="dropdown-menu cart dropdown-menu-sm dropdown-menu-left ">
                                        <div class="dropdown-header">

                                            @if(!Cart::isempty())
                                                <span>سبد خرید</span>
                                                <span class="red"> &nbsp; {{Cart::getTotalQuantity()}}&nbsp; کالا &nbsp; </span>
                                            @else
                                                <span class="mr-lg-5 pink">سبد خرید شما خالی است</span>
                                            @endif

                                        </div>
                                        @foreach(\Cart::getContent() as $item)
                                            <div class="dropdown-list-icons">
                                                <a href="#" class="dropdown-item">
                                                    <div class="dropdown-item-icon">
                                                        <img src="{{$item->attributes->path}}">
                                                    </div>
                                                    <div class="mr-3">
                                                        {{$item->name}}
                                                        @if($item->attributes->size)
                                                        <div class="pt-1"> اندازه :{{$item->attributes->size}} </div>
                                                        @elseif($item->attributes->color)
                                                            <div class="pt-1"> رنگ :{{$item->attributes->attValue}} </div>
                                                        @endif
                                                        <div class="pt-1"> قیمت :{{number_format($item->price)}} تومان</div>
                                                        <div class="pt-1"> تعداد :{{$item->quantity}} عدد</div>
                                                    </div>
                                                    <button class="remove" type="button" onclick="event.preventDefault();
                                                        document.getElementById('remove-cart-item_{{$item->id}}').submit();"><i
                                                            class="mdi mdi-close red"></i></button>
                                                </a>
                                                <form id="remove-cart-item_{{$item->id}}"
                                                      action="{{ route('cart.remove', ['id' => $item->id]) }}" method="post"
                                                      style="display: none;">
                                                    @csrf
                                                </form>
                                            </div>
                                        @endforeach
                                        @if(!Cart::isempty())
                                            <hr>
                                            <div class="dropdown-footer text-center">
                                                <div class="dt-sl mb-3">
                                                    <span class="float-right">جمع :</span>
                                                    <span class="float-left">{{\Cart::gettotal()}} تومان</span>
                                                </div>
                                                <a href="{{route('cart.show')}}" class="btn btn-primary-cm">مشاهده سبد خرید</a>
                                                <a href="{{url('shopping-payment')}}" class="btn btn-primary">ثبت سفارش</a>
                                            </div>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End topbar -->

        <!-- Start bottom-header -->

        <div class="bottom-header dt-sl mb-sm-bottom-header">
            <div class="container main-container">
                <!-- Start Main-Menu -->
                <nav class="main-menu d-flex justify-content-md-between justify-content-end dt-sl">
                    <ul class="list hidden-sm">
                        <li class="list-item category-list">
                            <a href="{{route('get.productList',['slug' => $categories[0]->slug,'sku'=>$categories[0]->sku])}}"><i class=" ml-1"></i>{{$categories[0]->title}}</a>
                            <ul>
                                @foreach( $categories[0]->childrenRecursive as $category   )
                                <li>
                                    <a href="{{route('get.productList',['slug' => $category->slug,'sku'=>$category->sku])}}">{{$category->title}}</a>
                                    <ul class="row">
                                        @foreach($category->childrenRecursive as $cat)
                                            <li class="sublist--title"><a href="{{route('get.productList',['slug' => $cat->slug,'sku'=>$cat->sku])}}">{{$cat->title}}</a></li>
                                            @if($cat->childrenRecursive )
                                                    @foreach( $cat->childrenRecursive as $subCat   )
                                                        <li class="sublist--item"><a href="{{route('get.productList',['slug' => $subCat->slug,'sku'=>$subCat->sku])}}">{{$subCat->title}}</a></li>
                                                    @endforeach
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        @foreach($categories as $category)
                            @if($category->id == $categories[0]->id)
                                @continue
                            @endif
                            @if(!$category->parent_id && count($category->childrenRecursive))
                        <li class="list-item list-item-has-children mega-menu mega-menu-col-4">
                                <a class="nav-link" href="{{route('get.productList',['slug' => $category->slug,'sku'=>$category->sku])}}">{{$category->title}}</a>
                            <ul class="sub-menu nav">
                                @foreach($category->childrenRecursive as $cat)
                                    @if($cat)
                                <li class="list-item list-item-has-children">

                                    <a class="nav-link" href="{{route('get.productList',['slug' => $cat->slug,'sku'=>$cat->sku])}}">{{$cat->title}}</a>
                                    <ul class="sub-menu nav">
                                        @foreach($cat->childrenRecursive as $subCat)
                                            <li class="list-item">
                                                <a class="nav-link" href="{{route('get.productList',['slug' => $subCat->slug,'sku'=>$subCat->sku])}}">{{$subCat->title}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                        @endif
                       @endforeach
                        <!-- mega menu 4 column -->
                        <!-- dropdown-menu -->
                    </ul>
                    <button class="btn-menu">
                        <div class="align align__justify">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </button>
                    <div class="side-menu">
                        <div class="logo-nav-res dt-sl text-center">
                            <a href="{{route('/')}}">
                                <img src="{{asset($logo->path)}}" alt="">
                            </a>
                        </div>
{{--                        <div class="search-box-side-menu dt-sl text-center mt-2 mb-3">--}}
{{--                            <form action="">--}}
{{--                                <input type="text" name="s" placeholder="جستجو کنید...">--}}
{{--                                <i class="mdi mdi-magnify"></i>--}}
{{--                            </form>--}}
{{--                        </div>--}}
                        <ul class="navbar-nav dt-sl">
                            @foreach($categories as $category)
                                @if(!$category->parent_id && count($category->childrenRecursive))
                                    <li @if(count($category->childrenRecursive))class="sub-menu" @endif>
                                    <a href="{{route('get.productList',['slug' => $category->slug,'sku'=>$category->sku])}}">{{$category->title}}</a>
                                        <ul>
                                            @foreach($category->childrenRecursive as $cat)
                                                    <li  @if(count($cat->childrenRecursive))class="sub-menu" @endif>
                                                          <a href="{{route('get.productList',['slug' => $cat->slug,'sku'=>$cat->sku])}}">{{$cat->title}}</a>

                                                        <ul>
                                                            @foreach($cat->childrenRecursive as $subCat)
                                                                <li>
                                                                    <a href="{{route('get.productList',['slug' => $subCat->slug,'sku'=>$subCat->sku])}}">{{$subCat->title}}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                            @endforeach
                                        </ul>
                                   </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <div class="overlay-side-menu">
                    </div>
                </nav>
                <!-- End Main-Menu -->
            </div>
        </div>

        <!-- End bottom-header -->
    </header>
    <!-- End header -->

    <!-- Start main-content -->
     @yield('content')
    <!-- End main-content -->

    <!-- Start footer -->
      @include('layouts.footer')
    <!-- End footer -->

</div>


<!-- Core JS Files -->

<script src="{{asset('assets/js/vendor/jquery-3.4.1.min.js')}}"></script>@yield('vue')
@yield('vue')
<script src="{{asset('js/app.js')}}"></script>

<script src="{{asset('assets/js/vendor/popper.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/bootstrap.min.js')}}"></script>
<!-- Plugins -->
<script src="{{asset('assets/js/vendor/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/jquery.horizontalmenu.js')}}"></script>
<script src="{{asset('assets/js/vendor/wNumb.js')}}"></script>
<script scr="{{asset('assets/js/vendor/jquery.fancybox.min.js')}}" ></script>
<script scr="{{asset('assets/js/vendor/countdown.min.js')}}" ></script>
<!-- Main JS File -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

@yield('scripts')
<script src="{{asset('assets/js/main.js')}}"></script>

@include('sweetalert::alert')



</body>

</html>

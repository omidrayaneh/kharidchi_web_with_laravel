<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#f7858d">
    <meta name="msapplication-navbutton-color" content="#f7858d">
    <meta name="apple-mobile-web-app-status-bar-style" content="#f7858d">
   @yield('title')
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./assets/css/vendor/bootstrap.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="./assets/css/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="./assets/css/vendor/jquery.horizontalmenu.css">
    <link rel="stylesheet" href="./assets/css/vendor/nice-select.css">
    <link rel="stylesheet" href="./assets/css/vendor/nouislider.min.css">
    <!-- Font Icon -->
    <link rel="stylesheet" href="./assets/css/vendor/materialdesignicons.min.css">
    <!-- Main CSS File -->
    <link rel="stylesheet" href="./assets/css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.css">

</head>

<body>

@yield('content')

<!-- Core JS Files -->
<script src="./assets/js/vendor/jquery-3.4.1.min.js"></script>
<script src="./assets/js/vendor/popper.min.js"></script>
<script src="./assets/js/vendor/bootstrap.min.js"></script>
<!-- Plugins -->
<script src="./assets/js/vendor/owl.carousel.min.js"></script>
<script src="./assets/js/vendor/jquery.horizontalmenu.js"></script>
<script src="./assets/js/vendor/jquery.nice-select.min.js"></script>
<script src="./assets/js/vendor/nouislider.min.js"></script>
<script src="./assets/js/vendor/wNumb.js"></script>
<script src="./assets/js/vendor/ResizeSensor.min.js"></script>
<script src="./assets/js/vendor/theia-sticky-sidebar.min.js"></script>
<!-- google map js -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAq7MrCR1A2qIShmjbtLHSKjcEIEBEEwM"></script>
<!-- Main JS File -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
<script src="{{asset('https://cdn.jsdelivr.net/npm/sweetalert2@9')}}"></script>
@yield('scripts')
@include('sweetalert::alert')

</body>

</html>

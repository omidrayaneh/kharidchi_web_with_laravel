<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * use slug instead of id
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public static function generateSKU()
    {
        $number = 'MY-'.mt_rand(1000, 99999);
        if (Category::checkSKU($number)) {
            return Category::generateSKU();
        }
        return (string)$number;
    }
    public static function generate4CategorySKU()
    {
        $number = mt_rand(1000, 99999);
        if (Category::checkSKU($number)) {
            return Category::generateSKU();
        }
        return (string)$number;
    }
    public static function checkSKU($number)
    {
        return Category::where('sku',$number)->exists();
    }

    public static function makeSlug($string)
    {
        return preg_replace('/\s+/u', '-', trim($string));
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    public function father()
    {
        return $this->belongsTo(Category::class,'parent_id');

    }

    public function fatherRecursive()
    {
        return $this->father()->with('fatherRecursive');
    }
    public function attributeGroups()
    {
       return $this->belongsToMany(AttributeGroup::class,'attributeGroup_category','category_id','attributeGroup_id');
    }

    public function products()
    {

        return $this->belongsToMany(Product::class);
    }
}

<?php

namespace App\Listeners;

use App\Events\OrderEvent;
use App\Notifications\InvoicePaid;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class OrderEventListener
{

    public function __construct()
    {

    }

    public function handle(OrderEvent $event)
    {
        $order = $event->order;
        $notification = new InvoicePaid($order);
        $admins = User::where('role' ,'admin')->get();
        foreach ($admins as $admin){
            $admin->notify($notification);
        }
    }
}

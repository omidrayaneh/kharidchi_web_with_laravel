<?php


namespace App\Repository;


interface CommentRepositoryInterface
{
    public function allWithPaginate($page);

    public function all();

    public function create($comment,$sku);

    public function delete($slug);

    public function update($id,$request);

    public function findById($id);


}

<?php


namespace App\Repository;


interface TextRepositoryInterface
{

    public function all();

    public function where($value);

    public function allWithPaginate($page);

    public function create($user);

    public function delete($id);

    public function update($id,$request);

    public function findById($id);

    public function validator($request,$id);

}

<?php


namespace App\Repository;


interface CouponRepositoryInterface
{

    public function all();

    public function allWithPaginate($page);

    public function create($request);

    public function delete($slug);

    public function update($slug,$request);


    public function findBySlug($slug);
    public function findByCode($code);

    public function findById($id);

    public function validator($request,$coupon);

}

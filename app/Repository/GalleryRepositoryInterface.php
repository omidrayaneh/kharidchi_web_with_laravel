<?php


namespace App\Repository;


interface GalleryRepositoryInterface
{
    public function all();

    public function allWithPaginate($page);

    public function create($request);

    public function delete($id);

    public function update($id,$request);


    public function findById($id);

    public function validator($request,$id);
}

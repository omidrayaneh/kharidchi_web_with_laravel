<?php


namespace App\Repository;


interface BrandRepositoryInterface
{
    public function allWithPhotoAndPaginate($page);


    public function all();

    public function create($category);

    public function delete($slug);

    public function update($slug,$request);

    public function findBySlug($slug);

    public function findBySlugWithPhoto($slug);

    public function findById($id);

    public function validator($request,$category);

}

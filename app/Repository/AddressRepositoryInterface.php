<?php


namespace App\Repository;


interface AddressRepositoryInterface
{

    public function all();

    public function allWithPaginate($page);

    public function create($user);

    public function delete($id);

    public function update($id,$request);

    public function enable($id);

    public function findById($id);

    public function userAddresses($id);

    public function validator($request,$id);

}

<?php


namespace App\Repository;


interface AttValueRepositoryInterface
{
    public function allByPaginate($page);

    public function AllWithByPaginate($page);

    public function AllWithByPaginateWithOrder($page);

    public function all();

    public function create($category);

    public function delete($slug);

    public function update($slug,$request);

    public function findBySlug($slug);

    public function findById($id);
    public function validator($request,$category);

    public function findByIdWithAllRelation($id);

    public function showProductAttribute($page);

    public function productAttribute($sku);
}

<?php


namespace App\Repository;


interface CategoryRepositoryInterface
{
    public function allByPaginate($page);

    public function all();

    public function create($category);

    public function delete($slug);

    public function update($slug,$request);

    public function findBySlug($slug);

    public function findById($id);
    public function validator($request,$category);

    public function saveSetting($request,$slug);

}

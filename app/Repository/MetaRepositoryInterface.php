<?php


namespace App\Repository;


interface MetaRepositoryInterface
{
    public function allPaginate($page);


    public function all();

    public function create($category);

    public function delete($id);

    public function update($id,$request);

    public function findById($id);

    public function validator($request,$meta);

}

<?php


namespace App\Repository\Eloquent;


use App\Text;
use App\Repository\TextRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class TextRepository implements TextRepositoryInterface
{

    public function all()
    {
        return Text::all();
    }

    public function where($value)
    {
        if ($value == 'question')
            return Text::where([['title', $value], ['status', 1]])->get();
        else
            return Text::where([['title', $value], ['status', 1]])->latest()->first();


    }


    public function allWithPaginate($page)
    {
        return Text::paginate($page);
    }

    public function create($request)
    {

        $Text = new Text();
        $Text->title = $request->get('title');
        $Text->text = $request->get('text');
        $Text->question = $request->get('question');
        if ($request->get('status') == 'on')
            $Text->status = 1;
        else
            $Text->status = 0;

        $Text->user_id = auth()->id();
        $Text->save();

        toast('با موفقیت ایجاد  شد', 'success');
    }

    public function delete($slug)
    {
        // TODO: Implement delete() method.
    }

    public function update($id, $request)
    {
        $Text = $this->findById($id);
        $Text->title = $request->get('title');
        $Text->text = $request->get('text');
        $Text->question = $request->get('question');
        if ($request->get('status') == 'on')
            $Text->status = 1;
        else
            $Text->status = 0;

        $Text->save();


        toast('با موفقیت بروز  شد', 'success');

    }

    public function findById($id)
    {
        return Text::findOrFail($id);
    }

    public function validator($request, $id)
    {
        $Text = $this->findById($id);
        return $validator = Validator::make($request->all(), [
            'title' => 'required',
            'text' => 'required',
        ], [
            'title.required' => 'عنوان متن  خالیست',
            'text.required' => 'جزئیات متن خالیست',
        ]);
    }


}

<?php


namespace App\Repository\Eloquent;


use App\AttributeValue;
use App\Attvalue;
use App\Category;
use App\Product;
use App\Repository\ProductRepositoryInterface;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class ProductRepository implements ProductRepositoryInterface
{

    protected $product;
    /**
     * @var AttvalueRepository
     */

    public function __construct(Product $product)
    {
        $this->product = $product;
    }


    public function findBySlug($slug)
    {
        return Product::where('slug', $slug)->first();
    }

    public function findBySku($sku)
    {
        return Product::where('sku', $sku)->first();
    }

    public function findById($id)
    {
        return Product::findOrFail($id);
    }

    public function allWithPhotoAndPaginate($page)
    {
        return Product::with('photos')->latest()->paginate($page);
    }

    public function allWithPhoto()
    {
        return Product::with('photos', 'categories')->where('status', 1)->get();
    }

    public function findBySkuWithAllRelations($sku)
    {
        return Product::with('photos', 'att_values', 'brand', 'categories','values')->where('sku', $sku)->first();
    }

    public function all()
    {
        return Product::all();

    }

    public function create($request)
    {
        $product = new Product();
        $product->title = $request->title;
        $product->sku = Category::generateSKU();
        $product->slug = Category::makeSlug($request->title);
        $product->price = $request->price;
        $product->discount = $request->discount;
        $product->qty = $request->qty;
        $product->brand_id = $request->brand;
        if ($request->input('app') == 'on') {
            $product->app = 1;
        } else {
            $product->app = 0;
        }
        if ($request->input('status') == 'on') {
            $product->status = 1;
        } else {
            $product->status = 0;
        }
        $product->description = $request->description;
      //  $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;
         $product->meta_keywords = $request->meta_keywords;
        $product->user_id = auth()->user()->id;
        $product->save();



     //   $attColorCount = $request->input('color_count');
      //  $attSizeCount = $request->input('size_count');

        $attributes = $request->input('attributes');
        $product->att_values()->sync($attributes);

        $photos = explode(',', $request->input('photo_id')[0]);
        $product->photos()->sync($photos);

        $categories = explode(',', $request->input('categories')[0]);
        $product->categories()->sync($categories);

        $admins = User::where('role', 'admin')->get();
        foreach ($admins as $admin) {
            $admin->lastProduct()->attach($product->id, ['type' => 1]);
        }

        if (!empty($attributes)){
            foreach ($attributes as  $attribute){
                $attval = AttributeValue::findOrFail($attribute);
                if ($attval->size){
                    $tag_name = 'size_count_'.$attval->id;
                    $attSizeCount = $request->input($tag_name);
                    $value = new Attvalue();
                    $value->sizeCount = $attSizeCount;
                    $value->attributeValue_id = $attval->id;
                    $value->product_id = $product->id;
                    $value->save();
                }
                else if ($attval->color){
                    $tag_name = 'color_count_'.$attval->id;
                    $attColorCount = $request->input($tag_name);
                    $value = new Attvalue();
                    $value->colorCount = $attColorCount;
                    $value->attributeValue_id = $attval->id;
                    $value->product_id = $product->id;
                    $value->save();
                }

            }
        }


        toast('محصول با موفقیت ایجاد  شد', 'success');

    }

    function tags_to_array($string)
    {
        $array = explode(',', $string);
        $result = array();
        foreach ($array as $tag) {
            $result[] = trim($tag);
        }
        return $result;
    }

    public function update($sku, $request)
    {

        $updatedProduct = $this->findBySkuWithAllRelations($sku);
        $updatedProduct->title = $request->title;
        $updatedProduct->slug = Category::makeSlug($request->title);
        $updatedProduct->price = $request->price;
        $updatedProduct->discount = $request->discount;
        $updatedProduct->qty = $request->qty;
        $updatedProduct->brand_id = $request->brand;
        if ($request->input('app') == 'on') {
            $updatedProduct->app = 1;
        } else {
            $updatedProduct->app = 0;
        }
        if ($request->input('status') == 'on') {
            $updatedProduct->status = 1;
        } else {
            $updatedProduct->status = 0;
        }
        $updatedProduct->description = $request->description;
      //  $updatedProduct->meta_title = $request->meta_title;
        $updatedProduct->meta_description = $request->meta_description;
        $updatedProduct->meta_keywords = $request->meta_keywords;
        $updatedProduct->user_id = auth()->user()->id;
        $updatedProduct->save();
      //  $attColorCount = $request->input('color_count');
      //  $attSizeCount = $request->input('size_count');
        $attributes = $request->input('attributes');
        $updatedProduct->att_values()->sync($attributes);

        $photos = explode(',', $request->input('photo_id')[0]);
        $updatedProduct->photos()->sync($photos);

        $categories = explode(',', $request->input('categories')[0]);
        $updatedProduct->categories()->sync($categories);

        if (!empty($attributes)){
            $value =Attvalue::where('product_id',$updatedProduct->id);
            $value->delete();
            foreach ($attributes as  $attribute){
                $attval = AttributeValue::findOrFail($attribute);
                if ($attval->size){


                    $tag_name = 'size_count_'.$attval->id;
                    $attSizeCount = $request->input($tag_name);

                    $value = new Attvalue();
                    $value->sizeCount = $attSizeCount;
                    $value->attributeValue_id = $attval->id;
                    $value->product_id = $updatedProduct->id;
                    $value->save();
                }
                else if ($attval->color){


                    $tag_name = 'color_count_'.$attval->id;
                    $attColorCount = $request->input($tag_name);

                    $value = new Attvalue();
                    $value->colorCount = $attColorCount;
                    $value->attributeValue_id = $attval->id;
                    $value->product_id = $updatedProduct->id;
                    $value->save();
                }

            }
        }

        toast('محصول با موفقیت بروز  شد', 'success');
        return redirect('admin/products' );

    }

    public function delete($slug)
    {

    }

    public function validator($request, $sku)
    {
        $product = $this->findBySku($sku);
        return $validator = Validator::make($request->all(), [
            'title' => 'required|min:3|unique:products,title,' . $product->id,
            'price' => 'required',
            'qty' => 'required',
            'discount' => 'required',
            'category_id.*' => 'required',
            'description' => 'required|min:15',
            'photo_id.*' => 'required',
            //  'attributes.*'=> 'present|array',
            'attributes.*' => 'required',
        ], [
            'title.required' => 'نام محصول را وارد کنید',
            'title.min' => 'نام محصول کمتر از سه کاراکتر می باشد',
            'price.required' => 'قیمت محصول را وارد کنید',
            'qty.required' => 'تعداد محصول را وارد کنید',
            'discount.required' => 'تخفیف محصول را وارد کنید',
            'description.required' => 'توضیحات محصول را وارد کنید',
            'description.min' => 'توضیحات محصول کمتر از 15 کاراکتر می باشد',
            'category_id.*' => 'حداقل یک دسته بندی را انتخاب کنید',
            'photo_id.*' => 'تصویر محصول را وارد کنید',
            'attributes.*' => 'ویژگی را انتخاب کنید',

        ]);

    }


}

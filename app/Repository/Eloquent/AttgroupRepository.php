<?php


namespace App\Repository\Eloquent;


use App\AttributeGroup;
use App\Category;
use App\Repository\AttGroupRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class AttgroupRepository implements AttGroupRepositoryInterface
{

    protected $attribute;

    public function __construct(AttributeGroup $attribute)
    {
        $this->attribute = $attribute;
    }
    public function findByIdWithAllRelation()
    {
        return AttributeGroup::with('attValues')->where('status',1)->get();
    }

    public function findBySlug($slug)
    {
        return AttributeGroup::where('slug',$slug)->first();

    }

    public function findById($id)
    {
        return AttributeGroup::findOrFail($id);
    }

    public function allByPaginate($page)
    {
       return AttributeGroup::paginate($page);
    }

    public function all()
    {
        return AttributeGroup::all();

    }

    public function create($request)
    {
        $newattribute = new AttributeGroup();
        $newattribute->sku = Category::generateSKU();
        $newattribute->slug = Category::makeSlug($request->input('title'));
        $newattribute->title = $request->input('title');
        if ($request->input('color') == 'on') {
            $newattribute->color = 1;
        } else {
            $newattribute->color = 0;
        }
        if ($request->input('size') == 'on') {
            $newattribute->size = 1;
        } else {
            $newattribute->size = 0;
        }
        if ($request->input('status') == 'on') {
            $newattribute->status = 1;
        } else {
            $newattribute->status = 0;
        }
        if ($request->input('multiple') == 'on') {
            $newattribute->multiple = 1;
        } else {
            $newattribute->multiple = 0;
        }
        $newattribute->save();
        toast('با موفقیت ایجاد  شد', 'success');
    }



    public function update($slug, $request)
    {
        $attribute=AttributeGroup::where('slug',$slug)->first();

        $attribute->slug = Category::makeSlug($request->input('title'));
        $attribute->title = $request->input('title');
        if ($request->input('status') == 'on') {
            $attribute->status = 1;
        } else {
            $attribute->status = 0;
        }
        if ($request->input('color') == 'on') {
            $attribute->color = 1;
        } else {
            $attribute->color = 0;
        }
        if ($request->input('size') == 'on') {
            $attribute->size = 1;
        } else {
            $attribute->size = 0;
        }
        if ($request->input('multiple') == 'on') {
            $attribute->multiple = 1;
        } else {
            $attribute->multiple = 0;
        }
        $attribute->save();
        toast('با موفقیت بروز  شد', 'success');

    }

    public function delete($slug)
    {

    }

    public function validator($request, $slug)
    {
        $attribute = $this->findBySlug($slug);

        return $validator = Validator::make($request->all(), [
            'title' => 'required|min:2|unique:attribute_groups,title,' . $attribute->id,
        ], [
            'title.required' => 'عنوان ویژگی  خالیست',
            'title.min' => 'مقدار ویژگی کمتر از دو کاراکتر است',
            'title.unique' => 'این ویژگی قبلا ثبت شده است']);

    }
}

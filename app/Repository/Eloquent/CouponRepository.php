<?php


namespace App\Repository\Eloquent;


use App\Category;
use App\Coupon;
use App\Repository\BrandRepositoryInterface;
use App\Repository\CouponRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class CouponRepository implements CouponRepositoryInterface
{

    protected $coupon;

    public function __construct(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }


    public function all()
    {
        return Coupon::all();
    }

    public function allWithPaginate($page)
    {
        return Coupon::paginate($page);
    }

    public function create($request)
    {
        $newCoupon = new Coupon();
        $newCoupon->title = $request->input('title');
        $newCoupon->slug = Category::makeSlug($request->input('title'));
        $newCoupon->sku = Category::generateSKU();
        $newCoupon->code = $request->input('code');
        $newCoupon->price = $request->input('price');
        if ($request->input('status') == 'on') {
            $newCoupon->status = 1;
        }
        $newCoupon->save();
        toast('کوپن با موفقیت ایجاد  شد', 'success');
    }

    public function delete($slug)
    {

    }

    public function update($slug, $request)
    {
        $Coupon =$this->findBySlug($slug);
        $Coupon->title = $request->input('title');
        $Coupon->slug = Category::makeSlug($request->input('title'));
        $Coupon->code = $request->input('code');
        $Coupon->price = $request->input('price');
        if ($request->input('status') == 'on') {
            $Coupon->status = 1;
        }else{
            $Coupon->status = 0;
        }
        $Coupon->save();
        toast('کوپن با موفقیت ویرایش  شد', 'success');
    }

    public function findBySlug($slug)
    {
        return Coupon::where('slug',$slug)->where('status',1)->first();
    }
    public function findByCode($code)
    {
        return Coupon::where('code',$code)->first();
    }
    public function findById($id)
    {
        return Coupon::findOfFail($id);
    }

    public function validator($request, $slug)
    {
        $coupon= $this->findBySlug($slug);
        return $validator = Validator::make($request->all(), [
            'title' => 'required|min:3|unique:coupons,title,' . $coupon->id,
            'code' => 'required|min:2',
            'price' => 'required',
        ], [
            'title.required'=>'نام کوپن را وارد کنید',
            'title.unique'=>'نام کوپن قبلا ثبت شده است',
            'title.min'=>'نام کوپن کمتر از سه کاراکتر می باشد',
            'code.min'=>'کد کوپن کمتر از دو کاراکتر می باشد',
            'code.required'=>'کد کوپن را وارد کنید',
            'price.required'=>'قیمت کوپن را وارد کنید',
        ]);

    }


}

<?php


namespace App\Repository\Eloquent;


use App\Category;
use App\Http\Requests\StoreAttCat;
use App\Repository\CategoryRepositoryInterface;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Validator;

class CategoryRepository implements CategoryRepositoryInterface
{

    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function findBySlugWithAttGroup($slug)
    {
        return Category::with('attributeGroups')->where('slug', $slug)->get();
    }

    public function findBySlug($slug)
    {
        return Category::where('slug', $slug)->first();
    }

    public function findById($id)
    {
        return Category::where('slug', $id)->first();
    }

    public function allByPaginate($page)
    {
        return $categories = Category::with(['childrenRecursive', 'user'])
            ->where('parent_id', null)
            ->paginate($page);
    }

    public function all()
    {
        return Category::with('childrenRecursive')
            ->where('parent_id', null)
            ->get();
    }

    public function create($request)
    {
        $newcategory = new Category();
        $newcategory->sku = Category::generate4CategorySKU();
        $newcategory->slug = Category::makeSlug($request->input('title'));
        $newcategory->title = $request->input('title');
        $newcategory->meta_title = $request->input('meta_title');
        $newcategory->meta_description = $request->input('meta_description');
        $newcategory->meta_keywords = $request->input('meta_keywords');
        $newcategory->parent_id = $request->input('parent_id');
        if ($request->input('status') == 'on') {
            $newcategory->status = 1;
        }
        if ($request->input('app') == 'on') {
            $newcategory->ended = 1;
        }
        $newcategory->user_id = auth()->user()->id;
        $newcategory->save();
        toast('با موفقیت ایجاد  شد', 'success');
    }



    public function update($slug, $request)
    {
        $updateCategory = $this->findBySlug($slug);

        $updateCategory->slug = Category::makeSlug($request->title);
        $updateCategory->title = $request->title;
        $updateCategory->meta_title = $request->meta_title;
        $updateCategory->meta_description = $request->meta_description;
        $updateCategory->meta_keywords = $request->meta_keywords;
        $updateCategory->parent_id = $request->parent_id;
        if ($request->status == 'on') {
            $updateCategory->status = 1;
        } else {
            $updateCategory->status = 0;
        }
        if ($request->app == 'on') {
            $updateCategory->ended = 1;
        } else {
            $updateCategory->ended = 0;
        }
        $updateCategory->user_id = auth()->user()->id;
        $updateCategory->save();

        toast('با بروز ایجاد  شد', 'success');

    }

    public function delete($slug)
    {
        $category = $this->findBySlug($slug);
        return $category->delete();
    }

    public function saveSetting($request,$slug)
    {
        $category= $this->findBySlug($slug);
        $category->attributeGroups()->sync($request->attributes_id);
        $category->save();
        toast('با موفقیت بروز  شد', 'success');

    }
    public function validator($request, $slug)
    {
        $category = $this->findBySlug($slug);

        return $validator = Validator::make($request->all(), [
            'title' => 'required|min:3|unique:categories,title,' . $category->id,
        ], [
            'title.required' => 'عنوان گروه  خالیست',
            'title.min' => 'مقدار گروه کمتر از سه کاراکتر است',
            'title.unique' => 'این گروه قبلا ثبت شده است']);

    }
}

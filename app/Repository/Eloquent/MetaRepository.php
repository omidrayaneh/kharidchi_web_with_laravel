<?php


namespace App\Repository\Eloquent;


use App\Meta;
use App\Repository\MetaRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class MetaRepository implements MetaRepositoryInterface
{

    protected $meta;

    public function __construct(Meta $meta)
    {
        $this->Meta = $meta;
    }


    public function findById($id)
    {
        return Meta::findOrFail($id);
    }

    public function all()
    {
        return Meta::all();

    }

    public function create($request)
    {
        $newMeta= new Meta;
        $newMeta->title = $request->get('title');
        $newMeta->description = $request->get('description');
        $newMeta->keyword = $request->get('keyword');
        if ($request->status == 'on')
            $newMeta->status =1;
        else
            $newMeta->status =0;

        $newMeta->save();

        toast('با موفقیت ایجاد  شد', 'success');
    }



    public function update($id, $request)
    {

        $meta=$this->findById($id);

        $meta->title = $request->input('title');
        $meta->description = $request->input('description');
        $meta->keyword = $request->input('keyword');
        if ($request->status == 'on')
            $meta->status =1;
        else
            $meta->status =0;
        $meta->save();

        toast('با موفقیت بروز  شد', 'success');

    }

    public function delete($id)
    {

    }

    public function validator($request, $id)
    {
        $meta = $this->findById($id);
        return $validator = Validator::make($request->all(), [
            'title' => 'required|min:3|unique:metas,title,' . $meta->id,
            'description' => 'required',
            'keyword' => 'required'
        ], [
            'title.required' => 'عنوان متا  خالیست',
            'title.min' => 'مقدار متا کمتر از سه کاراکتر است',
            'title.unique' => 'این متا قبلا ثبت شده است',
            'description.required' => 'توضیحات متا را وارد کنید',
            'keyword.required' => 'کلمات کلیدی متا را وارد کنید',
        ]);

    }


    public function allPaginate($page)
    {
        return Meta::paginate($page);
    }
}

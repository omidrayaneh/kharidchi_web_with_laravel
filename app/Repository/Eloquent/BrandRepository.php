<?php


namespace App\Repository\Eloquent;


use App\Brand;
use App\Category;
use App\Repository\BrandRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class BrandRepository implements BrandRepositoryInterface
{

    protected $brand;

    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }


    public function findBySlug($slug)
    {
        return Brand::where('slug',$slug)->first();

    }

    public function findBySlugWithPhoto($slug)
    {
        return Brand::with('photos')->where('slug',$slug)->first();
    }

    public function findById($id)
    {
        return Brand::findOrFail($id);
    }



    public function allWithPhotoAndPaginate($page)
    {
       return Brand::with('photos')->paginate(7);
    }

    public function all()
    {
        return Brand::all();

    }

    public function create($request)
    {
        $newbrand= new Brand;
        $newbrand->sku = Category::generateSKU();
        $newbrand->slug = Category::makeSlug($request->get('title'));
        $newbrand->title = $request->get('title');
        $newbrand->description = $request->get('description');
        $newbrand->save();

        $photos = explode(',', $request->input('photo_id')[0]);
        $newbrand->photos()->sync($photos);

        toast('با موفقیت ایجاد  شد', 'success');
    }



    public function update($slug, $request)
    {

        $brand=$this->findBySlug($slug);

        $brand->slug = Category::makeSlug($request->input('title'));
        $brand->title = $request->input('title');
        $brand->description = $request->input('description');

        $brand->save();

        $photos = explode(',', $request->input('photo_id')[0]);
        $brand->photos()->sync($photos);

        toast('با موفقیت بروز  شد', 'success');

    }

    public function delete($slug)
    {

    }

    public function validator($request, $slug)
    {
        $brand = $this->findBySlug($slug);
        return $validator = Validator::make($request->all(), [
            'title' => 'required|min:3|unique:brands,title,' . $brand->id,
            'description' => 'required|min:3',
            'photo_id.*' => 'required'
        ], [
            'title.required' => 'عنوان برند  خالیست',
            'title.min' => 'مقدار برند کمتر از دو کاراکتر است',
            'title.unique' => 'این برند قبلا ثبت شده است',
            'description.required' => 'توضیحات برند را وارد کنید',
            'description.min' => 'توضیحات برند گمتر از 15 کاراکتر می باشد',
            'photo_id.*' => 'تصویر برند را انتخاب کنید'
        ]);

    }


}

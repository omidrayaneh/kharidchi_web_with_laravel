<?php


namespace App\Repository\Eloquent;


use App\Brand;
use App\Category;
use App\Comment;
use App\Product;
use App\Repository\BrandRepositoryInterface;
use App\Repository\CommentRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class CommentRepository implements CommentRepositoryInterface
{

    protected $comments;

    public function __construct(Comment $comments)
    {
        $this->comments = $comments;
    }


    public function findById($id)
    {
        return Comment::findOrFail($id);
    }

    public function allWithPaginate($page)
    {
       return Comment::with('users','user')->paginate(7);
    }

    public function all()
    {
        return Comment::all();

    }


    public function create($request,$sku)
    {
        $product=Product::where('sku',$sku)->first();
        $comments=$product->comments->where('written_by',auth()->id());
        if (count($comments)>=1){
            alert('خطا', 'شما قبلا نظر داده اید','error');
            return back();
        }
        $comment=new Comment();
         $comment->title=$request->title;
         $comment->body=$request->body;
         $comment->status=0;
         $comment->star=$request->stars;
         $comment->advantages=collect($request->comment['advantages'])->implode(' ');
         $comment->disadvantages=collect($request->comment['disadvantages'])->implode(' ');
         if ($request->customRadio == 1)
             $comment->recommended=1;
        elseif ($request->customRadio == 2)
            $comment->recommended=2;
        elseif ($request->customRadio == 3)
            $comment->recommended=3;

        $comment->written_by=auth()->user()->id;

         $product->comments()->save($comment);

        toast('نظر شما با موفقیت ثبت شد', 'success');
    }



    public function update($id, $request)
    {
        toast('با موفقیت بروز  شد', 'success');
    }

    public function delete($slug)
    {

    }



}

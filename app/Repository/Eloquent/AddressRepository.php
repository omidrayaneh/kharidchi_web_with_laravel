<?php


namespace App\Repository\Eloquent;


use App\Address;
use App\Repository\AddressRepositoryInterface;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AddressRepository implements AddressRepositoryInterface
{

    protected $addresses;

    public function __construct(Address $addresses)
    {
        $this->addresses = $addresses;
    }
    public function userAddresses($id)
    {
        return Address::where('user_id',$id)->orderBy('status','desc')->get();
    }
    public function allWithPaginate($page)
    {
        return Address::paginate($page);
    }

    public function findById($id)
    {
        return Address::findOrFail($id);
    }


    public function all()
    {
        return Address::all();

    }

    public function create($request)
    {
        DB::table('addresses')->where([['status', 1], ['user_id', auth()->id()]])->update(['status' => 0]);

        $newAddress= new Address;
        $newAddress->name = $request->get('name');
        $newAddress->phone = $request->get('mobile');
        $newAddress->province_id = $request->get('province_id');
        $newAddress->city_id = $request->get('city_id');
        $newAddress->address = $request->get('address');
        $newAddress->plaque = $request->get('plaque');
        $newAddress->post_code = $request->get('postal_code');
        $newAddress->status = 1;
        $newAddress->user_id = auth()->id();

        $newAddress->save();

        toast('آدرس  با موفقیت ایجاد  شد', 'success');
    }



    public function update($id, $request)
    {
        DB::table('addresses')->where([['status', 1], ['user_id', auth()->id()]])->update(['status' => 0]);

        $editAddress= $this->findById($id);
        $editAddress->name = $request->get('name');
        $editAddress->phone = $request->get('phone');
        $editAddress->province_id = $request->get('province_id');
        $editAddress->city_id = $request->get('city_id');
        $editAddress->address = $request->get('address');
        $editAddress->plaque = $request->get('plaque');
        $editAddress->post_code = $request->get('post_code');
        $editAddress->user_id = auth()->id();

        $editAddress->save();

        toast('آدرس  با موفقیت بروز  شد', 'success');


    }

    public function delete($slug)
    {

    }

    public function validator($request, $id)
    {
        return $validator = Validator::make($request->all(), [
            'mobile' => 'required|unique:users,mobile,' . $id,
            'email' => 'required|unique:users,email,'.$id,
        ], [
            'mobile.required'=>'موبایل کاربر را وارد کنید',
            'mobile.unique'=>'شماره موبایل تکراری است',
            'email.unique'=>'ایمیل تکرای است',
            'email.required'=>'ایمیل را وارد کنید',

        ]);

    }


    public function enable($id)
    {
        DB::table('addresses')->where([['status', 1], ['user_id', auth()->id()]])->update(['status' => 0]);
        DB::table('addresses')->where([['id', $id], ['user_id', auth()->id()]])->update(['status' => 1]);
    }
}

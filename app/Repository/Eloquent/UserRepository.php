<?php


namespace App\Repository\Eloquent;


use App\Category;
use App\Repository\UserRepositoryInterface;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserRepository implements UserRepositoryInterface
{

    protected $users;

    public function __construct(User $users)
    {
        $this->users = $users;
    }

    public function allWithPaginate($page)
    {
        return User::paginate($page);
    }

    public function findById($id)
    {
        return User::findOrFail($id);
    }


    public function all()
    {
        return User::all();

    }

    public function create($request)
    {
        $newUser= new User;
        $newUser->name = $request->get('name');
        $newUser->family = $request->get('family');
        $newUser->email = $request->get('email');
        $newUser->national_code = $request->get('national_code');
        $newUser->cart = $request->get('cart');
        $newUser->mobile = $request->get('mobile');
        $newUser->phone = $request->get('phone');
        $newUser->role = $request->get('role');
        if ($request->get('password')!=null)
            $newUser->password = Hash::make($request->get('password'));
        $newUser->foreign = 0;
        $newUser->subscribe = 0;
        if ($request->input('status') == 'on') {
            $newUser->status = 1;
        }
        else
          $newUser->status = 0;
        $newUser->save();

        toast('کاربر با موفقیت ایجاد  شد', 'success');
    }



    public function update($id, $request)
    {

        $user=$this->findById($id);
        $user->name = $request->get('name');
        $user->family = $request->get('family');
        $user->email = $request->get('email');
        $user->national_code = $request->get('national_code');
        $user->cart = $request->get('cart');
        $user->mobile = $request->get('mobile');
        $user->phone = $request->get('phone');
        $user->role = $request->get('role');
        if ($request->get('password')!=null)
            $user->password = Hash::make($request->get('password'));
        $user->foreign = 0;
        $user->subscribe = 0;
        if ($request->input('status') == 'on') {
            $user->status = 1;
        }
         else
          $user->status = 0;
        $user->save();

        toast('با موفقیت بروز  شد', 'success');

    }

    public function delete($slug)
    {

    }

    public function validator($request, $id)
    {
        return $validator = Validator::make($request->all(), [
            'mobile' => 'required|unique:users,mobile,' . $id,
            'email' => 'required|unique:users,email,'.$id,
        ], [
            'mobile.required'=>'موبایل کاربر را وارد کنید',
            'mobile.unique'=>'شماره موبایل تکراری است',
            'email.unique'=>'ایمیل تکرای است',
            'email.required'=>'ایمیل را وارد کنید',

        ]);

    }


}

<?php


namespace App\Repository\Eloquent;


use App\Cost;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CostRepository implements \App\Repository\CostRepositoryInterface
{

    public function all()
    {
       return Cost::all();
    }

    public function enabled()
    {
        return Cost::where('status',1)->latest()->first();
    }

    public function allWithPaginate($page)
    {
        return Cost::paginate($page);
    }

    public function create($request)
    {
        DB::table('costs')->where([['status', 1]])->update(['status' => 0]);


        $cost = new Cost();
        $cost->title = $request->get('title');
        $cost->price = $request->get('price');
        $cost->free = $request->get('free');
        if ($request->get('status') == 'on')
            $cost->status =1;
        else
            $cost->status =0;

        $cost->user_id = auth()->id();
        $cost->save();

        toast('با موفقیت ایجاد  شد', 'success');
    }

    public function delete($slug)
    {
        // TODO: Implement delete() method.
    }

    public function update($id, $request)
    {
        DB::table('costs')->where([['status', 1]])->update(['status' => 0]);

        $cost=$this->findById($id);
        $cost->title = $request->get('title');
        $cost->price = $request->get('price');
        $cost->free = $request->get('free');
        if ($request->get('status') == 'on')
            $cost->status =1;
        else
            $cost->status =0;

        $cost->save();

        toast('با موفقیت بروز  شد', 'success');

    }

    public function findById($id)
    {
        return Cost::findOrFail($id);
    }

    public function validator($request, $id)
    {
        $cost = $this->findById($id);
        return $validator = Validator::make($request->all(), [
            'title' => 'required|min:3|unique:costs,title,' . $cost->id,
            'price' => 'required|numeric',
        ], [
            'title.required' => 'عنوان هزینه  خالیست',
            'title.min' => 'مقدار هزینه کمتر از سه کاراکتر است',
            'title.unique' => 'این هزینه قبلا ثبت شده است',
            'price.required' => 'مبلغ هزینه خالیست',
            'price.numeric' => 'لطفا فقط عدد وارد کنید',
        ]);
    }

}

<?php


namespace App\Repository\Eloquent;


use App\AttributeValue;
use App\Category;
use App\Product;
use App\Repository\AttValueRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AttvalueRepository implements AttValueRepositoryInterface
{

    protected $attribute;

    public function __construct(AttributeValue $attribute)
    {
        $this->attribute = $attribute;
    }


    public function findBySlug($slug)
    {
        return AttributeValue::where('slug', $slug)->first();

    }

    public function findByIdWithAllRelation($id)
    {
        return AttributeValue::with('attGroup')->where('attributeGroup_id', $id)->get();
    }

    public function findById($id)
    {
        return AttributeValue::findOrFail($id);
    }

    public function allByPaginate($page)
    {
        return AttributeValue::paginate($page);
    }

    public function AllWithByPaginate($page)
    {
        return AttributeValue::with('attGroup')->paginate($page);
    }

    public function AllWithByPaginateWithOrder($page)
    {
        return AttributeValue::with('attGroup')->orderBy('attributeGroup_id')->paginate($page);
    }

    public function all()
    {
        return AttributeValue::all();

    }

    public function create($request)
    {
        $newAttValue = new AttributeValue();
        $newAttValue->sku = Category::generateSKU();
        $newAttValue->slug = Category::makeSlug($request->input('title'));
        $newAttValue->title = $request->input('title');
        $newAttValue->color = $request->input('color');
        if ($request->input('size') == 'on')
            $newAttValue->size = 1;
        else
            $newAttValue->size = 0;
        $newAttValue->attributeGroup_id = $request->input('att_group_id');

        $newAttValue->save();
        toast('با موفقیت ایجاد  شد', 'success');
    }


    public function update($slug, $request)
    {
        $attribute = AttributeValue::where('slug', $slug)->first();

        $attribute->slug = Category::makeSlug($request->input('title'));
        $attribute->title = $request->input('title');
        $attribute->color = $request->input('color');
        if ($request->input('size') == 'on')
            $attribute->size = 1;
        else
            $attribute->size = 0;
        $attribute->attributeGroup_id = $request->input('att_group_id');

        $attribute->save();
        toast('با موفقیت بروز  شد', 'success');

    }

    public function delete($slug)
    {

    }

    public function validator($request, $slug)
    {
        $attribute = $this->findBySlug($slug);

        return $validator = Validator::make($request->all(), [
            'title' => 'required|unique:attribute_values,title,' . $attribute->id,
        ], [
            'title.required' => 'عنوان مقادیر ویژگی  خالیست',
            'title.unique' => 'این مقادیر ویژگی قبلا ثبت شده است']);

    }

    public function showProductAttribute($page)
    {
        return DB::table('products')
            ->Join('attvalues', 'attvalues.product_id', '=', 'products.id')
            ->Join('attribute_values', 'attribute_values.id', '=', 'attvalues.attributeValue_id')
            ->Join('attribute_groups', 'attribute_groups.id', '=', 'attribute_values.attributeGroup_id')
            ->select(
                'products.title as productName',
                'products.qty',
                'attvalues.sizeCount',
                'attvalues.colorCount',
                'attribute_values.title as att_value',
                'attribute_values.color as color_status',
                'attribute_values.size as  size_status',
                'attribute_groups.title as att_group',
            )
            ->paginate($page);
    }

    public function productAttribute($sku)
    {
        return DB::table('products')
            ->Join('attvalues', 'attvalues.product_id', '=', 'products.id')
            ->Join('attribute_values', 'attribute_values.id', '=', 'attvalues.attributeValue_id')
            ->select(
                'products.title as productName',
                'products.qty',
                'attvalues.sizeCount',
                'attvalues.colorCount',
                'attribute_values.title as att_value',
                'attribute_values.color as color_status',
                'attribute_values.size as  size_status',
            )
            ->where('products.sku',$sku)
            ->get();

    }
}

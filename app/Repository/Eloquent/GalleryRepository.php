<?php


namespace App\Repository\Eloquent;


use App\Photo;
use App\Repository\GalleryRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class GalleryRepository implements GalleryRepositoryInterface
{
    protected $gallery;

    /**
     * GalleryRepository constructor.
     * @param $gallery
     */
    public function __construct(Photo $gallery)
    {
        $this->gallery = $gallery;
    }


    public function all()
    {
    }

    public function allWithPaginate($page)
    {
        return Photo::where('type',1)->paginate($page);
    }

    public function create($request)
    {
        $uploadedFile = $request->file('file');
        $filename = time().$uploadedFile->getClientOriginalName();
        $original_name = $uploadedFile->getClientOriginalName();

        Storage::disk('local')->putFileAs(
            'public/photos', $uploadedFile, $filename
        );

        $photo = new Photo();
        $photo->original_name = $original_name;
        $photo->path = $filename;
        $photo->type = 1;;
        $photo->user_id = auth()->id();
        $photo->save();

        return $photo->id;
    }

    public function delete($id)
    {
    }

    public function update($id, $request)
    {
        $photo = $this->findById($id);

        $photo->place = $request->place;
        $photo->detail = $request->detail;
        if ($request->input('status') == 'on')
            $photo->status = 1;
        else
            $photo->status = 0;
        $photo->save();
        toast('تصویر با موفقیت ویرایش  شد', 'success');
    }

    public function findById($id)
    {
        return Photo::findOrFail($id);
    }

    public function validator($request, $id)
    {
        $gallery= $this->findById($id);
//        return $validator = Validator::make($request->all(), [
//            'detail' => 'required',
//            'place' => 'required',
//        ], [
//            'detail.required' => 'عنوان عکس  خالیست',
//            'place.required' => 'مکان عکس  خالیست',
//        ]);
    }
}

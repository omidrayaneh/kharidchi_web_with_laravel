<?php


namespace App\Repository;


interface ProductRepositoryInterface
{
    public function allWithPhotoAndPaginate($page);
    public function allWithPhoto();

    public function all();

    public function create($category);

    public function delete($slug);

    public function update($slug,$request);

    public function findBySlug($slug);

    public function findBySku($sku);

    public function findBySkuWithAllRelations($sku);

    public function findById($id);

    public function validator($request,$category);


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    public function favourable()
    {
        return $this->morphTo()->with('photos');
    }
}

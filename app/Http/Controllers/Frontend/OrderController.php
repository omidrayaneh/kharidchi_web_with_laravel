<?php

namespace App\Http\Controllers\Frontend;

use App\AttributeGroup;
use App\AttributeValue;
use App\Cost;
use App\Coupon;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Shetabit\Payment\Facade\Payment;
use Shetabit\Payment\Invoice;

class OrderController extends Controller
{
    public function verify($value =null)
    {
        $cost = Cost::where('status',1)->latest()->first();
        $amount = (int)intval(str_replace(',', '', \Cart::gettotal()));
        if (!empty($value)) {
            $coupon = Coupon::findOrFail($value);
            $amount = $amount - $coupon->price;
        }
        $flag = true;
        $saved_order_item=null;
        $orderId = 0;

//        foreach (\Cart::getContent() as $saved_items) {
//            $saved_order_item = OrderItem::where('product_id', $saved_items->id )
//                                        ->where('price', $saved_items->price)
//                                        ->where('discount', $saved_items->attributes->discount)
//                                        ->where('qty', $saved_items->quantity)
//                                        ->first();
//            if (empty($saved_order_item)){
//                break;
//            }
//        }
//        if (!empty($saved_order_item)){
//            $saved_order = Order::where('id', $saved_order_item->order_id)
//                ->where('status',0)
//                ->where('user_id', auth()->user()->id)
//                ->first();
//        }

//        if(!empty($saved_order) && $saved_order->amount==$amount){
//            $flag = false;
//        }

      //  if ($flag) {
            $items = \Cart::getContent();
            $order = new Order();
            if (!empty($cost) && $cost->free >$amount )
                $amount = $amount + (int)$cost->price;
            $order->amount = $amount;
            if (!empty($coupon))
               $order->coupon_discount = $coupon->price;
            $order->status = 0;
            $order->user_id = auth()->user()->id;
            $order->save();
            Session::flash('order', $order );
            $orderId = $order->id;
            foreach (\Cart::getContent() as $item) {
                $attValue = [];
                $attGroup = [];
                $product = Product::where('sku',$item->attributes->productId)->first();
                if ($item->attributes->attValue){
                    $attValue = AttributeValue::where('title',$item->attributes->attValue)->first();
                    $attGroup = AttributeGroup::findOrFail($attValue->attributeGroup_id);
                }

                $orderItems = new OrderItem();
                $orderItems->order_id = $orderId;
                $orderItems->qty = $item->quantity;
                $orderItems->price = $product->price;
                $orderItems->discount = $product->discount;
                $orderItems->product_id = $product->id;
                if ($item->attributes->attValue){
                    $orderItems->attValue = $attValue->id;
                    $orderItems->attGroup = $attGroup->id;
                }
                $orderItems->save();

            }
//        } else {
//            $orderId = $saved_order->id;
//        }
        $invoice = (new Invoice)->amount($amount);

        $invoice->detail(['detailName' => __('word.storeName')]);
        $invoice->uuid($orderId);
        $message = 'Ok';
       // $changedProducts = [];

        $checkOrder = Order::with('items')->whereId($orderId)->first();

//        if ($checkOrder) {
//            foreach ($checkOrder->items as $item) {
//
//                $product = Product::findorfail($item->product_id);
//                $basketp = $product->price * $product->discount / 100;
//                $basketp = $item->price + $basketp;
//                if ($product->status == 1 && $basketp == $product->price && $item->discount == $product->discount) {
//                    $message = 'Ok';
//               /* } else {
//                    array_push($changedProducts, $item->product_id);
//                    $itemB = OrderItem::where('product_id', $item->product_id)->first();
//                    if ($itemB) {
//                        $item->status = 0;
//                        $itemB->save();
//                    }
//                    $message = 'NotOk';*/
//                }
//            }
//        }

        if ($message == 'Ok') {
            return Payment::purchase($invoice, function ($driver, $transactionId) use ($orderId) {
                $payment = new \App\Payment();
                $payment->order_id = $orderId;
                $payment->authority = $transactionId;
                $payment->save();
            })->pay();

        } else {
            // bargasht be safhe avval va darkhaste tayide taghirate geymat
            // tarrahie method baraye tayide ajnas mojadad
            // dar haman safhe ajnasi ke status 0 mibashad darkhaste tayid ham mikhaym azashun va geymate jadid o gadimesho neshun midim
            return back();
        }

    }
}

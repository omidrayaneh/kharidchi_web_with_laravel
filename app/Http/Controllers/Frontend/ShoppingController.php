<?php

namespace App\Http\Controllers\Frontend;

use App\Address;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShoppingController extends Controller
{
    public function index()
    {
        return view('shopping.index');
    }

    public function shoppingPayment()
    {
       $address= Address::where([['user_id',auth()->id()],['status',1]])->first();
        if (empty($address))
             return redirect('/profile/addresses');
        return view('shopping.shopping-payment',compact(['address']));
    }
    public function completeBuy()
    {
        return view('shopping.shopping-complete-buy');
    }
    public function notCompleteBuy()
    {
        return view('shopping.shopping-not-complete-buy');
    }
}

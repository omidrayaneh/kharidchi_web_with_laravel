<?php

namespace App\Http\Controllers\Frontend;

use App\Favorite;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function profile()
    {
        $user=User::find(Auth::user()->id);
        $favorites=Auth::user()->userFavorite()->get();
        return view('profile.index',compact('user','favorites'));
    }
    public function profileAdditional()
    {
        return view('profile.additional-info');
    }
    public function profilePersonal()
    {
        $user=User::find(Auth::user()->id);
        return view('profile.personal-info',compact('user'));
    }

    public function addresses()
    {

    }
    public function favorites()
    {
        $user=User::find(Auth::user()->id);
        $favorites=Auth::user()->userFavorite()->get();
        return view('profile.favorites',compact(['favorites','user']));
    }
    public function updateProfile(Request $request)
    {
       $user= auth()->user();
       $user->name=$request->input('name');
       $user->family=$request->input('family');
       $user->phone=$request->input('phone');
       $user->cart=$request->input('cart');
       $user->national_code=$request->input('national_code');
       $user->email=$request->input('email');
       if ($request->input('subscribe'))
           $user->subscribe=1;
       else
           $user->subscribe=0;
        if ($request->input('foreign'))
            $user->foreign=1;
        else
            $user->foreign=0;
       $user->save();
        alert()->success('ویرایش موفق','پروفایل شما با موفقیت ویرایش شد');
       return back();

    }
    public function orderList(){
        $user=User::find(Auth::user()->id);
        $orders=Order::where('user_id',auth()->user()->id)->latest()->paginate(7);
        return view('profile.orders',compact(['orders','user']));
    }
}

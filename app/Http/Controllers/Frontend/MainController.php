<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\Photo;
use App\Product;
use App\Repository\Eloquent\BrandRepository;
use App\Repository\Eloquent\CategoryRepository;
use App\Repository\Eloquent\ProductRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    protected $category, $products, $brand;

    public function __construct(ProductRepository $products, BrandRepository $brand
        , CategoryRepository $category)
    {
        $this->category = $category;
        $this->brand = $brand;
        $this->products = $products;
    }

    public function index()
    {

//        $sells_products = Product::with('photos')
//            ->whereHas('orderItems', function (Builder $query) {
//                $query->where('status', 1);
//            })->get();

        $sliders = Photo::where([['status',1],['place' , 'slider']] )->get();
        $res_sliders = Photo::where([['status',1],['place' , 'res-slider']] )->get();
        $sidebar_banner = Photo::where([['status',1],['place' , 'sidebar-banner']] )->latest()->first();
        $medium_banners = Photo::where([['status',1],['place' , 'medium-banner']] )->get();
        $small_banners = Photo::where([['status',1],['place' , 'small-banner']] )->get();
        $large_banner = Photo::where([['status',1],['place' , 'large-banner']] )->first();

        $sells_products = DB::table('products')
            ->Join('order_items', 'order_items.product_id', '=', 'products.id')
            ->Join('orders', 'orders.id', '=', 'order_items.order_id')
            ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
            ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
            ->where('orders.status', 1)
            ->where('products.status', 1)
            ->select('products.*', 'photos.*')
            ->groupBy('products.id')
            ->limit(10)->get();


          $products = Product::with('photos', 'orderItems')
            ->where('status',1)
            ->orderBy('created_at', 'DESC')->limit(10)->get();//last products

          $random_product = Product::with('photos', 'orderItems')
              ->where('status',1)
             ->inRandomOrder()
              ->limit(1)->first();//random products
        return view('index', compact([
            'products',
            'sells_products',
            'sliders',
            'res_sliders',
            'sidebar_banner',
            'medium_banners',
            'small_banners',
            'large_banner',
            'random_product'
            ]));
    }

}

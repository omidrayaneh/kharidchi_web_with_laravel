<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Otp;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Ipecompany\Smsirlaravel\Smsirlaravel;
use Mockery\Exception;

class UserController extends Controller
{

    //show register view
    public function index()
    {
        return view('auth/register');
    }

    // create user and sent otp eith sms to user mobile
    public function register(Request $request)
    {

            $validator = Validator::make($request->all(), [
                'mobile' => 'required|unique:users',
                'password' => 'required|min:8',
            ]);
            if ($validator->fails()) {
                alert()->error('اخطار','خطا در ورود اطلاعات');

                return redirect('/register')->withErrors($validator)->withInput();
            }

            $user = new User();
            $user->mobile = $request->get('mobile');
            $user->password = Hash::make($request->get('password'));
            $randomOtp = mt_rand(10000, 99999);
              $msg = Smsirlaravel::ultraFastSend(['VerificationCode'=>$randomOtp],49435,$request->get('mobile'));
            $msg['IsSuccessful'] = true;
            if ($msg['IsSuccessful']) {
                $user->save();
                $userId = $user->id;

                $otp = new Otp();
                $otp->user_id = $userId;
                $otp->otp = $randomOtp;
                $otp->save();

                // return redirect otp page;
                return view('verify')->with('user', $user);

            } else {
                return redirect('register');
            }


    }


    //show verify view
    public function verify()
    {
        return view('verify');
    }

    //check otp equal with enter user otp
    public function verification(Request $request)
    {
        $enterOtp = $request->get('a')
            . $request->get('b')
            . $request->get('c')
            . $request->get('d')
            . $request->get('e');

        if ($request->has('userId')) {

            $userId = $request->get('userId');

            $user = User::where('id', $userId)->first();

            $newUser = User::where('id', $userId)->first();

            $userOtp = Otp::where('user_id', $user->id)->latest()->first();

            if ($newUser!=null){
                if ($userOtp->otp == $enterOtp) {
                    $user->status = 1;
                    $user->save();
                    $userOtp->status = 1;
                    $userOtp->save();
                    //change password
                    return view('resetPassword')->with('user', $user->id);
                }
            }else{
                if ($userOtp->otp == $enterOtp) {
                    $user->status = 1;
                    $user->save();
                    $userOtp->status = 1;
                    $userOtp->save();
                    return redirect('login');
                }
            }

            return view('verify')->with('user', $user);
        }
        return redirect('login');

    }


    public function forgot(Request $request)
    {
        return view('auth/forgot');
    }

    public function forgotPassword(Request $request)
    {

        $mobile = $request->get('mobile');
        $user = new User();
        $user = User::where('mobile', $mobile)->first();

        $randomOtp = mt_rand(10000, 99999);
          $msg = Smsirlaravel::ultraFastSend(['VerificationCode'=>$randomOtp],49436 ,$request->get('mobile'));
        $msg['IsSuccessful'] = true;
        if ($msg['IsSuccessful']) {

            $otp = new Otp();
            $otp->user_id = $user->id;
            $otp->otp = $randomOtp;
            $otp->save();

            // return redirect otp page;
            return view('verify')->with('user', $user);


        }

    }

    public function reset()
    {
        return view('resetPassword');
    }

    public function changePassword(Request $request)
    {
        $newPassword=$request->get('password');
       $userId=$request->get('user');
       $user=User::whereId($userId)->first();
       $user->password=Hash::make($newPassword);
       $user->save();
       return redirect('login');

    }


}

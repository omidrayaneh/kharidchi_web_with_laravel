<?php

namespace App\Http\Controllers\Frontend;

use App\Address;
use App\AttributeGroup;
use App\AttributeValue;
use App\Attvalue;
use App\Cost;
use App\Http\Controllers\Controller;
use App\Product;
use App\Repository\Eloquent\CostRepository;
use App\Repository\Eloquent\ProductRepository;
use Illuminate\Support\Facades\Auth;
use Psy\Util\Str;


class CartController extends Controller
{

    protected $products;
    /**
     * @var CostRepository
     */
    private $cost;

    public function __construct(ProductRepository $products,CostRepository $cost)
    {
        $this->products = $products;
        $this->cost = $cost;
    }

    public function addToCart($sku,$idd=null)
    {
//        if(
//            Auth::user()->name == null ||
//            Auth::user()->family == null ||
//            Auth::user()->phone == null||
//            Auth::user()->cart == null||
//            Auth::user()->email == null
//        ){
//            Alert('خطا','لطفا مشخصات کاربری خود را کامل کنید','error');
//            return redirect(route('additional-info'));
//        }
      //  $address = Address::where('user_id',auth()->id())->get();
//        if(count($address)==0){
//            Alert('خطا','لطفا آدرس خود را وارد کنید','error');
//            return redirect(route('addresses.index'));
//
//        }
        $product = $this->products->findBySkuWithAllRelations($sku);



        $qty = 0;//for all att value
        $color_qty = 0;// color size limit
        $size_qty = 0;//size  limit
        foreach( \Cart::getContent() as $item){
            if($item->attributes->productId == $product->sku ){
                $qty +=$item->quantity;
                if($product->qty == $qty){
                    Alert('خطا','آخرین کالا را انتخاب کرده اید','error');
                    return back();
                }
            }
        }



        $attValue=null;
        $attGroup=null;
        if ($idd!=null){//size or color attribute value

             $attValue=AttributeValue::findOrFail($idd);
             $attGroup = AttributeGroup::where('id',$attValue->attributeGroup_id)->first();
              $attval = Attvalue::where([['product_id',$product->id],['attributeValue_id',$attValue->id]])->first();
            //check color or size count exist
            foreach( \Cart::getContent() as $item){

                if ($item->attributes->attValueId == $attval->attributeValue_id){
                    if ($item->quantity== $attval->colorCount  && $attval->colorCount != null){

                        Alert('خطا','آخرین کالا را انتخاب کرده اید','error');
                        return back();
                    }
                    elseif ($item->quantity== $attval->sizeCount  && $attval->colorCount == null){
                        Alert('خطا','آخرین کالا را انتخاب کرده اید','error');
                        return back();
                    }
                }
            }


            // add the product to cart with attValue multiple
            \Cart::add(array(
                'id' => $product->id + $attValue->id,
                'name' => $product->title,
                'price' => $product->price - ($product->price * $product->discount / 100),
                'quantity' => 1,
                'attributes' => array(
                    'productId'=> $product->sku,
                    'path' => $product->photos[0]->path,
                    'discount' => $product->discount,
                    'discount_price' => $product->price * $product->discount / 100,
                    'coupon' => 0,
                    'attValueId' => $attValue->id,
                    'attValue' => $attValue->title,
                    'color' => $attValue->color,
                    'attGroup' => $attGroup->title
                ),
            ));

            return back();
        }else{
            // add the product to cart with out attValue single
            \Cart::add(array(
                'id' => $product->id ,
                'name' => $product->title,
                'price' => $product->price - ($product->price * $product->discount / 100),
                'quantity' => 1,
                'attributes' => array(
                    'productId'=> $product->sku,
                    'path' => $product->photos[0]->path,
                    'discount' => $product->discount,
                    'discount_price' => $product->price * $product->discount / 100,
                    'coupon' => 0
                ),
            ));
            return back();
        }




    }

    public function removeCart($id)
    {
        $item = \Cart::get($id);
            if ($item->quantity == 1)
                \Cart::remove($id);
            else
                \Cart::update($id, array(
                    'quantity' => -1,
                ));
            return back();

    }

    public function showCart()
    {
        return view('cart.index');
    }


}

<?php

namespace App\Http\Controllers\Frontend;

use App\AttributeGroup;
use App\AttributeValue;
use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\Repository\Eloquent\AttgroupRepository;
use App\Repository\Eloquent\AttvalueRepository;
use App\Repository\Eloquent\CategoryRepository;
use App\Repository\Eloquent\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\queue;


class ProductController extends Controller
{
    protected $products, $brand, $attValue, $attGroup, $comments;
    /**
     * @var CategoryRepository
     */
    private $category;

    public function __construct(ProductRepository $products, AttvalueRepository $attValue,
                                AttgroupRepository $attGroup, CategoryRepository $category)
    {
        $this->products = $products;
        $this->attValue = $attValue;
        $this->attGroup = $attGroup;
        $this->category = $category;
    }

    public function index($sku)
    {
        $values = DB::table('products')
            ->Join('attributeValue_product', 'attributeValue_product.product_id', '=', 'products.id')
            ->Join('attribute_values', 'attribute_values.id', '=', 'attributeValue_product.attributeValue_id')
            ->Join('attribute_groups', 'attribute_groups.id', '=', 'attribute_values.attributeGroup_id')
            ->select('attribute_groups.title as groupTitle', 'attribute_values.title as valueTitle')
            ->where('products.sku', $sku)
            ->where('attribute_groups.number', 0)
            ->groupBy('attribute_groups.id')
            ->limit(10)->get();

        $attGroup = $this->attGroup->findByIdWithAllRelation();
        $product = $this->products->findBySkuWithAllRelations($sku);


        if ($product->qty == 0)
            return redirect('/');

        $product->visit++;
        $product->save();
        $comments = $product->comments->where('status', 1);

        $star = 0;
        if (count($comments) != 0) {

            foreach ($comments as $comment) {
                $star += $comment->star;
            }
            $star = $star / count($comments);
        }

        return view('product.index', compact(['product', 'attGroup', 'comments', 'star', 'values']));
    }

    public function productList($sku, $slug)
    {

        $category = Category::where('sku', $sku)->first();
        $title = $category->title;
        return view('category.show-products', compact(['sku', 'category','title']));
    }

    public function productAllProducts($sku,$value)
    {
        if ($value == 'false') {
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
               // ->Join('order_items', 'order_items.product_id', '=', 'products.id')
              //  ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.created_at')
                ->where('categories.sku', $sku)
              //  ->where('orders.status', 1)
                ->where('products.status', 1)
                ->paginate(20);
        }
        else{
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
               // ->Join('order_items', 'order_items.product_id', '=', 'products.id')
               // ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.created_at')
               // ->where('orders.status', 1)
                ->where('products.status', 1)
               ->where('categories.sku', $sku)
               ->where('products.qty','!=',0)
                ->paginate(20);
        }
        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function GetAscProducts($sku, $value)
    {
        if ($value == 'false'){
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
             //   ->Join('order_items', 'order_items.product_id', '=', 'products.id')
              //  ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
              //  ->where('orders.status', 1)
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.price', 'ASC')
                ->where('categories.sku', $sku)
                ->paginate(20);
            }
        else{
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
              //  ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
             //   ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
             //   ->where('orders.status', 1)
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.price', 'ASC')
                ->where('categories.sku', $sku)
                ->where('products.qty', '!=',0)
                ->paginate(20);
            }

        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function GetDescProducts($sku, $value)
    {

        if ($value == 'false'){
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
              //  ->Join('order_items', 'order_items.product_id', '=', 'products.id')
               // ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
               // ->where('orders.status', 1)
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.price', 'DESC')
                ->where('categories.sku', $sku)
                ->paginate(20);
        }else{

            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
              //  ->Join('order_items', 'order_items.product_id', '=', 'products.id')
             //   ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
            //    ->where('orders.status', 1)
                ->where('products.status', 1)
                ->where('products.qty','!=' ,0)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.price', 'DESC')
                ->where('categories.sku', $sku)
                ->paginate(20);
        }

        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function GetVisitedProducts($sku,$value)
    {
        if ($value == 'false'){
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
              //  ->Join('order_items', 'order_items.product_id', '=', 'products.id')
              //  ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
              //  ->where('orders.status', 1)
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.visit', 'DESC')
                ->where('categories.sku', $sku)
                ->paginate(20);
        }else{
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
              //  ->Join('order_items', 'order_items.product_id', '=', 'products.id')
              //  ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
              //  ->where('orders.status', 1)
                ->where('products.status', 1)
                ->where('products.qty','!=', 0)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.visit', 'DESC')
                ->where('categories.sku', $sku)
                ->paginate(20);
        }


        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function GetNewProducts($sku)
    {
        $products = Product::with('photos')->whereHas('categories', function ($q) use ($sku) {
            $q->where('sku', $sku);
        })->orderBy('created_at', 'DESC')
            ->paginate(2);
        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function GetSellProducts($sku,$value)
    {
        if ($value == 'false'){
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('orders.status', 1)
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->where('categories.sku', $sku)
                ->paginate(20);
        }
        else{
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('orders.status', 1)
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->where('categories.sku', $sku)
                ->where('products.qty', '!=',0)
                ->paginate(20);
            }

        $response = [
            'products' => $products
        ];
        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    /////////////////////////main search
    public function search(Request $request)
    {


        $data = $request->q;
        if (empty($data))
            return redirect('/');

        $product = Product::where('title', 'like', '' . $data . '')->first();

        $category = Category::where('title', 'like', '%' . $data . '%')->first();
      //  $category = $this->fatherRecursive($category->fatherRecursive);
        if (!empty($category)) {
          //  $category = $this->fatherRecursive($category);
            $category = Category::with('childrenRecursive')->where('id', $category->id)->first();
            return view('product.main-search', compact(['data', 'category']));
        } else
            return view('product.main-search', compact(['data']));

    }

    public function fatherRecursive($category)
    {

        if ($category->fatherRecursive)
            return $this->fatherRecursive($category->fatherRecursive);
        else
            return $category;

    }


    public function searchAll($data, $value)
    {

        $products = '';
        if ($value == 'false') {
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->where('products.status', 1)
                ->where('products.title', 'like', '%' . $data . '%')
                ->orWhere('categories.title', 'like', '%' . $data . '%')
                ->paginate(20);
        } else {
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->where('products.status', 1)
                ->where('products.title', 'like', '%' . $data . '%')
                ->orWhere('categories.title', 'like', '%' . $data . '%')
                ->where('products.qty', '!=', 0)
                ->paginate(20);
        }

        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function searchPriceDesc($data, $value)
    {
        $products = '';
        if ($value == 'false') {
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->where('products.status', 1)
                ->where('products.title', 'like', '%' . $data . '%')
                ->orWhere('categories.title', 'like', '%' . $data . '%')
                ->orderBy('products.price', 'DESC')
                ->paginate(20);
        } else {
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->where('products.status', 1)
                ->where('products.title', 'like', '%' . $data . '%')
                ->orWhere('categories.title', 'like', '%' . $data . '%')
                ->where('products.qty', '!=', 0)
                ->orderBy('products.price', 'DESC')
                ->paginate(20);
        }

        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function searchPriceAsc($data, $value)
    {
        $products = '';
        if ($value == 'false') {
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->where('products.status', 1)
                ->where('products.title', 'like', '%' . $data . '%')
                ->orWhere('categories.title', 'like', '%' . $data . '%')
                ->orderBy('products.price', 'ASC')
                ->paginate(20);
        } else {
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->where('products.title', 'like', '%' . $data . '%')
                ->where('products.status', 1)
                ->orWhere('categories.title', 'like', '%' . $data . '%')
                ->where('products.qty', '!=', 0)
                ->orderBy('products.price', 'ASC')
                ->paginate(20);
        }

        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function searchVisit($data, $value)
    {
        $products = '';
        if ($value == 'false') {
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->where('products.status', 1)
                ->where('products.title', 'like', '%' . $data . '%')
                ->orWhere('categories.title', 'like', '%' . $data . '%')
                ->orderBy('products.visit', 'DESC')
                ->paginate(20);
        } else {
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->where('products.status', 1)
                ->where('products.title', 'like', '%' . $data . '%')
                ->orWhere('categories.title', 'like', '%' . $data . '%')
                ->where('products.qty', '!=', 0)
                ->orderBy('products.visit', 'DESC')
                ->paginate(20);
        }

        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function searchSells($data, $value)
    {
        $products = '';

        if ($value == 'false') {
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->where('products.status', 1)
                ->where('orders.status', 1)
                ->where('products.title', 'like', '%' . $data . '%')
                ->orWhere('categories.title', 'like', '%' . $data . '%')
                ->paginate(20);
        } else {
            $products = DB::table('products')
                ->Join('category_product', 'category_product.product_id', '=', 'products.id')
                ->Join('categories', 'category_product.category_id', '=', 'categories.id')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->where('products.status', 1)
                ->where('orders.status', 1)
                ->where('products.title', 'like', '%' . $data . '%')
                ->orWhere('categories.title', 'like', '%' . $data . '%')
                ->where('products.qty', '!=', 0)
                ->paginate(20);
        }

        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    ////////////////////// sells products
    public function productSell()
    {
        return view('category.sell-products');
    }

    public function productSellData($value)
    {
        if ($value == 'false'){
            $sells_products = DB::table('products')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('orders.status', 1)
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->paginate(20);
            }
        else{
            $sells_products = DB::table('products')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('orders.status', 1)
                ->where('products.status', 1)
                ->where('products.qty', '!=',0)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->paginate(20);
        }

        $response = [
            'products' => $sells_products
        ];
        return response()->json($response, 200);
    }

    public function GetSellsNewProducts($value)
    {

        if ($value == 'false'){
            $sells_products = DB::table('products')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('orders.status', 1)
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('orders.created_at', 'DESC')
                ->paginate(20);
        }
        else{
            $sells_products = DB::table('products')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('orders.status', 1)
                ->where('products.status', 1)
                ->where('products.qty', '!=',0)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('orders.created_at', 'DESC')
                ->paginate(20);
        }
        $response = [
            'products' => $sells_products
        ];
        return response()->json($response, 200);
    }

    public function GetSellsVisitedProducts($value)
    {
        if ($value == 'false'){
            $sells_products = DB::table('products')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('orders.status', 1)
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.visit', 'DESC')
                ->paginate(20);
        }
        else{
            $sells_products = DB::table('products')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('orders.status', 1)
                ->where('products.status', 1)
                ->where('products.qty', '!=',0)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.visit', 'DESC')
                ->paginate(20);
        }

        $response = [
            'products' => $sells_products
        ];
        return response()->json($response, 200);
    }

    public function GetSellsAcePriceProducts($value)
    {
        if ($value == 'false'){
            $sells_products = DB::table('products')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('orders.status', 1)
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.price', 'ASC')
                ->paginate(20);
        }
        else{
            $sells_products = DB::table('products')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('orders.status', 1)
                ->where('products.status', 1)
                ->where('products.qty', '!=',0)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.price', 'ASC')
                ->paginate(20);
        }

        $response = [
            'products' => $sells_products
        ];
        return response()->json($response, 200);
    }

    public function GetSellsDescPriceProducts($value)
    {
        if ($value == 'false') {
           $products = DB::table('products')
               ->Join('order_items', 'order_items.product_id', '=', 'products.id')
               ->Join('orders', 'orders.id', '=', 'order_items.order_id')
               ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
               ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
               ->where('orders.status', 1)
               ->where('products.status', 1)
               ->select('products.*', 'photos.path')
               ->groupBy('products.id')
               ->orderBy('products.price', 'DESC')
               ->paginate(20);
       }
       else{
           $products = DB::table('products')
               ->Join('order_items', 'order_items.product_id', '=', 'products.id')
               ->Join('orders', 'orders.id', '=', 'order_items.order_id')
               ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
               ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
               ->where('orders.status', 1)
               ->where('products.status', 1)
               ->where('products.qty','!=',0)
               ->select('products.*', 'photos.path')
               ->groupBy('products.id')
               ->orderBy('products.price', 'DESC')
               ->paginate(20);
       }
        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    ////////////////////// new products
    public function productNew()
    {
        return view('category.new-products');
    }

    public function GetNewSellProducts($value)
    {
        $products = '';

        if ($value == 'false') {
            $products = DB::table('products')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('payments', 'orders.id', '=', 'payments.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('orders.status', 1)
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('orders.created_at', 'DESC')
                ->paginate(20);
        }
        else{
            $products = DB::table('products')
                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
                ->Join('payments', 'orders.id', '=', 'payments.order_id')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('orders.status', 1)
                ->where('products.status', 1)
                ->where('products.qty', '!=',0)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('orders.created_at', 'DESC')
                ->paginate(20);
        }

        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function productNewData($value)
    {
        $products = '';

        if ($value == 'false') {
            $products = DB::table('products')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->paginate(20);
        }
        else{
            $products = DB::table('products')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('products.status', 1)
                ->where('products.qty','!=', 0)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->paginate(20);
        }

        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function GetNewsVisitedProducts($value)
    {
        $products = '';

        if ($value == 'false') {
            $products = DB::table('products')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.visit', 'DESC')
                ->paginate(20);
        }
        else{
            $products = DB::table('products')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('products.status', 1)
                ->where('products.qty','!=' ,0)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.visit', 'DESC')
                ->paginate(20);
        }

        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function GetNewsAceProducts($value)
    {
        $products = '';

        if ($value == 'false') {
            $products = DB::table('products')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.price', 'ASC')
                ->paginate(20);
        }
        else{
            $products = DB::table('products')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('products.status',1)
                ->where('products.qty', '!=',0)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.price', 'ASC')
                ->paginate(20);
        }

        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

    public function GetNewsDeceProducts($value)
    {
        $products = '';

        if ($value == 'false') {
            $products = DB::table('products')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('products.status', 1)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.price', 'DESC')
                ->paginate(20);
        }
        else{
            $products = DB::table('products')
                ->Join('photo_product', 'photo_product.product_id', '=', 'products.id')
                ->Join('photos', 'photos.id', '=', 'photo_product.photo_id')
                ->where('products.status', 1)
                ->where('products.qty', '!=',0)
                ->select('products.*', 'photos.path')
                ->groupBy('products.id')
                ->orderBy('products.price', 'DESC')
                ->paginate(20);
        }
        $response = [
            'products' => $products
        ];
        return response()->json($response, 200);
    }

}

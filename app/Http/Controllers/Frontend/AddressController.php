<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Province;
use App\Repository\Eloquent\AddressRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AddressController extends Controller
{
    protected $addresses;

    public function __construct(AddressRepository $addresses)
    {
        $this->addresses=$addresses;
    }
    public function index()
    {
        $addresses=$this->addresses->userAddresses(auth()->id());
        $provinces=Province::with('cities')->get();
        return view('profile.addresses',compact(['provinces','addresses']));
    }


    public function create()
    {

    }


    public function store(Request $request)
    {
       $this->addresses->create($request);
       return back();
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request,$id)
    {
        $this->addresses->update($id,$request);
        return "update successfully";
    }
    public function enable($id)
    {
        DB::table('addresses')->where([['status', 1], ['user_id', auth()->id()]])->update(['status' => 0]);
        DB::table('addresses')->where([['id', $id], ['user_id', auth()->id()]])->update(['status' => 1]);
       // $this->addresses->enable($id);
        toast('آدرس مورد نظر به عنوان پیش فرض انتخاب شد', 'success');
    }

    public function destroy($id)
    {
        $address= $this->addresses->findById($id);
        $address->delete();
    }
}

<?php

namespace App\Http\Controllers\Frontend;

use App\Favorite;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    public function add($sku)
    {

        $product=Product::where('sku',$sku)->first();

        $favourites=Favorite::where('favourable_id',$product->id)
                            ->where('user_id',auth()->user()->id)
                            ->first();
        $favourite=new Favorite();
        if (empty($favourites)){
            $favourite->user_id=auth()->user()->id;
            $product->favourite()->save($favourite);
            toast('ثبت در لیست علاقه مندی', 'success');
        }else{
            $product->favourite()->delete($favourites);
            toast('حذف از لیست علاقه مندی', 'error');
        }

        return back();
    }
}

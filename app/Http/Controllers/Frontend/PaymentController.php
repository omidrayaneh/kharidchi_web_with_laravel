<?php

namespace App\Http\Controllers\Frontend;

use App\Address;
use App\Attvalue;
use App\Events\OrderEvent;
use App\Http\Controllers\Controller;
use App\Notifications\InvoicePaid;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Ipecompany\Smsirlaravel\Smsirlaravel;
use Shetabit\Payment\Exceptions\InvalidPaymentException;
use Shetabit\Payment\Facade\Payment;

class PaymentController extends Controller
{

    public function verify(Request $request)
    {
        try {

            $payment=\App\Payment::where('authority',$request->Authority)->first();
            $order=Order::with('items','user')->findOrFail($payment->order_id);
            $address = Address::where([['status',1],['user_id',$order->user_id]])->first();


            // $amont=(int)intval(str_replace(',', '', $amont));
            $receipt = Payment::amount((int)$order->amount)->transactionId($request->Authority)->verify();

            if ($payment->count()>0 && $request->Status=='OK' && $request->Authority== $payment->authority){
                $payment->status=$request->Status;
                $payment->RefID=$receipt->getReferenceId();
                $payment->save();
                $order->status=1;
                $order->save();
                foreach (\Cart::getContent() as $item) {
                    $product = Product::where('sku',$item->attributes->productId)->first();
                    $attval = Attvalue::where([['product_id',$product->id],['attributeValue_id',$item->attributes->attValueId]])->first();
                    if (isset($item->attributes->color) && $item->attributes->color == null) //if color(attValue) is null means size attribute exist in cart
                    {
                        $attval->sizeCount = $attval->sizeCount-$item->quantity;
                        $attval->save();
                    }
                    elseif (isset($item->attributes->color) && $item->attributes->color != null) //if color(attValue) is not null means color attribute exist in cart
                    {

                        $attval->colorCount = $attval->colorCount-$item->quantity;
                        $attval->save();
                    }
                    $product->qty -=  $item->quantity;
                    $product->save();

                }

                $admin = User::where('role','admin')->first();

                \Cart::clear();
                event(new OrderEvent($order));
                 Smsirlaravel::ultraFastSend(['VerificationCode'=>$order->id],49439 ,auth()->user()->mobile);
                 Smsirlaravel::ultraFastSend(['VerificationCode'=>$order->id],49438 ,$admin->mobile);
                return redirect('/shopping-complete-buy')->with('address',$address)->with('order',$order)->with('payment',$payment);;
            }
        } catch (InvalidPaymentException $exception) {
            $payment = \App\Payment::where('order_id',$order->id)->latest()->first();

            return redirect('/shopping-not-complete-buy')->with('address',$address)->with('order',$order)->with('payment',$payment);;
           // echo $exception->getMessage();
//            \Cart::clear();
        }
   }
}

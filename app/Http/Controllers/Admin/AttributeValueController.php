<?php

namespace App\Http\Controllers\Admin;

use App\AttributeGroup;
use App\AttributeValue;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAttValue;
use App\Repository\Eloquent\AttgroupRepository;
use App\Repository\Eloquent\AttvalueRepository;
use Illuminate\Http\Request;

class AttributeValueController extends Controller
{

    protected $attribute;
    protected $attGroup;

    public function __construct(AttvalueRepository $attribute, AttgroupRepository $attGroup)
    {
        $this->attribute=$attribute;
        $this->attGroup=$attGroup;

    }
    public function index()
    {

        $attributesValue=$this->attribute->AllWithByPaginateWithOrder(7);
        return view('admin.attributes-value.index',compact(['attributesValue']));
    }


    public function create()
    {
        $attvalue = 'undefined';
        $attributes=AttributeGroup::all();
        return view('admin.attributes-value.create',compact(['attributes','attvalue']));
    }


    public function store(StoreAttValue $request)
    {
        $this->attribute->create($request);
        return redirect('admin/attributes-value');
    }

    public function show($slug)
    {
        //
    }


    public function edit($slug)
    {
        $attributes=AttributeGroup::all();
        $attvalue=$this->attribute->findBySlug($slug);
        return view('admin.attributes-value.edit',compact(['attributes','attvalue']));
    }


    public function update(Request $request, $slug)
    {
        $validator = $this->attribute->validator($request,$slug);

        if ($validator->fails()) {
            return redirect('admin/attributes-value/'.$slug.'/edit')
                ->withErrors($validator)->withInput();
        }

        $this->attribute->update($slug,$request);

        return redirect('admin/attributes-value');
    }


    public function destroy($slug)
    {
        $attDelete=$this->attribute->findBySlug($slug);
        $attDelete->delete();
    }

    public function getAttGroup($id)
    {
        $attribute = $this->attGroup->findById($id);

        $response = [
            'attributeName'=>$attribute
        ];

        return response()->json($response , 200);
    }

    public function productValues()
    {
        $products =  $this->attribute->showProductAttribute(10);
        return view('admin.products.product-attribute',compact('products'));
    }
    public function getAttValuess()
    {
        $attributesValue=$this->attribute->AllWithByPaginate(7);
        $responce = [
            'attributesValue' => $attributesValue
        ];

        return response()->json($responce, 200);
    }

    public function search_attValue($value)
    {
        $attributesValue = AttributeValue::with('attGroup')
            ->whereHas('attGroup',function ($q) use ($value) {
                $q->where('title', 'like', '%'.$value.'%');
            })
            ->orWhere('title', 'like', '%'.$value.'%')
            ->paginate(7);

        $responce = [
            'attributesValue' => $attributesValue
        ];

        return response()->json($responce, 200);
    }


}

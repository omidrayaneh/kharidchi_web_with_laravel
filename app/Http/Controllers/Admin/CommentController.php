<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\Product;
use App\Repository\Eloquent\CommentRepository;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    protected $comments;

    public function __construct(CommentRepository $comments)
    {
        $this->comments = $comments;
    }

    public function index()
    {
        $comments = $this->comments->allWithPaginate(10);
        return view('admin.comments.index', compact(['comments']));
    }
    public function read($id)
    {
        $comment= $this->comments->findById($id);
        $comment->read = 1;
         $comment->save();
    }

    public function create(Request $request,$sku)
    {
        $this->comments->create($request,$sku);
        return back();
    }


    public function store(Request $request)
    {
        //
    }

    public function show(Product $product)
    {
        $order =Order::with('items')->where('user_id',auth()->id())->whereHas('items',function ($q)use($product){
            return $q->where('product_id',$product->id);
        })->get();
        if (count($order)!= 0){
            return view('product.comment', compact(['product']));
        }
        alert('خطا', 'برای ثبت نظر، قبلا باید کالا خریداری شده باشد','error');
        return back();

    }


    public function edit($id)
    {
        $comment= $this->comments->findById($id);
        if ($comment->status == 0){
            toast('با موفقیت منتشر  شد', 'success');
            $comment->status =1;
        }
        else{
            toast('تایید نشد', 'success');
            $comment->status =0;
        }
        $comment->accepted_by=auth()->id();
        $comment->save();
        return back();
    }


    public function update(Request $request, $id)
    {
       return $request;
    }

    public function destroy($id)
    {
        $comment = $this->comments->findById($id);
        $comment->delete();
    }
    public function recommend(Request $request,$id)
    {
        $comment = $this->comments->findById($id);
        if ($request->recommend == 'yes')
            $comment->recommend ++;
        else if ($request->recommend == 'no')
            $comment->notrecommend ++;
        $comment->save();
        toast('نظر شما با موفقیت ثبت شد', 'success');
        return back();
    }
}

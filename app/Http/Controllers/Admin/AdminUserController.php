<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUser;
use App\Repository\Eloquent\UserRepository;
use App\User;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{

    protected $users;

    public function __construct(UserRepository $users)
    {
       $this->users=$users;
    }
    public function index()
    {
        $users=$this->users->allWithPaginate(7);
       return view('admin.users.index',compact(['users']));
    }

    public function create()
    {
        return view('admin.users.create');
    }


    public function store(StoreUser $request)
    {
       $this->users->create($request);
        return redirect('admin/users');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $user=$this->users->findById($id);
        return view('admin.users.edit',compact(['user']));
    }


    public function update(Request $request, $id)
    {
        $validator = $this->users->validator($request, $id);
        $validator->fails();
        if ($validator->fails()) {
            return redirect('admin/users/' . $id . '/edit')
                ->withErrors($validator)->withInput();
        }

        $this->users->update($id, $request);
        return redirect('admin/users'.$_COOKIE['pageurl']);
    }


    public function destroy($id)
    {
        $user=$this->users->findById($id);
        $user->delete();
    }
}

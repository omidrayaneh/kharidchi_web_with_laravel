<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCost;
use App\Repository\Eloquent\CostRepository;
use Illuminate\Http\Request;

class CostController extends Controller
{

    /**
     * @var CostRepository
     */
    private $cost;

    public function __construct(CostRepository $cost)
    {
        $this->cost = $cost;
    }

    /**
     * Display a listing of the resource.
     *
     * @return |\Illuminate\Http\Response
     */
    public function index()
    {
        $costs = $this->cost->allWithPaginate(10);
        return view('admin.costs.index',compact(['costs']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return |\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.costs.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  |\Illuminate\Http\Request  $request
     * @return |\Illuminate\Http\Response
     */
    public function store(StoreCost $request)
    {
        $this->cost->create($request);
        return redirect('admin/costs');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return |Illuminate\Http\Response
     */
    public function edit($id)
    {
        $costs = $this->cost->findById($id);
        return view('admin.costs.edit',compact(['costs']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  |\Illuminate\Http\Request  $request
     * @param  int  $id
     * @return |\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->cost->validator($request,$id);
        if ($validator->fails()) {
            return redirect('admin/costs/'.$id.'/edit')
                ->withErrors($validator)->withInput();
        }
        $this->cost->update($id,$request);
        return redirect('admin/costs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cost= $this->cost->findById($id);
        $cost->delete();
    }
}

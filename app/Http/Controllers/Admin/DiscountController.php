<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $products = Product::all();
        return view('admin.discounts.index',compact(['categories','products']));
    }

    public function storeCategories(Request $request)
    {
        foreach($request->categories as $category){
             $products = Product::whereHas('categories',function($query) use ($category) {
                $query->where('category_id',$category);
            })->get();
            foreach($products as $product){
                $product->discount = $request->discount;
                $product->save();
            }
        }

        return redirect('admin/products');

    }

    public function storeProducts(Request $request)
    {
        foreach($request->products as $productId){
            $product = Product::findOrFail($productId);
            $product->discount = $request->discount;
            $product->save();
        }
        return redirect('admin/products');
    }

    public function storeIncreaseCategories(Request $request)
    {//افزایش
        foreach($request->categories as $category){
            $products = Product::whereHas('categories',function($query) use ($category) {
                $query->where('category_id',$category);
            })->get();
            foreach($products as $product){
                $price =$product->price + round($product->price * $request->increase /100);
                $product->price = round($price/100)*100;
                $product->save();
            }
        }
        return redirect('admin/products');
    }
}

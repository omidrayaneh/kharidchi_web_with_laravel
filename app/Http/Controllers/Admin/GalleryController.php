<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Photo;
use App\Repository\Eloquent\GalleryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    protected $gallery;

    /**
     * GalleryController constructor.
     * @param $gallery
     */
    public function __construct(GalleryRepository $gallery)
    {
        $this->gallery = $gallery;
    }


    public function index()
    {
        $photos = $this->gallery->allWithPaginate(10);
        return view('admin.galleries.index', compact(['photos']));
    }


    public function create()
    {
        return view('admin.galleries.create');
    }


    public function store(Request $request)
    {
        $photoId = $this->gallery->create($request);
        response()->json([
            'photo_id' => $photoId
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $photo = $this->gallery->findById($id);
        return view('admin.galleries.edit',compact(['photo']));
    }


    public function update(Request $request, $id)
    {
       // $validator = $this->gallery->validator($request,$id);

//        if ($validator->fails()) {
//            return redirect('admin/galleries/'.$id.'/edit')
//                ->withErrors($validator)->withInput();
//        }

        $this->gallery->update($id,$request);
        return redirect('admin/galleries');
    }

    public function update_photo(Request $request, $id)
    {
         $photo = $this->gallery->findById($id);

         $image_path ='/home/mobayada/public_html'.$photo->path;
        unlink($image_path);

        $uploadedFile = $request->file('file');
        $filename = time().$uploadedFile->getClientOriginalName();
        $original_name = $uploadedFile->getClientOriginalName();

        Storage::disk('local')->putFileAs(
            'public/photos', $uploadedFile, $filename
        );

        $photo->original_name = $original_name;
        $photo->path = $filename;
        $photo->save();

    }


    public function destroy($id)
    {
        $photo = $this->gallery->findById($id);
        $photo->delete();
       // $image_path = public_path().'/'.$photo->path;
       $image_path ='/home/mobayada/public_html'.$photo->path;

        unlink($image_path);
        return response(['success'=>'حذف شد',200]);
    }
}

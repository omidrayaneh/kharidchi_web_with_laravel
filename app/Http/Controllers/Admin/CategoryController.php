<?php

namespace App\Http\Controllers\Admin;


use App\AttributeGroup;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAttCat;
use App\Http\Requests\StoreCategory;
use App\Repository\Eloquent\AttgroupRepository;
use App\Repository\Eloquent\CategoryRepository;
use Illuminate\Http\Request;
use function Sodium\compare;

class CategoryController extends Controller
{

    protected $category;
    protected $attributes;

    public function __construct(CategoryRepository $category, AttgroupRepository $attributes)
    {
        $this->category = $category;
        $this->attributes = $attributes;
    }

    public function index()
    {
        $categories = $this->category->allByPaginate(5);
        return view('admin.categories.index', compact(['categories']));
    }

    public function indexSetting($slug)
    {
        $attGroups = $this->attributes->all();
        $category = $this->category->findBySlug($slug);
        return view('admin.categories.settings', compact(['category', 'attGroups']));
    }

    public function saveSetting(StoreAttCat $request,$slug)
    {
        $this->category->saveSetting($request,$slug);
        return redirect()->to('/admin/categories');
    }

    public function create()
    {

        $categories = $this->category->all();
        return view('admin.categories.create', compact(['categories']));
    }


    public function store(StoreCategory $request)
    {
        $this->category->create($request);
        return redirect('admin/categories');
    }


    public function show($id)
    {
        //
    }


    public function edit($slug)
    {
        $categories = $this->category->all();
        $category = $this->category->findBySlug($slug);
        return view('admin.categories.edit', compact(['categories', 'category']));
    }


    public function update(Request $request, $slug)
    {
        $validator = $this->category->validator($request, $slug);

        if ($validator->fails()) {
            return redirect('admin/categories/' . $slug . '/edit')
                ->withErrors($validator)->withInput();
        }

        $this->category->update($slug, $request);
        return redirect('admin/categories');
    }


    public function destroy($slug)
    {
        $this->category->delete($slug);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function apiCategories()
    {
        $categories = $this->category->all();
        $responce=[
            'categories'=>$categories
        ];
        return response()->json($responce,200);
    }

    public function apiAttributes(Request $request)
    {
       $categories= $request->all();
       $attributes=AttributeGroup::with('attValues','categories')
           ->whereHas('categories',function ($q) use ($categories){
               $q->whereIn('categories.id',$categories);
           })->get();
        $responce=[
            'attributes'=>$attributes
        ];

        return response()->json($responce,200);
    }

}

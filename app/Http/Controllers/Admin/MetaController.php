<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMeta;
use App\Repository\Eloquent\MetaRepository;
use Illuminate\Http\Request;

class MetaController extends Controller
{


    private $meta;

    public function __construct(MetaRepository $meta)
    {

        $this->meta = $meta;
    }

    public function index()
    {
        $metas = $this->meta->allPaginate(10);
        return view('admin.metas.index',compact(['metas']));
    }


    public function create()
    {
        return view('admin.metas.create');
    }

    public function store(StoreMeta $request)
    {
        $this->meta->create($request);
        return redirect('admin/metas');
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $meta = $this->meta->findById($id);
        return view('admin.metas.edit',compact(['meta']));
    }


    public function update(Request $request, $id)
    {

        $validator = $this->meta->validator($request,$id);
        if ($validator->fails()) {
            return redirect('admin/metas/'.$id.'/edit')
                ->withErrors($validator)->withInput();
        }
        $this->meta->update($id,$request);
        return redirect('admin/metas');
    }

    public function destroy($id)
    {
        $meta = $this->meta->findById($id);
        $meta->delete();
    }
}

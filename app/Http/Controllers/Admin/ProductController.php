<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProduct;
use App\Product;
use App\Repository\Eloquent\AttvalueRepository;
use App\Repository\Eloquent\BrandRepository;
use App\Repository\Eloquent\CategoryRepository;
use App\Repository\Eloquent\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ProductController extends Controller
{

    protected $category, $products, $brand;
    /**
     * @var AttvalueRepository
     */

    public function __construct(ProductRepository $products, BrandRepository $brand
        , CategoryRepository $category)
    {
        $this->category = $category;
        $this->brand = $brand;
        $this->products = $products;
    }

    public function index()
    {
        $products = $this->products->allWithPhotoAndPaginate(7);
        return view('admin.products.index', compact(['products']));
    }


    public function create()
    {
        $brands = $this->brand->all();
        $categories = $this->category->all();
        return view('admin.products.create', compact(['brands', 'categories']));
    }


    public function store(StoreProduct $request)
    {
        $this->products->create($request);
        return redirect('admin/products');
    }


    public function show($id)
    {
        //
    }


    public function edit($sku)
    {
        $brands = $this->brand->all();
        $categories = $this->category->all();
        $product = $this->products->findBySkuWithAllRelations($sku);
        $product->users()->wherePivot('user_id',  Auth::id())->wherePivot('type',1)->detach();
//        $user->lastProduct()->attach($lastVisitedProduct->id,['type'=>0]);
        return view('admin.products.edit', compact(['product', 'brands', 'categories']));
    }


    public function update(Request $request, $sku)
    {
        $validator = $this->products->validator($request, $sku);
        $validator->fails();
        if ($validator->fails()) {
            return redirect('admin/products/' . $sku . '/edit')
                ->withErrors($validator)->withInput();
        }

        $this->products->update($sku, $request);
       return redirect('admin/products');
    }


    public function destroy($slug)
    {
        $product = $this->products->findBySlug($slug);
        foreach ($product->photos as $photo){
            $image_path = public_path().'/'.$photo->path;
            unlink($image_path);
        }
        $product->delete();


    }

    public function getProducts()
    {
        $products = $this->products->allWithPhotoAndPaginate(7);

        $responce=[
            'products'=>$products
        ];

        return response()->json($responce,200);
    }

    public function search_products($value)
    {

        $products = Product::where('title', 'like', '%'.$value.'%')->orWhere('price', 'like', '%'.$value.'%')->paginate(7);
        $responce = [
            'products' => $products
        ];

        return response()->json($responce, 200);

    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBrand;
use App\Repository\Eloquent\BrandRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Mockery\Exception;

class BrandController extends Controller
{

    protected $brands;


    public function __construct(BrandRepository $brands)
    {
        $this->brands = $brands;
    }


    public function index()
    {
        $brands=$this->brands->allWithPhotoAndPaginate(7);

        return view('admin.brands.index',compact(['brands']));
    }


    public function create()
    {
        return view('admin.brands.create');
    }


    public function store(StoreBrand $request)
    {

        $this->brands->create($request);
        return redirect('admin/brands');
    }


    public function show($slug)
    {
        //
    }


    public function edit($slug)
    {
        $brand=$this->brands->findBySlug($slug);
        return view('admin.brands.edit',compact(['brand']));
    }


    public function update(Request $request, $slug)
    {
        $validator = $this->brands->validator($request,$slug);

        if ($validator->fails()) {
            return redirect('admin/brands/'.$slug.'/edit')
                ->withErrors($validator)->withInput();
        }

        $this->brands->update($slug,$request);

        return redirect('admin/brands');
    }


    public function destroy($slug)
    {
        $brand=$this->brands->findBySlugWithPhoto($slug);
        $brand->delete();
        $image_path = public_path().'/'.$brand->photos[0]->path;
        unlink($image_path);
    }
}

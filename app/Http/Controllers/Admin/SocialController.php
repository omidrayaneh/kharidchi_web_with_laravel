<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return |\Illuminate\Http\Response
     */
    public function index()
    {
        $socials =Social::where('status',1)->paginate(7);
        return view('admin.socials.index',compact(['socials']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return |\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.socials.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        DB::table('socials')->where([['status', 1]])->update(['status' => 0]);

        $social= new Social();
        $social->telegram = $request->get('telegram');
        $social->instagram = $request->get('instagram');
        $social->linkedin = $request->get('linkedin');
        $social->twitter = $request->get('twitter');
        $social->user_id = auth()->id();

        if ($request->status == 'on')
            $social->status =1;
        else
            $social->status =0;

        $social->save();

        toast('با موفقیت ایجاد  شد', 'success');
        return redirect('admin/socials');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return |\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $social = Social::findOrFail($id);
        return view('admin.socials.edit',compact(['social']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        DB::table('socials')->where([['status', 1]])->update(['status' => 0]);

        $social = Social::findOrFail($id);
        $social->telegram = $request->get('telegram');
        $social->instagram = $request->get('instagram');
        $social->linkedin = $request->get('linkedin');
        $social->twitter = $request->get('twitter');
        $social->user_id = auth()->id();

        if ($request->status == 'on')
            $social->status =1;
        else
            $social->status =0;

        $social->save();

        toast('با موفقیت بروز  شد', 'success');
        return redirect('admin/socials');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $social = Social::findOrFail($id);
        $social->delete();

    }
}

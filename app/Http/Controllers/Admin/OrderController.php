<?php

namespace App\Http\Controllers\Admin;

use App\Address;
use App\AttributeGroup;
use App\AttributeValue;
use App\Attvalue;
use App\City;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\Product;
use App\Province;
use App\Repository\Eloquent\CostRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{

    /**
     * @var CostRepository
     */
    private $cost;

    public function __construct(CostRepository $cost)
    {
        $this->cost = $cost;
    }

    public function index()
    {
       $orders= Order::with('items','user','payments')->orderBy('id','desc')->paginate(10);
        return view('admin.orders.index',compact(['orders']));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {

    }


    public function edit($id)
    {
        $order = Order::with('items','payments')->findOrFail($id);
        $total=0;
        foreach ($order->items as $item){
            $total +=$item->qty * ($item->price-($item->price * $item->discount)/100);
        }
        $cost = $this->cost->enabled();

        $orders = DB::table('products')
            ->Join('order_items', 'order_items.product_id', '=', 'products.id')
           ->Join('orders', 'orders.id', '=', 'order_items.order_id')
           ->Join('payments', 'payments.order_id', '=', 'orders.id')
           ->Join('users', 'orders.user_id', '=', 'users.id')
           ->Join('addresses', 'addresses.user_id', '=', 'users.id')
           ->Join('cities', 'cities.id', '=', 'addresses.city_id')
           ->Join('provinces', 'provinces.id', '=', 'addresses.province_id')
           ->leftJoin('attvalues', 'attvalues.product_id', '=', 'products.id')//
           ->leftJoin('attribute_values', 'attribute_values.id', '=', 'order_items.attValue')//
           ->leftJoin('attribute_groups', 'attribute_groups.id', '=', 'attribute_values.attributeGroup_id')//
           ->leftJoin('attributeValue_product', 'attributeValue_product.attributeValue_id', '=', 'attribute_values.id')//
           ->where('orders.id',$id)
           ->where('addresses.status',1)
            ->select(
                'attribute_values.title as attValue',
                        'attribute_groups.title as attGroup',
                        'products.title as product',
                        'addresses.address',
                        'addresses.phone',
                        'addresses.plaque',
                        'addresses.post_code',
                        'addresses.name as address_name',
                        'cities.name as city',
                        'provinces.name as province',
                        'users.name as username',
                        'users.family',
                        'users.mobile',
                        'users.email',
                        'orders.id as orderId',
                        'orders.amount',
                        'orders.approve',
                        'order_items.price',
                        'order_items.discount',
                        'order_items.qty',
                        'order_items.created_at',
                        'payments.authority',
                        'payments.created_at as pay_day'
            )
           ->groupBy('order_items.id')
           ->get();
//        if (count($orders)==0){
//            $orders = DB::table('products')
//                ->Join('order_items', 'order_items.product_id', '=', 'products.id')
//                ->Join('orders', 'orders.id', '=', 'order_items.order_id')
//                ->Join('payments', 'payments.order_id', '=', 'orders.id')
//                ->Join('users', 'orders.user_id', '=', 'users.id')
//                ->Join('addresses', 'addresses.user_id', '=', 'users.id')
//                ->Join('cities', 'cities.id', '=', 'addresses.city_id')
//                ->Join('provinces', 'provinces.id', '=', 'addresses.province_id')
//             //   ->Join('attvalues', 'attvalues.product_id', '=', 'products.id')
//              //  ->join('attribute_values', 'attribute_values.id', '=', 'order_items.attValue')
//              //  ->join('attribute_groups', 'attribute_groups.id', '=', 'attribute_values.attributeGroup_id')
//               // ->Join('attributevalue_product', 'attributevalue_product.attributeValue_id', '=', 'attribute_values.id')
//                ->where('orders.id',$id)
//                ->where('addresses.status',1)
//                ->select(
//                 //   'attribute_values.title as attValue',
//                 //   'attribute_groups.title as attGroup',
//                    'products.title as product',
//                    'addresses.address',
//                    'addresses.phone',
//                    'addresses.plaque',
//                    'addresses.post_code',
//                    'addresses.name as address_name',
//                    'cities.name as city',
//                    'provinces.name as province',
//                    'users.name as username',
//                    'users.family',
//                    'users.mobile',
//                    'users.email',
//                    'orders.id as orderId',
//                    'orders.amount',
//                    'orders.approve',
//                    'order_items.price',
//                    'order_items.discount',
//                    'order_items.qty',
//                    'order_items.created_at',
//                    'payments.authority',
//                    'payments.created_at as pay_day'
//                )
//                ->groupBy('order_items.id')
//                ->get();

      //  }
      //  $attValues = Attvalue::all()
//        $order=Order::with('items','user','payments')->whereHas('payments',function($q){
//            $q->where('status','OK' );
//      })->findOrFail($id);
      // $userAddresses=Address::where([['user_id',$order->user_id],['status',1]])->first();
     //   $attValue = AttributeValue::findOrFail(7);
      //  $attGroup = AttributeGroup::findOrFail(1);
      //  $city=City::findOrFail($userAddresses->city_id);
       // $province=Province::findOrFail($userAddresses->province_id);
       return view('admin.orders.edit',compact(['orders','total']));
    }


    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);


        $orderItems = OrderItem::where('order_id',$id)->get();
        if ( $order->approve ==-1) {
            $order->approve =1;
        }
        else if($order->approve ==1){
            $order->approve =0;
            foreach ($orderItems as $item) {
                $product = null;
                $product = Product::findOrFail($item->product_id);
                $product->qty += $item->qty;
                $product->save();
            }
        }
        else if($order->approve ==0){
            toast('فقط یک بار میتوانید سفرش را فعال کنید', 'error');
            return back();
//            $order->approve =1;
//            foreach ($orderItems as $item) {
//                $product = null;
//                $product = Product::findOrFail($item->product_id);
//                $product->qty -= $item->qty;
//                $product->save();
//            }
        }
        $order->save();
        toast('با موفقیت بروز  شد', 'success');

        return redirect('admin/orders');

    }


    public function destroy($id)
    {
        //
    }

    public function markAsRead(Request $request)
    {
       auth()->user()->unreadNotifications->where('id', $request->id)->markAsRead();
    }

    public function readOrder(Request $request)
    {
        $order = Order::findOrFail($request->id);
        if ($order->approve ==null || $order->approve ==0) {
            $order->approve =1;
        }
        else{
            $order->approve =0;
        }
        $order->save();
        toast('با موفقیت بروز  شد', 'success');

        return redirect('admin/orders');
    }
}

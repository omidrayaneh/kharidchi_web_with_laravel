<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBrand;
use App\Http\Requests\StoreCoupon;
use App\Repository\Eloquent\BrandRepository;
use App\Repository\Eloquent\CouponRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class CouponController extends Controller
{

    protected $coupon;


    public function __construct(CouponRepository $coupon)
    {
        $this->coupon = $coupon;
    }


    public function index()
    {
        $coupons=$this->coupon->allWithPaginate(7);
        return view('admin.coupons.index',compact(['coupons']));
    }


    public function create()
    {
        return view('admin.coupons.create');
    }


    public function store(StoreCoupon $request)
    {
        $this->coupon->create($request);
        return redirect('admin/coupons');
    }


    public function show($slug)
    {
        //
    }


    public function edit($slug)
    {
        $coupon=$this->coupon->findBySlug($slug);
        return view('admin.coupons.edit',compact(['coupon']));
    }


    public function update(Request $request, $slug)
    {
        $validator = $this->coupon->validator($request,$slug);
        if ($validator->fails()) {
            return redirect('admin/coupons/'.$slug.'/edit')
                ->withErrors($validator)->withInput();
        }
        $this->coupon->update($slug,$request);
        return redirect('admin/coupons');
    }

    public function destroy($slug)
    {
        $coupon=$this->coupon->findBySlug($slug);
        $coupon->delete();
    }
    public function addCoupons(Request $request)
    {
        $coupon=$this->coupon->findByCode($request->code);
        if (!$coupon){
            alert('خطا','کوپن وارد شده اشتباه است','error');
            return back();
        }
        $check=Auth::user()->whereHas('coupons',function ($q) use ($request){
            $q->where('code',$request->code);
        })->exists();
        if (!$check){
           $user= Auth::user();
           $user->coupons()->attach($coupon->id);
            Session::flash('coupon', $coupon );
            alert('خطا','در صورت خروج از این صفحه امکان استفاده از گوپن وجود نخواهد داشت','info');
            return back();
        }
        alert('خطا','قبلا از این کوپن استفاده کرده اید','error');
        return back();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PhotoController extends Controller
{
    public function store(Request $request)
    {

        if ($request->hasFile('upload')){
            $original_name = $request->file('upload')->getClientOriginalName();
            $filename = pathinfo($original_name,PATHINFO_FILENAME);
            $extention = $request->file('upload')->getClientOriginalExtension();
            $filename = $filename.'_'.time().'.'.$extention;
            // $request->file('upload')->move('https://mobayadak.com/storage/',$filename);
        Storage::disk('local')->putFileAs(
            'public/photos',  $request->file('upload'), $filename
        );
    

            $CKeditor = $request->input('CKEditorFuncNum');
            $url = asset('/storage/'.$filename);
            $msg = 'Image Upload Successfully';


            $photo = new Photo();
            $photo->original_name = $original_name;
            $photo->path = $filename;
            $photo->type = 0;
            $photo->user_id = auth()->id();
            $photo->save();

            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKeditor ,`$url` , `$msg`)</script>";
            @header('Content-type: text/html;charset=utf-8');
            echo $response;
        }

    }


    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);
        $image_path = public_path().$photo->path;
        unlink($image_path);
        return response(['success'=>'حذف شد',200]);
    }
    public function upload(Request $request)
    {
        $uploadedFile = $request->file('file');
        $type=null;
        if (isset($request->type))
            $type = $request->type;
        $filename = time().$uploadedFile->getClientOriginalName();
        $original_name = $uploadedFile->getClientOriginalName();

        Storage::disk('local')->putFileAs(
            'public/photos', $uploadedFile, $filename
        );

        $photo = new Photo();
        $photo->original_name = $original_name;
        $photo->path = $filename;
        if (isset($request->type))
          $photo->type = $type;
        $photo->user_id = auth()->user()->id;
        $photo->save();

        return response()->json([
            'photo_id' => $photo->id
        ]);
    }



}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreText;
use App\Repository\Eloquent\TextRepository;
use Illuminate\Http\Request;

class TextController extends Controller
{

    /**
     * @var TextRepository
     */
    private $text;

    public function __construct(TextRepository $text)
    {
        $this->text = $text;
    }

    /**
     * Display a listing of the resource.
     *
     * @return |\Illuminate\Http\Response
     */
    public function index()
    {
        $texts = $this->text->allWithPaginate(10);
        return view('admin.texts.index',compact(['texts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return |\Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.texts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  :\Illuminate\Http\Request  $request
     * @return :\Illuminate\Http\Response
     */
    public function store(StoreText $request)
    {
        $this->text->create($request);
        return redirect('admin/texts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return :\Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return ;\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $text = $this->text->findById($id);
        return view('admin.texts.edit',compact(['text']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ;\Illuminate\Http\Request  $request
     * @param  int  $id
     * @return ;\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->text->validator($request,$id);
        if ($validator->fails()) {
            return redirect('admin/texts/'.$id.'/edit')
                ->withErrors($validator)->withInput();
        }
        $this->text->update($id,$request);
        return redirect('admin/texts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return ;\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $text= $this->text->findById($id);
        $text->delete();
    }
}

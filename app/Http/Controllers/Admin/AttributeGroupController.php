<?php

namespace App\Http\Controllers\Admin;

use App\AttributeGroup;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAttGroup;
use App\Repository\Eloquent\AttgroupRepository;
use Illuminate\Http\Request;

class AttributeGroupController extends Controller
{

    protected $attribute;

   public function __construct(AttgroupRepository $attribute)
   {
       $this->attribute=$attribute;
   }

    public function index()
    {
        $attributes=$this->attribute->allByPaginate(7);
        return view('admin.attributes.index',compact(['attributes']));
    }


    public function create()
    {
        return view('admin.attributes.create');
    }


    public function store(StoreAttGroup $request)
    {
        $this->attribute->create($request);
        return redirect('admin/attributes');
    }


    public function show($slug)
    {
        //
    }


    public function edit($slug)
    {
        $attribute=$this->attribute->findBySlug($slug);
        return view('admin.attributes.edit',compact('attribute'));
    }


    public function update(Request $request, $slug)
    {
        $validator = $this->attribute->validator($request,$slug);

        if ($validator->fails()) {
            return redirect('admin/attributes/'.$slug.'/edit')
                ->withErrors($validator)->withInput();
        }

        $this->attribute->update($slug,$request);

        return redirect('admin/attributes');

    }


    public function destroy($slug)
    {
        $attDelete=$this->attribute->findBySlug($slug);
        $attDelete->delete();

    }

    public function getAttGroups()
    {
        $attributes=$this->attribute->allByPaginate(7);
        $responce = [
            'attributes' => $attributes
        ];

        return response()->json($responce, 200);
    }

    public function search_attGroup($value)
    {
        $attributes = AttributeGroup::where('title', 'like', '%'.$value.'%')->paginate(7);
        $responce = [
            'attributes' => $attributes
        ];

        return response()->json($responce, 200);
    }


}

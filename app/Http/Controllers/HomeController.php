<?php

namespace App\Http\Controllers;

use App\Repository\Eloquent\TextRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * @var TextRepository
     */
    private $text;

    public function __construct(TextRepository $text)
    {
        $this->text = $text;
    }

    public function privacy()
    {
        $privacy = $this->text->where('privacy');
        if (empty($privacy))
            abort(404);
        return view('texts.privacy',compact(['privacy']));
    }
    public function teams()
    {
        $teams = $this->text->where('teams');
        if (empty($teams))
             abort(404);
        return view('texts.teams',compact(['teams']));
    }
    public function question()
    {
        $questions = $this->text->where('question');
        if (empty($questions))
            abort(404);
        return view('texts.question',compact(['questions']));
    }
    public function about()
    {
        $about= $this->text->where('about');
        if (empty($about))
            abort(404);
        return view('texts.about',compact(['about']));
    }
    public function contact()
    {
        $contact= $this->text->where('contact');
        if (empty($contact))
            abort(404);
        return view('texts.contact',compact(['contact']));
    }
    public function careers()
    {
        $careers= $this->text->where('careers');
        if (empty($careers))
            abort(404);
        return view('texts.careers',compact(['careers']));
    }
    public function returning()
    {
        $returning= $this->text->where('returning');
        if (empty($returning))
            abort(404);
        return view('texts.returning',compact(['returning']));
    }
    public function register()
    {
        $register= $this->text->where('register-product');
        if (empty($register))
            abort(404);
        return view('texts.register',compact(['register']));
    }
    public function send()
    {
        $send= $this->text->where('send-product');
        if (empty($send))
            abort(404);
        return view('texts.send',compact(['send']));
    }
    public function payment()
    {
        $payment= $this->text->where('payment');
        if (empty($payment))
            abort(404);
        return view('texts.payment',compact(['payment']));
    }

    public function social()
    {
        return view('');
    }
}

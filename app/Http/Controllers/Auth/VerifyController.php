<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Otp;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerifyController extends Controller
{

    public function verify()
    {
       return view('verify');
    }

    public function register(Request $request)
    {
        return $request;
    }

}

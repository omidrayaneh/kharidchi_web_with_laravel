<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function username()
    {
        return 'mobile';
    }
    //check user is active
    protected function credentials(\Illuminate\Http\Request $request)
    {
        return ['mobile' => $request->{$this->username()}, 'password' => $request->password, 'status' => 1];
    }
    protected function authenticated(Request $request, $user)
    {
        alert('',$user->name.'، خوش آمدید ','success');

    }
    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            alert('','نام کاربری یا رمز عبور اشتباه است','error')
        ]);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile' => 'required|unique:users',
            'email' => 'unique:users',
            'password'=>'required|min:8',
        ];

    }

    public function messages()
    {
        return [
            'mobile.required'=>'موبایل کاربر را وارد کنید',
            'mobile.unique'=>'شماره موبایل تکراری است',
            'email.unique'=>'ایمیل تکرای است',
            'email.required'=>'ایمیل را وارد کنید',
            'password.required'=>'رمز عبور را وارد کنید',
            'password.min'=>'رمز عبور کمتر از 8 کاراکتر است',
        ];
    }

}

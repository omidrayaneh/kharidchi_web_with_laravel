<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAttCat extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           // 'attributes_id' => 'required',
        ];

    }

    public function messages()
    {
        return [
           // 'attributes_id.required'=>'حداقل یک ویژگی را انتخاب کنید',
        ];
    }

}

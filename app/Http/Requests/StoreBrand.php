<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBrand extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|unique:brands',
            'description' => 'required|min:15',
                'photo_id.*'=> 'required'

        ];

    }

    public function messages()
    {
        return [
            'title.required'=>'نام برند را وارد کنید',
            'title.unique'=>'نام برند قبلا ثبت شده است',
            'title.min'=>'نام برند کمتر از سه کاراکتر می باشد',
            'description.required'=>'توضیحات برند را وارد کنید',
            'description.min'=>'توضیحات برند کمتر از 15 کاراکتر می باشد',
            'photo_id.*' => 'تصویر برند را انتخاب کنید'

        ];
    }

}

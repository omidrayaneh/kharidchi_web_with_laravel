<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMeta extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|unique:metas',
            'description' => 'required',
            'keyword' => 'required',

        ];

    }

    public function messages()
    {
        return [
            'title.required'=>'عنوان متا را وارد کنید',
            'title.unique'=>'عنوان متا قبلا ثبت شده است',
            'title.min'=>'عنوان متا کمتر از سه کاراکتر می باشد',
            'description.required'=>'توضیحات متا را وارد کنید',
            'keyword.required'=>'کلمات کلیدی متا را وارد کنید',
        ];
    }

}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|unique:categories',
        ];

    }

    public function messages()
    {
        return [
            'title.required'=>'نام دسته بندی را وارد کنید',
            'title.unique'=>'نام دسته بندی قبلا ثبت شده است',
            'title.min'=>'نام دسته بندی کمتر از سه کاراکتر می باشد',
        ];
    }

}

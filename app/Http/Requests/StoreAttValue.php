<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAttValue extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:attribute_values',
        ];

    }

    public function messages()
    {
        return [
            'title.required'=>'نام مقدار ویژگی را وارد کنید',
            'title.unique'=>'نام مقدار ویژگی قبلا ثبت شده است',
        ];
    }

}

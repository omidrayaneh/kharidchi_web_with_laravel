<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCoupon extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|unique:coupons',
            'code' => 'required|min:2',
            'price' => 'required',
        ];

    }

    public function messages()
    {
        return [
            'title.required'=>'نام کوپن را وارد کنید',
            'title.unique'=>'نام کوپن قبلا ثبت شده است',
            'title.min'=>'نام کوپن کمتر از سه کاراکتر می باشد',
            'code.min'=>'کد کوپن کمتر از دو کاراکتر می باشد',
            'code.required'=>'کد کوپن را وارد کنید',
            'price.required'=>'قیمت کوپن را وارد کنید',

        ];
    }

}

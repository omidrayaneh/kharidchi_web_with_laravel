<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAttGroup extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:2|unique:attribute_groups',
        ];

    }

    public function messages()
    {
        return [
            'title.required'=>'نام ویژگی را وارد کنید',
            'title.unique'=>'نام ویژگی قبلا ثبت شده است',
            'title.min'=>'نام ویژگی کمتر از دو کاراکتر می باشد',
        ];
    }

}

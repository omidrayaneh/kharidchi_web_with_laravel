<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|unique:products',
            'price'=>'required',
            'qty'=>'required',
            'discount'=>'required',
            'categories.*'=>'required',
            'description' => 'required|min:15',
            'photo_id.*'=> 'required',
            'attributes.*'=> 'required',
        ];

    }

    public function messages()
    {
        return [
            'title.required'=>'نام محصول را وارد کنید',
            'title.min'=>'نام محصول کمتر از سه کاراکتر می باشد',
            'price.required'=>'قیمت محصول را وارد کنید',
            'qty.required'=>'تعداد محصول را وارد کنید',
            'discount.required'=>'تخفیف محصول را وارد کنید',
            'description.required'=>'توضیحات محصول را وارد کنید',
            'description.min'=>'توضیحات محصول کمتر از 15 کاراکتر می باشد',
            'categories.0.required'=>'حداقل یک دسته بندی را انتخاب کنید',
            'photo_id.0.required' => 'تصویر محصول را وارد کنید',
             'attributes.*'=> 'ویژگی را انتخاب کنید',
        ];
    }

}

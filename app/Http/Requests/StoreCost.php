<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|unique:costs',
            'price' => 'required|numeric',
            'free' => 'required|numeric',

        ];

    }

    public function messages()
    {
        return [
            'title.required' => 'عنوان هزینه  خالیست',
            'title.min' => 'مقدار هزینه کمتر از سه کاراکتر است',
            'title.unique' => 'این هزینه قبلا ثبت شده است',
            'price.required' => 'مبلغ هزینه خالیست',
            'price.numeric' => 'لطفا فقط عدد وارد کنید',
            'free.required' => 'مبلغ رایگان خالیست',
            'free.numeric' => 'لطفامبلغ رایگان، فقط عدد وارد کنید',
        ];
    }

}

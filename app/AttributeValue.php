<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function attGroup()
    {
      return  $this->belongsTo(AttributeGroup::class , 'attributeGroup_id');
    }
    public function products()
    {
        return $this->belongsToMany(Product::class,'attributeValue_product' ,'attributeValue_id','product_id');
    }
}

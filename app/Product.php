<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function photos()
    {
        return $this->belongsToMany(Photo::class);
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('type');
    }
    public function att_values()
    {
        return $this->belongsToMany(AttributeValue::class,'attributeValue_product','product_id','attributeValue_id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function favourite()
    {
        return $this->morphMany(Favorite::class, 'favourable');
    }
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function orderItems()
    {
       return $this->hasMany(OrderItem::class);
    }

    public function values()
    {
        return $this->hasMany(Attvalue::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $uploads = '/storage/';

    public function getPathAttribute($photo)
    {
        return $this->uploads . $photo;
    }
    public function brands()
    {
       return $this->belongsToMany(Brand::class);
    }

    public function products()
    {
       return $this->belongsToMany(Product::class);
    }
}

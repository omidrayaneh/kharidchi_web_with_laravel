<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'password','mobile','role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     * arsalan new push last
     * @var array
     */
    protected $casts = [
        'mobile_verified_at' => 'datetime',
    ];
    protected $guarded=[
        'role'
    ];



    public function otps()
    {
        return $this->hasMany(Otp::class);
    }

    public function categories()
    {
       return $this->hasMany(Category::class);
    }

    public function products()
    {
        return $this->belongsTo(Product::class);
    }

    public function lastProduct()
    {
        return $this->belongsToMany(Product::class)->withPivot('type');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    public function coupons()
    {
        return $this->belongsToMany(Coupon::class);
    }

    public function favorite()
    {
        return $this->hasMany(Favorite::class);
    }

    public function userFavorite()
    {
        return $this->favorite()->with('favourable');
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

}

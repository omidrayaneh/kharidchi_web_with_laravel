<?php

namespace App\Providers;


use App\Repository\AddressRepositoryInterface;
use App\Repository\AttGroupRepositoryInterface;
use App\Repository\AttValueRepositoryInterface;
use App\Repository\BrandRepositoryInterface;
use App\Repository\CategoryRepositoryInterface;
use App\Repository\CostRepositoryInterface;
use App\Repository\CouponRepositoryInterface;
use App\Repository\Eloquent\AddressRepository;
use App\Repository\Eloquent\AttgroupRepository;
use App\Repository\Eloquent\AttvalueRepository;
use App\Repository\Eloquent\BrandRepository;
use App\Repository\Eloquent\CategoryRepository;
use App\Repository\Eloquent\CostRepository;
use App\Repository\Eloquent\CouponRepository;
use App\Repository\Eloquent\GalleryRepository;
use App\Repository\Eloquent\MetaRepository;
use App\Repository\Eloquent\ProductRepository;
use App\Repository\Eloquent\TextRepository;
use App\Repository\Eloquent\UserRepository;
use App\Repository\GalleryRepositoryInterface;
use App\Repository\ProductRepositoryInterface;
use App\Repository\TextRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Translation\MetadataAwareInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {


    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
        $this->app->bind(AttGroupRepositoryInterface::class,AttGroupRepository::class);
        $this->app->bind(AttValueRepositoryInterface::class, AttValueRepository::class);
        $this->app->bind(BrandRepositoryInterface::class, BrandRepository::class);
        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);
        $this->app->bind(CouponRepositoryInterface::class, CouponRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(MetadataAwareInterface::class, MetaRepository::class);
        $this->app->bind(CostRepositoryInterface::class, CostRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(GalleryRepositoryInterface::class, GalleryRepository::class);
        $this->app->bind(AddressRepositoryInterface::class, AddressRepository::class);
        $this->app->bind(TextRepositoryInterface::class, TextRepository::class);
    }
}

<?php

namespace App\Providers;

use App\Category;
use App\Comment;
use App\Cost;
use App\Meta;
use App\Photo;
use App\Social;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.*',function($view){
            $comments = Comment::where('status',1)->get();
            $categories = Category::with('children','childrenRecursive')->get();
            $ads = Photo::where([['status',1],['place' , 'ads']] )->first();
            $logo= Photo::where([['status',1],['place' , 'logo']] )->first();
            $view->with('ads',$ads);
            $view->with('logo',$logo);
            $view->with('categories',$categories);
            $view->with('comments',$comments);
        });
        View::composer('*',function ($view){

        });
        View::composer('*',function ($view){
            $social =Social::where('status',1)->latest()->first();

            $cost = Cost::where('status',1)->latest()->first();
            $meta = Meta::where('status',1)->latest()->first();
            $logo= Photo::where([['status',1],['place' , 'logo']] )->first();
            $view->with('social',$social);
            $view->with('cost',$cost);
            $view->with('logo',$logo);
            $view->with('meta',$meta);
        });
        View::composer('admin.layouts.*',function($view){
            $comments = Comment::where('read',0)->get();
            $view->with('comments',$comments);
        });

    }
}

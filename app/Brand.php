<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function photos()
    {
       return $this->belongsToMany(Photo::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}

<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/********************  clear cache  ****************************************/
Route::get('/clear-cache', function() {
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

Auth::routes();


/********************  registration and login  ****************************************/

Route::post('register-user','Frontend\UserController@register')->name('register-user');

Route::get('/verify','Frontend\UserController@verify')->name('verify');
Route::post('/verify-user','Frontend\UserController@verification')->name('verify-user');

Route::get('forgot','Frontend\UserController@forgot')->name('forgot');
Route::post('forgot-password','Frontend\UserController@forgotPassword')->name('forgot-password');

Route::get('reset','Frontend\UserController@reset')->name('reset');
Route::post('reset-password','Frontend\UserController@changePassword')->name('reset-password');


/********************  api ****************************************/

Route::prefix('api/')->group(function () {
    Route::get('categories','Admin\CategoryController@apiCategories');
    Route::post('categories/attributes','Admin\CategoryController@apiAttributes');

    /********************   category search route ****************************************/

    Route::get('search/{sku}/{value}','Frontend\ProductController@productAllProducts');
    Route::get('sort-desc-products/{sku}/{value}','Frontend\ProductController@GetDescProducts');
    Route::get('sort-asc-products/{sku}/{value}','Frontend\ProductController@GetAscProducts');
    Route::get('sort-visit-products/{sku}/{value}','Frontend\ProductController@GetVisitedProducts');
    Route::get('sort-sell-products/{sku}/{value}','Frontend\ProductController@GetSellProducts');
    Route::get('sort-new-products/{sku}','Frontend\ProductController@GetNewProducts');


    /********************   sells products ****************************************/

    Route::get('get/sell-products/{value}','Frontend\ProductController@productSellData');
    Route::get('sort-sells-new-products/{value}','Frontend\ProductController@GetSellsNewProducts');
    Route::get('sort-sells-visited-products/{value}','Frontend\ProductController@GetSellsVisitedProducts');
    Route::get('sort-sells-ace-price-products/{value}','Frontend\ProductController@GetSellsAcePriceProducts');
    Route::get('sort-sells-desc-price-products/{value}','Frontend\ProductController@GetSellsDescPriceProducts');

    /********************   main  sort  new products route ****************************************/

    Route::get('get/new-products/{value}','Frontend\ProductController@productNewData');
    Route::get('sort-sell-product/{value}','Frontend\ProductController@GetNewSellProducts');
    Route::get('sort-visit-products/{value}','Frontend\ProductController@GetNewsVisitedProducts');
    Route::get('sort-ace-products/{value}','Frontend\ProductController@GetNewsAceProducts');
    Route::get('sort-desc-products/{value}','Frontend\ProductController@GetNewsDeceProducts');

    /********************  main search sort route  ****************************************/

    Route::get('search-all/{data}/{value}','Frontend\ProductController@searchAll');
    Route::get('search-price-desc/{data}/{value}','Frontend\ProductController@searchPriceDesc');
    Route::get('search-price-asc/{data}/{value}','Frontend\ProductController@searchPriceAsc');
    Route::get('search-visit/{data}/{value}','Frontend\ProductController@searchVisit');
    Route::get('search-sells/{data}/{value}','Frontend\ProductController@searchSells');

});



/********************  admin panel ****************************************/

Route::group(['middleware' => ['admin']], function () {
    Route::prefix('admin/')->group(function (){
        Route::get('dashboard', 'Admin\AdminController@index')->name('dashboard');
        Route::resource('categories', 'Admin\CategoryController');
        Route::resource('texts', 'Admin\TextController');
        Route::resource('costs', 'Admin\CostController');
        Route::resource('users', 'Admin\AdminUserController');
        Route::get('categories/{slug}/setting', 'Admin\CategoryController@indexSetting')->name('categories.indexSetting');
        Route::post('categories/{slug}/setting', 'Admin\CategoryController@saveSetting');
        Route::post('photos/create','Admin\PhotoController@store')->name('photos.store');
//        Route::post('photos/destroy/{id}','Admin\PhotoController@destroy')->name('photos.delete');

        Route::resource('attributes', 'Admin\AttributeGroupController');
        Route::resource('attributes-value', 'Admin\AttributeValueController');
        Route::get('attribute-group-name/{id}','Admin\AttributeValueController@getAttGroup')->name('attribute.group.name');
        Route::resource('brands', 'Admin\BrandController');
        Route::resource('metas', 'Admin\MetaController');
        Route::resource('products', 'Admin\ProductController');
        Route::get('product-attributes', 'Admin\AttributeValueController@productValues')->name('product.attribute');
        Route::resource('coupons', 'Admin\CouponController');
        Route::get('photos', 'Admin\PhotoController@upload');
        Route::post('photos/upload', 'Admin\PhotoController@upload')->name('photos.upload');
        Route::resource('comments','Admin\CommentController');
        Route::patch('comments/read/{id}','Admin\CommentController@read');
        Route::resource('orders','Admin\OrderController');
        Route::post('orders/markAsRead','Admin\OrderController@markAsRead')->name('orders.markAsRead');
        Route::resource('galleries','Admin\GalleryController');
        Route::patch('update/slider/{id}', 'Admin\GalleryController@update_photo')->name('slider.update');

        Route::delete('photos/{id}','Admin\PhotoController@destroy');
        Route::get('discounts' , 'Admin\DiscountController@index')->name('group.discount');
        Route::post('discounts/categories' , 'Admin\DiscountController@storeCategories')->name('discounts.categories');
        Route::post('increase/categories' , 'Admin\DiscountController@storeIncreaseCategories')->name('increase.categories');
        Route::post('discounts/products' , 'Admin\DiscountController@storeProducts')->name('discounts.products');
        Route::resource('socials','Admin\SocialController');

        // products api
        Route::get('all-products', 'Admin\ProductController@getProducts');
        Route::post('search/products/{value}' , 'Admin\ProductController@search_products');

        //attribute group api
        Route::get('all-att-group', 'Admin\AttributeGroupController@getAttGroups');
        Route::post('search/attribute-group/{value}' , 'Admin\AttributeGroupController@search_attGroup');

        //attribute value api
        Route::get('all-att-value', 'Admin\AttributeValueController@getAttValuess');
        Route::post('search/attribute-value/{value}' , 'Admin\AttributeValueController@search_attValue');


    });
});


/********************  user panel ****************************************/

Route::group(['middleware'=>'auth'], function() {
   // Route::get('/profile', 'Frontend\UserController@profile')->name('user.profile');
    Route::get('/profile','Frontend\ProfileController@profile')->name('profile');
    Route::get('/profile/additional-info','Frontend\ProfileController@profileAdditional')->name('additional-info');
    Route::post('/profile/additional-info','Frontend\ProfileController@updateProfile')->name('additional.info');
    Route::get('/profile/personal-info','Frontend\ProfileController@profilePersonal')->name('personal-info');
    Route::resource('/profile/addresses','Frontend\AddressController');
    Route::post('/profile/addresses/enable/{id}' , 'Frontend\AddressController@enable');
    Route::get('/profile/favorites','Frontend\ProfileController@favorites')->name('favorites');
    Route::get('/profile/orders','Frontend\ProfileController@orderList')->name('orders.list');
    Route::get('/cart','Frontend\CartController@showCart')->name('cart.show');
    Route::get('/order-verify/{value?}', 'Frontend\OrderController@verify')->name('order.verify');
    Route::get('/payment-verify', 'Frontend\PaymentController@verify')->name('payment.verify');
    Route::get('/shopping', 'Frontend\ShoppingController@index')->name('shopping');
    Route::get('/shopping-payment', 'Frontend\ShoppingController@shoppingPayment')->name('shopping.payment');
    Route::get('/shopping-complete-buy','Frontend\ShoppingController@completeBuy')->name('shopping.complete');
    Route::get('/shopping-not-complete-buy','Frontend\ShoppingController@notCompleteBuy')->name('shopping.notComplete');
    Route::post('/coupon','Admin\CouponController@addCoupons')->name('add.coupon');
    Route::get('product/comment/{product:sku}/{slug}','Admin\CommentController@show')->name('comment.show');
    Route::post('product/comment/{sku}','Admin\CommentController@create')->name('comment.create');
    Route::post('/product/recommend/{id}','Admin\CommentController@recommend')->name('comment.recommend');

    //add product to favorite list
    Route::get('favorite','Frontend\FavoriteController@add')->name('favorite.add');
    Route::post('favorite-add/{sku}','Frontend\FavoriteController@add');
});


/********************  guest page  ****************************************/

Route::get('/','Frontend\MainController@index')->name('/');
Route::get('add/{id}/{idd?}','Frontend\CartController@addToCart')->name('add');
Route::post('remove/{id}','Frontend\CartController@removeCart')->name('cart.remove');
Route::get('/product/{sku}/{slug}','Frontend\ProductController@index')->name('product');
//all cat products possessing
Route::get('/search/{sku}/{slug}','Frontend\ProductController@productList')->name('get.productList');
Route::get('/search/sells','Frontend\ProductController@productSell')->name('get.productSell');
Route::get('/search/news','Frontend\ProductController@productNew')->name('get.productNew');
Route::get('/search','Frontend\ProductController@search')->name('search');

Route::get('/privacy','HomeController@privacy')->name('privacy');
Route::get('/about-us','HomeController@about')->name('about');
Route::get('/careers','HomeController@careers')->name('careers');
Route::get('/contact','HomeController@contact')->name('contact');
Route::get('/terms-of-use','HomeController@teams')->name('teams');
Route::get('/questions','HomeController@question')->name('questions');
Route::get('/returning-product','HomeController@returning')->name('returning');
Route::get('/register-product','HomeController@register')->name('register.product');
Route::get('/send-product','HomeController@send')->name('send.product');
Route::get('/payment-product','HomeController@payment')->name('payment.product');






<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();

            $table->float('price',11,0);
            $table->float('discount',11,0)->nullable();
            $table->integer('qty');
            $table->integer('valueColor')->nullable();
            $table->integer('valueSize')->nullable();
            $table->tinyInteger('status');
            $table->string('attValue')->nullable();
            $table->string('attGroup')->nullable();

            $table->unsignedBigInteger('attvalue_id')->nullable();
            $table->foreign('attvalue_id')->references('id')->on('attvalues')->onDelete('CASCADE');

            $table->unsignedBigInteger('attributeValue_id');
            $table->foreign('attributeValue_id')->references('id')->on('attribute_values')->onDelete('CASCADE');

            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('CASCADE');


            $table->unsignedBigInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}

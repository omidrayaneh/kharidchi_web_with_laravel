<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('body');
            $table->integer('commentable_id');
            $table->string('commentable_type');
            $table->tinyInteger('status')->nullable();
            $table->tinyInteger('read')->default(0);
            $table->tinyInteger('star')->nullable();
            $table->string('advantages')->nullable();
            $table->string('disadvantages')->nullable();
            $table->tinyInteger('recommended')->nullable();
            $table->integer('recommend')->default(0);
            $table->integer('notrecommend')->default(0);

            $table->unsignedBigInteger('written_by');
            $table->foreign('written_by')->references('id')->on('users');

            $table->unsignedBigInteger('accepted_by')->nullable();
            $table->foreign('accepted_by')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
